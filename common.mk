ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

define PINFO
PINFO DESCRIPTION=QNX Screen Window Manager
endef

EXTRA_SRCVPATH=$(PROJECT_ROOT)/src $(PROJECT_ROOT)/resmgr $(PROJECT_ROOT)/taskbar
NAME=screenwm
USEFILE=
LIBS+=screen slog2 cairo socket
POST_INSTALL=$(CP_HOST) -r $(PROJECT_ROOT)/images $(INSTALL_ROOT_nto)/usr/share/screenwm/

include $(MKFILES_ROOT)/qtargets.mk
