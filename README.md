ScreenWM
========

Simple window manager for the QNX Screen compositor.

Disclaimer
----------

This project is not affiliated with BlackBerry QNX in any way, and is the
result of a personal effort by the author in his spare time.

Build
-----

To build ScreenWM you need to have a licensed copy of QNX SDP, with the
following libraries installed for development:

* Screen
* Fontconfig
* Libpng
* Freetype
* Cairo

`QNX_HOST` and `QNX_TARGET` are assumed to be set to the correct SDP version.

ScreenWM uses the standard QNX recursive make system: to build run `make` from
the top-level directory with the appropriate target CPU, e.g.:

    # make CPU=x86_64

Deploy
------

On the target machine, ScreenWM is expected to reside in a separate directory,
which contains both the executable and the necessary ancillary files. The latter
include configuration files, images, theme DLLs and desktop files for task bar
shortcuts.

An example deployment under `/system/opt/screenwm` has the following structure:

```
/system
    |- /opt
        |- /screenwm
            |- screenwm
			|- screenwm.ini
			|- startup.ini
			|- theme.so
			|- terminal.desktop
			|- browser.desktop
			|- /images
                |- background.png
				|- qnx_logo.png
				|- smoky_close.png
				|- smoky_close_pressed.png
				|- smoky_min.png
				|- smoky_min_pressed.png
				|- smoky_max.png
				|- smoky_max_pressed.png
				|- terminal.png
				|- browser.png
```

In the example above,

* `screenwm` is the executable;
* `screenwm.ini` is the main configuration file (see [Configure](#config)
  below);
* `startup.ini` is a configuration file for launching programs when the window
  manager is started;
* `theme.so` is an optional theme DLL (need not exist, in which case the default
  "smoky" theme is used);
* the `*.desktop` files are standard XDG shortcut files used to populate the
  task bar;
* the `images` folder holds the various images needed for drawing the background,
  window decorations, application icons, etc.

Configure
---------

Configuration consists of multiple `*.ini` and `*.desktop` files under the main
folder.

### `screenwm.ini`

This is the main configuration file. The following fields can be used under the
`[General]` group (all optional):

* `background`: either the name of a PNG image, or the hex value of a colour,
  preceded by `@` (e.g., `@00c0c0` for a 90's background).
* `taskbar`: if specified, uses the native task bar included with ScreenWM. This
  can be omitted in favour of an external program providing a launcher.
* `screenIdle`: the idle time, in minutes, before the display is turned
  off. This option may not be supported on all machines. Press any key to turn
  the display back on.

Example:

```
[General]
background=images/background.png
taskbar=bottom
screenIdle=20
```

### `startup.ini`

The file specifies programs that are launched by ScreenWM when it starts. The
file is optional: when using the native task bar there is no need to launch any
external program to have a usable desktop.

The file consists of groups, each representing one program to launch. The
following fields are recognized:

* `exec`: the command line to use for the program;
* `position`: initial position, in absolute screen coordinates. A negative value
  aligns to the right/bottom.
* `type`: optional field, with the either `dock` or `locker` for the value. Both
  types are frameless and top-level, with a locker also blocking all input until
  it is unlocked. The locker can be made visible by pressing `CTRL-ALT-DELETE`.

Example:

```
[Locker]
exec=/system/bin/screenlock
position=0x0
type=locker

[Clock]
exec=/system/bin/analogclock
position=100x100

[Launcher]
exec=/system/opt/launcher/launcher
position=0x-1
type=dock
```

### `.desktop`

The native task bar reads each of these files and uses these to populate
quick-access buttons. The format adheres to the XDG standard.

Example:

```
[Desktop Entry]
Name=Terminal
Exec=/system/opt/qterminal/qterminal
Icon=images/terminal.png
Categories=Utilities
```
