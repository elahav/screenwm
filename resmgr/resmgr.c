/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    resmgr.c
 * @brief   ScreenWM resource manager
 *
 * The resource manager advertises information about the window manager, as well
 * as provides a control interface for clients. In particular, the resource
 * manager exposes the list of current windows, and allows a client to activate
 * a window.
 * The node registered by the resource manager is /dev/screenwm. Reading from
 * this noe provides the current window list, which is composed of window names
 * and handles. Writing one of these handle values to the node activates the
 * corresponding window.
 *
 * The resource manager is implemented in C as a separate static library as
 * attempts to write it in C++ proved awkward.
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <pthread.h>
#include <sys/iofunc.h>
#include <sys/dispatch.h>
#include "../public/screenwm.h"
#include "resmgr.h"

typedef struct window_s window_t;

/**
 * A node on a list of windows.
 */
struct window_s
{
    window_t    *next;
    void        *handle;
    char        name[];
};

typedef struct notify_s notify_t;

/**
 * Notification structure.
 * Keeps track of a client that has registered an event to be delivered when
 * a certain event occurs.
 */
struct notify_s
{
    notify_t        *next;
    rcvid_t         rcvid;
    struct sigevent event;
};

static resmgr_connect_funcs_t   connect_funcs;
static resmgr_io_funcs_t        io_funcs;
static iofunc_attr_t            io_attr;
static resmgr_attr_t            rattr;
static dispatch_t               *resmgr_dispatch;
static int                      resmgr_id;
static pthread_t                resmgr_tid;
static screen_context_t         screen_context;
static screen_event_t           screen_event;
static window_t                 *window_list;
static pthread_mutex_t          window_list_mutex = PTHREAD_MUTEX_INITIALIZER;
static notify_t                 *notify_table[_SCREENWM_EVENT_NUM];
static pthread_mutex_t          notify_table_mutex = PTHREAD_MUTEX_INITIALIZER;

/**
 * read() callback.
 * Replys to an _IO_READ message from the connected client with a list of frame
 * titles.
 * @param   ctp     Connection and message context
 * @param   msg     _IO_READ message
 * @param   ocb     Open connection block
 * @return  Number of parts in the reply vector
 */
static int
resmgr_read(resmgr_context_t *ctp, io_read_t *msg, RESMGR_OCB_T *ocb)
{
    // Do not handle offsets other than 0. The client is expected to provide a
    // sufficiently-sized reply buffer to hold the entire list.
    if (ocb->offset > 0) {
        _IO_SET_READ_NBYTES(ctp, 0);
        return _RESMGR_NPARTS(0);
    }

    // Determine the number of bytes in the reply message.
    size_t  maxbytes = (ctp->msg_max_size - ctp->offset) - sizeof(msg->i);
    if (maxbytes > msg->i.nbytes) {
        maxbytes = msg->i.nbytes;
    }

    char    *outbuf = (char *)(&msg->i + 1);
    size_t  outbytes = 0;

    // Write back the list of window titles and handles.
    pthread_mutex_lock(&window_list_mutex);
    for (window_t *window = window_list;
         window != NULL;
         window = window->next) {
        // Determine the number of bytes required for writing this window
        // information.
        size_t  nbytes = strlen(window->name) + 18;
        if ((outbytes + nbytes) >= maxbytes) {
            break;
        }

        nbytes = snprintf(outbuf, nbytes, "%s\n%p\n", window->name,
                          window->handle);

        outbuf += nbytes;
        outbytes += nbytes;
    }
    pthread_mutex_unlock(&window_list_mutex);

    SETIOV(ctp->iov, &msg->i + 1, outbytes);
	_IO_SET_READ_NBYTES(ctp, outbytes);
	ocb->offset += outbytes;
	return _RESMGR_NPARTS(1);
}

/**
 * write() callback.
 * Expects a number identifying the list position of a frame to make active
 * @param   ctp     Connection and message context
 * @param   msg     _IO_WRITE message
 * @param   ocb     Open connection block
 * @return  Number of parts in the reply vector
 */
static int
resmgr_write(resmgr_context_t *ctp, io_write_t *msg, RESMGR_OCB_T *ocb)
{
    size_t  nbytes = msg->i.nbytes;

    if ((ctp->offset + nbytes) > ctp->msg_max_size) {
        return EBADMSG;
    }

    // Convert text into an index number.
    char const  *str = (char const *)(msg + 1);
    uintptr_t   handle = 0;
    for (size_t i = 0; i < nbytes; i++) {
        if (str[i] == '\0') {
            break;
        }

        if ((str[i] >= '0') && (str[i] <= '9')) {
            handle = (handle << 4) + (str[i] - '0');
        } else if ((str[i] >= 'a') && (str[i] <= 'f')) {
            handle = (handle << 4) + (str[i] - 'a' + 10);
        } else if ((str[i] >= 'A') && (str[i] <= 'F')) {
            handle = (handle << 4) + (str[i] - 'A' + 10);
        } else {
            return ERANGE;
        }
    }

    fflush(stderr);

    int data[4] = { 0, (int)handle, (int)(handle >> 32) };
    screen_set_event_property_iv(screen_event, SCREEN_PROPERTY_USER_DATA,
                                 data);
    if (screen_send_event(screen_context, screen_event, getpid()) < 0) {
        return ENXIO;
    }

	_IO_SET_WRITE_NBYTES(ctp, nbytes);
    return _RESMGR_NPARTS(0);
}

/**
 * Handles a _IO_MSG message.
 * Messages of this type are used to register notifications for various actions.
 * @param   ctp     Connection and message context
 * @param   msg     _IO_MSG message, holding a screenwm_notify_msg structure
 * @param   ocb     Open connection block
 * @return  0 if successful, error code otherwise
 */
static int
resmgr_msg(resmgr_context_t *ctp, io_msg_t *msg, RESMGR_OCB_T *ocb)
{
    if (ctp->size < sizeof(struct screenwm_notify_msg)) {
        return EBADMSG;
    }

    struct screenwm_notify_msg  *snmsg = (void *)msg;
    if (snmsg->event_name >= _SCREENWM_EVENT_NUM) {
        return ERANGE;
    }

    notify_t    *notify = malloc(sizeof(notify_t));
    if (notify == NULL) {
        return ENOMEM;
    }

    notify->rcvid = ctp->rcvid;
    memcpy(&notify->event, &snmsg->event, sizeof(struct sigevent));

    pthread_mutex_lock(&notify_table_mutex);
    notify->next = notify_table[snmsg->event_name];
    notify_table[snmsg->event_name] = notify;
    pthread_mutex_unlock(&notify_table_mutex);

    return 0;
}

/**
 * Entry function for the thread running the resource manager's event loop.
 * @param   arg     Dispatch context allocated by screen_resmgr_init()
 */
static void *
resmgr_thread(void *arg)
{
    dispatch_context_t  *dispctx = arg;

    for (;;) {
		dispctx = dispatch_block(dispctx);
		if (dispctx == NULL) {
            break;
		}

		dispatch_handler(dispctx);
	}

    resmgr_detach(resmgr_dispatch, resmgr_id, 0);
    dispatch_destroy(resmgr_dispatch);
    return NULL;
}

/**
 * Initializes the resource manager.
 * @param   scrctx  Screen context handle for the window manager
 * @return  0 if successful, -1 otherwise
 */
int
screenwm_resmgr_init(screen_context_t scrctx)
{
    // Create the main dispatch structure.
    dispatch_t  *dispatch = dispatch_create();
    if (dispatch == NULL) {
        return -1;
    }

    // Initialize I/O callback functions.
    iofunc_func_init(_RESMGR_CONNECT_NFUNCS, &connect_funcs,
					 _RESMGR_IO_NFUNCS, &io_funcs);
    io_funcs.read = resmgr_read;
    io_funcs.write = resmgr_write;
    io_funcs.msg = resmgr_msg;

    // Initialize mount point node attributes.
    time_t          curtime = time(NULL);
	iofunc_attr_init(&io_attr, S_IFREG | 0666, 0, 0);
	io_attr.nbytes = 4096;
	io_attr.mtime = curtime;
	io_attr.atime = curtime;
	io_attr.ctime = curtime;

    // Set resource manager attributes.
	memset(&rattr, 0, sizeof(rattr));
	rattr.nparts_max = 1;
	rattr.msg_max_size = 2048;

    // Attach to the mount point.
    int id = resmgr_attach(dispatch, &rattr, "/dev/screenwm", _FTYPE_ANY, 0,
                           &connect_funcs, &io_funcs, &io_attr);
	if (id < 0) {
        dispatch_destroy(dispatch);
        return -1;
	}

    // Create the initial message context.
    dispatch_context_t  *dispctx = dispatch_context_alloc(dispatch);
    if (dispctx == NULL) {
        resmgr_detach(dispatch, id, 0);
        dispatch_destroy(dispatch);
        return -1;
    }

    resmgr_id = id;
    resmgr_dispatch = dispatch;
    screen_context = scrctx;

    // Create a screen event used to notify the window manager event loop.
    int const   type = SCREEN_EVENT_USER;
    screen_create_event(&screen_event);
    screen_set_event_property_iv(screen_event, SCREEN_PROPERTY_TYPE, &type);

    // Start the resource manager loop on another thread.
    if (pthread_create(&resmgr_tid, NULL, resmgr_thread, dispctx) != 0) {
        dispatch_context_free(dispctx);
        resmgr_detach(dispatch, id, 0);
        dispatch_destroy(dispatch);
        return - 1;
    }

    return 0;
}

/**
 * Adds a window to the list exposed by the resource manager.
 * The function is called when a window is first posted, as well as when a
 * window title changes.
 * @param   name    Window title
 * @param   handle  Opaque value identifying the window
 */
void
screenwm_resmgr_add_window(const char *name, void *handle)
{
    pthread_mutex_lock(&window_list_mutex);

    // Find the end of the list.
    window_t    **itr = &window_list;
    window_t    *window;
    for (;;) {
        window = *itr;
        if (window == NULL) {
            break;
        }

        if (window->handle == handle) {
            // Found a window with the same handle, which happens when an
            // existing window title changes.
            window = realloc(window, sizeof(*window) + strlen(name) + 1);
            strcpy(window->name, name);
            *itr = window;
            break;
        }

        itr = &(*itr)->next;
    }

    // Create a new window structure.
    if (window == NULL) {
        window = malloc(sizeof(*window) + strlen(name) + 1);
        window->next = NULL;
        window->handle = handle;
        strcpy(window->name, name);

        // Link at the end of the list.
        *itr = window;
    }

    pthread_mutex_unlock(&window_list_mutex);

    // Notify clients.
    screenwm_resmgr_deliver_event(SCREENWM_EVENT_WINDOW_LIST,
                                  SCREENWM_WINDOW_LIST_ADDED,
                                  handle);
}

/**
 * Deletes a window from the list exposed by the resource manager.
 * The function is called when a window is hidden or destroyed.
 * @param   handle  Opaque value identifying the window
 */
void
screenwm_resmgr_remove_window(void *handle)
{
    // Remove from the list of windows.
    pthread_mutex_lock(&window_list_mutex);
    window_t    **itr = &window_list;
    for (;;) {
        window_t    *window = *itr;
        if (window == NULL) {
            break;
        }

        if (window->handle == handle) {
            *itr = window->next;
            free(window);
            break;
        }

        itr = &(*itr)->next;
    }
    pthread_mutex_unlock(&window_list_mutex);

    // Notify clients.
    screenwm_resmgr_deliver_event(SCREENWM_EVENT_WINDOW_LIST,
                                  SCREENWM_WINDOW_LIST_REMOVED,
                                  handle);
}

/**
 * Notifies registered clients of certain actions, typically those triggered by
 * keyboard shortcuts.
 * @param   action  Action code
 */
void
screenwm_resmgr_deliver_event(int event, int const code, void * const handle)
{
    pthread_mutex_lock(&notify_table_mutex);
    for (notify_t *notify = notify_table[event]; notify != NULL;
         notify = notify->next) {
        notify->event.sigev_code = code;
        notify->event.sigev_value.sival_ptr = handle;
        if (MsgDeliverEvent(notify->rcvid, &notify->event) == -1) {
            perror("MsgDeliverEvent");
        }
    }
    pthread_mutex_unlock(&notify_table_mutex);
}
