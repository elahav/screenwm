ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

define PINFO
PINFO DESCRIPTION = Photon theme for the Screen Window Manager
endef
INSTALLDIR=
NAME=photon
USEFILE=

EXTRA_INCVPATH=$(PROJECT_ROOT)/../../src $(LIBCAIRO_INC_PATH)
EXTRA_LIBVPATH=$(LIBCAIRO_LIB_PATH)
LIBS=cairo

include $(MKFILES_ROOT)/qtargets.mk
