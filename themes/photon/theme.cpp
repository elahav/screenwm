/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "theme.h"
#include <cairo/cairo.h>
#include <iostream>

namespace ScreenWM
{

/**
 * Constants for the position and size of various frame elements.
 */
struct FrameMetrics
{
    static const int    TopHeight = 24;
    static const int    BottomHeight = 5;
    static const int    LeftWidth = 5;
    static const int    RightWidth = 5;
    static const int    ButtonWidth = 21;
    static const int    ButtonHeight = 21;
    static const int    ButtonPad = 0;
    static const int    ButtonTop = 2;
    static const int    ButtonBottom = ButtonTop + ButtonHeight;
    static const int    CloseButtonRight = -2;
    static const int    CloseButtonLeft = CloseButtonRight - ButtonWidth;
    static const int    MaximizeButtonRight = CloseButtonLeft - ButtonPad;
    static const int    MaximizeButtonLeft = MaximizeButtonRight - ButtonWidth;
    static const int    MinimizeButtonRight = MaximizeButtonLeft - ButtonPad;
    static const int    MinimizeButtonLeft = MinimizeButtonRight - ButtonWidth;
    static const int    TextSize = 14;
    static const int    IconLeft = 2;
    static const int    IconTop = 2;
    static const int    IconRight = IconLeft + 28;
    static const int    TitleLeft = IconRight;
    static const int    TitleTop = 2;
    static const int    TitleRight = MinimizeButtonLeft;
    static const int    TitleBottom = TopHeight;
    static const int    TitleRightEdgeLeft = MinimizeButtonLeft - 5;
};

/**
 * Implements a Photon theme for the Screen Window Manager.
 */
class PhotonTheme : public Theme
{
public:
    PhotonTheme() {}
    ~PhotonTheme() {}

    virtual Theme *create() { return new PhotonTheme; }
    virtual void destroy() { delete this; }
    virtual int getFormat() { return SCREEN_FORMAT_RGBA8888; }

    virtual void drawFrame(const Frame &frame, const Rect &rect, bool active);
    virtual void drawElement(const Frame &frame, Element element,
                             ButtonState state, Rect &rect);
    virtual int getElementSize(Element) const;
    virtual Element getElementFromPoint(const Frame &frame,
                                        const Point &pos) const;

protected:
    virtual bool init();

private:
    static cairo_surface_t  *closeImage_[3];
    static cairo_surface_t  *maximizeImage_[3];
    static cairo_surface_t  *minimizeImage_[3];
    static cairo_surface_t  *iconImage_;
    static cairo_surface_t  *titleEdgeImage_;
    static cairo_pattern_t  *titlePattern_[2];

    void drawTab(const Frame &frame, double left, double top, double right,
                 double bottom, bool active, cairo_t *cr);
};

cairo_surface_t *PhotonTheme::closeImage_[3];
cairo_surface_t *PhotonTheme::maximizeImage_[3];
cairo_surface_t *PhotonTheme::minimizeImage_[3];
cairo_surface_t *PhotonTheme::iconImage_;
cairo_surface_t *PhotonTheme::titleEdgeImage_;
cairo_pattern_t *PhotonTheme::titlePattern_[2];

/**
 * Draws a frame, along with all of its elements, into the buffer of the given
 * frame window.
 * @param   frame   Window to draw to
 */
void
PhotonTheme::drawFrame(const Frame &frame, const Rect &rect, bool active)
{
    // Get the window's buffer.
    Buffer          buffer = frame.getBuffer();
    if (!buffer.isValid()) {
        return;
    }

    unsigned char   *ptr
        = reinterpret_cast<unsigned char *>(buffer.getPointer());
    if (ptr == NULL) {
        return;
    }

    cairo_surface_t *surface =
        cairo_image_surface_create_for_data(ptr, CAIRO_FORMAT_ARGB32,
                                            rect.width_, rect.height_,
                                            buffer.getStride());

    cairo_t         *cr = cairo_create(surface);

    cairo_select_font_face(cr, "Fixed", CAIRO_FONT_SLANT_NORMAL,
                           CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, FrameMetrics::TextSize);

    cairo_set_line_width(cr, 1);
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);

    // Frame.
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_move_to(cr, 0.5, 0.5);
    cairo_line_to(cr, 0.5, rect.height_ - 0.5);
    cairo_line_to(cr, rect.width_ - 0.5, rect.height_ - 0.5);
    cairo_line_to(cr, rect.width_ - 0.5, 0.5);
    cairo_line_to(cr, 0.5, 0.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.22, 0.22, 0.22);
    cairo_move_to(cr, 1.5, 1.5);
    cairo_line_to(cr, 1.5, rect.height_ - 1.5);
    cairo_line_to(cr, rect.width_ - 1.5, rect.height_ - 1.5);
    cairo_line_to(cr, rect.width_ - 1.5, 1.5);
    cairo_line_to(cr, 1.5, 1.5);
    cairo_stroke(cr);

    // Left side.
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_move_to(cr, 2.5, 23.5);
    cairo_line_to(cr, 2.5, rect.height_ - 18.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.9, 0.9, 0.93);
    cairo_move_to(cr, 3.5, 23.5);
    cairo_line_to(cr, 3.5, rect.height_ - 18.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.61, 0.61, 0.58);
    cairo_move_to(cr, 4.5, 23.5);
    cairo_line_to(cr, 4.5, rect.height_ - 18.5);
    cairo_stroke(cr);

    // Right side.
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_move_to(cr, rect.width_ - 4.5, 23.5);
    cairo_line_to(cr, rect.width_ - 4.5, rect.height_ - 18.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.75, 0.75, 0.75);
    cairo_move_to(cr, rect.width_ - 3.5, 23.5);
    cairo_line_to(cr, rect.width_ - 3.5, rect.height_ - 18.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.68, 0.68, 0.68);
    cairo_move_to(cr, rect.width_ - 2.5, 23.5);
    cairo_line_to(cr, rect.width_ - 2.5, rect.height_ - 18.5);
    cairo_stroke(cr);

    // Bottom.
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_move_to(cr, 18.5, rect.height_ - 4.5);
    cairo_line_to(cr, rect.width_ - 18.5, rect.height_ - 4.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.75, 0.75, 0.75);
    cairo_move_to(cr, 18.5, rect.height_ - 3.5);
    cairo_line_to(cr, rect.width_ - 18.5, rect.height_ - 3.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.68, 0.68, 0.68);
    cairo_move_to(cr, 18.5, rect.height_ - 2.5);
    cairo_line_to(cr, rect.width_ - 18.5, rect.height_ - 2.5);
    cairo_stroke(cr);

    // Bottom-left corner.
    cairo_set_source_rgb(cr, 0.77, 0.77, 0.77);
    cairo_move_to(cr, 2.5, rect.height_ - 17.5);
    cairo_line_to(cr, 2.5, rect.height_ - 2.5);
    cairo_line_to(cr, 17.5, rect.height_ - 2.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.75, 0.75, 0.75);
    cairo_move_to(cr, 3.5, rect.height_ - 17.5);
    cairo_line_to(cr, 3.5, rect.height_ - 3.5);
    cairo_line_to(cr, 17.5, rect.height_ - 3.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.55, 0.55, 0.55);
    cairo_move_to(cr, 4.5, rect.height_ - 17.5);
    cairo_line_to(cr, 4.5, rect.height_ - 4.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.77, 0.77, 0.77);
    cairo_move_to(cr, 5.5, rect.height_ - 4.5);
    cairo_line_to(cr, 17.5, rect.height_ - 4.5);
    cairo_stroke(cr);

    // Bottom-right corner.
    cairo_set_source_rgb(cr, 0.77, 0.77, 0.77);
    cairo_move_to(cr, rect.width_ - 4.5, rect.height_ - 17.5);
    cairo_line_to(cr, rect.width_ - 4.5, rect.height_ - 4.5);
    cairo_line_to(cr, rect.width_ - 17.5, rect.height_ - 4.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.75, 0.75, 0.75);
    cairo_move_to(cr, rect.width_ - 3.5, rect.height_ - 17.5);
    cairo_line_to(cr, rect.width_ - 3.5, rect.height_ - 3.5);
    cairo_line_to(cr, rect.width_ - 17.5, rect.height_ - 3.5);
    cairo_stroke(cr);

    cairo_set_source_rgb(cr, 0.55, 0.55, 0.55);
    cairo_move_to(cr, rect.width_ - 2.5, rect.height_ - 17.5);
    cairo_line_to(cr, rect.width_ - 2.5, rect.height_ - 2.5);
    cairo_line_to(cr, rect.width_ - 17.5, rect.height_ - 2.5);
    cairo_stroke(cr);

    // Title background.
    const int titleRight = rect.width_ + FrameMetrics::TitleRight;
    cairo_rectangle(cr, FrameMetrics::TitleLeft, FrameMetrics::TitleTop,
                    titleRight, FrameMetrics::TitleBottom);
    cairo_set_source(cr, active ? titlePattern_[0] : titlePattern_[1]);
    cairo_fill(cr);

    // Title text.
    cairo_text_extents_t    extents;
    cairo_text_extents(cr, frame.getTitle().c_str(), &extents);
    int titleCentre = (FrameMetrics::TitleLeft + titleRight) / 2;
    cairo_set_source_rgb(cr, 0, 0, 0.39);
    cairo_move_to(cr, titleCentre - extents.width / 2, 16);
    cairo_show_text(cr, frame.getTitle().c_str());

    // Buttons.
    const int   idx = active ? 0 : 2;
    cairo_set_source_surface(cr, closeImage_[idx],
                             rect.width_ + FrameMetrics::CloseButtonLeft,
                             FrameMetrics::ButtonTop);
    cairo_paint(cr);

    cairo_set_source_surface(cr, maximizeImage_[idx],
                             rect.width_ + FrameMetrics::MaximizeButtonLeft,
                             FrameMetrics::ButtonTop);
    cairo_paint(cr);

    cairo_set_source_surface(cr, minimizeImage_[idx],
                             rect.width_ + FrameMetrics::MinimizeButtonLeft,
                             FrameMetrics::ButtonTop);
    cairo_paint(cr);

    cairo_set_source_surface(cr, iconImage_, FrameMetrics::IconLeft,
                             FrameMetrics::IconTop);
    cairo_paint(cr);

    cairo_set_source_surface(cr, titleEdgeImage_,
                             rect.width_ + FrameMetrics::TitleRightEdgeLeft,
                             FrameMetrics::ButtonTop);
    cairo_paint(cr);

    cairo_destroy(cr);
    cairo_surface_destroy(surface);
}

/**
 * Draws a specific element into the buffer of the given frame window.
 * @param   frame   Window to draw to
 * @param   element Identifies the element to draw
 * @param   state   For buttons, whether the button is pressed or not
 * @param   rect    Holds the coordinates of the drawn area, upon return
 */
void
PhotonTheme::drawElement(const Frame &frame, Element element,
                       ButtonState state, Rect &rect)
{
    // Get the window's buffer.
    Buffer          buffer = frame.getBuffer();
    if (!buffer.isValid()) {
        return;
    }

    // Get the frame's size.
    if (!frame.getGeometry(rect)) {
        return;
    }

    unsigned char   *ptr
        = reinterpret_cast<unsigned char *>(buffer.getPointer());
    if (ptr == NULL) {
        return;
    }

    cairo_surface_t *surface =
        cairo_image_surface_create_for_data(ptr, CAIRO_FORMAT_ARGB32,
                                            rect.width_, rect.height_,
                                            buffer.getStride());

    cairo_t         *cr = cairo_create(surface);

    // Dispatch to element-specific drawing.
    switch (element) {
    case CloseButton:
        cairo_set_source_surface(cr, closeImage_[state],
                                 rect.width_ + FrameMetrics::CloseButtonLeft,
                                 FrameMetrics::ButtonTop);
        cairo_paint(cr);
        break;

    case MaximizeButton:
        cairo_set_source_surface(cr, maximizeImage_[state],
                             rect.width_ + FrameMetrics::MaximizeButtonLeft,
                             FrameMetrics::ButtonTop);
        cairo_paint(cr);
        break;

    case MinimizeButton:
        cairo_set_source_surface(cr, minimizeImage_[state],
                                 rect.width_ + FrameMetrics::MinimizeButtonLeft,
                                 FrameMetrics::ButtonTop);
        cairo_paint(cr);
        break;

    default:
        break;
    }

    cairo_destroy(cr);
    cairo_surface_destroy(surface);
}

/**
 * Returns the size, in pixels, of a specific element.
 * The meaning of the size value depends on the requested element.
 * @param   element Element to get the size for
 * @return  Element size in pixels
 */
int
PhotonTheme::getElementSize(Element element) const
{
    switch (element) {
    case TitleBar:
        return FrameMetrics::TopHeight;

    case LeftFrame:
        return FrameMetrics::LeftWidth;

    case RightFrame:
        return FrameMetrics::RightWidth;

    case BottomFrame:
        return FrameMetrics::BottomHeight;

    default:
        return -1;
    }
}

/**
 * Determines the frame element from a given mouse cursor position.
 * @param   frame   The frame
 * @param   pos     Cursor coordinates
 * @return  The element matching the given coordinates, Theme::None if the
 *          coordinates do not correspond to any element.
 */
Theme::Element
PhotonTheme::getElementFromPoint(const Frame &frame, const Point &pos) const
{
    // Get the window's size.
    Rect            rect;
    if (!frame.getGeometry(rect)) {
        return Theme::None;
    }

    // Check for a button.
    if ((pos.y_ >= FrameMetrics::ButtonTop)
        && (pos.y_ < FrameMetrics::ButtonBottom)) {
        // Vertical point matches the button row.
        if ((pos.x_ >= (rect.width_ + FrameMetrics::CloseButtonLeft))
            && (pos.x_ < (rect.width_ + FrameMetrics::CloseButtonRight))) {
            return Theme::CloseButton;
        }

        if ((pos.x_ >= (rect.width_ + FrameMetrics::MaximizeButtonLeft))
            && (pos.x_ < (rect.width_ + FrameMetrics::MaximizeButtonRight))) {
            return Theme::MaximizeButton;
        }

        if ((pos.x_ >= (rect.width_ + FrameMetrics::MinimizeButtonLeft))
            && (pos.x_ < (rect.width_ + FrameMetrics::MinimizeButtonRight))) {
            return Theme::MinimizeButton;
        }
    }

    // Check for the title bar.
    if (pos.y_ < FrameMetrics::TopHeight) {
        return Theme::TitleBar;
    }

    // Check for the bottom frame.
    if (pos.y_ > (rect.height_ - FrameMetrics::BottomHeight)) {
        return Theme::BottomFrame;
    }

    // Check for the left frame.
    if (pos.x_ < FrameMetrics::LeftWidth) {
        return Theme::LeftFrame;
    }

    // Check for the right frame.
    if (pos.x_ > (rect.width_ - FrameMetrics::RightWidth)) {
        return Theme::RightFrame;
    }

    return Theme::None;
}

/**
 * Initializes the theme.
 * Loads the necessary bitmaps for drawing the buttons.
 */
bool
PhotonTheme::init()
{
    closeImage_[0] = cairo_image_surface_create_from_png("photon_close.png");
    closeImage_[1] = cairo_image_surface_create_from_png("photon_close_pressed.png");
    closeImage_[2] = cairo_image_surface_create_from_png("photon_close_inactive.png");
    maximizeImage_[0] = cairo_image_surface_create_from_png("photon_max.png");
    maximizeImage_[1] = cairo_image_surface_create_from_png("photon_max_pressed.png");
    maximizeImage_[2] = cairo_image_surface_create_from_png("photon_max_inactive.png");
    minimizeImage_[0] = cairo_image_surface_create_from_png("photon_min.png");
    minimizeImage_[1] = cairo_image_surface_create_from_png("photon_min_pressed.png");
    minimizeImage_[2] = cairo_image_surface_create_from_png("photon_min_inactive.png");
    iconImage_ = cairo_image_surface_create_from_png("photon_icon.png");
    titleEdgeImage_ = cairo_image_surface_create_from_png("photon_right_title_edge.png");
    if ((closeImage_[0] == NULL)
        || (closeImage_[1] == NULL)
        || (closeImage_[2] == NULL)
        || (maximizeImage_[0] == NULL)
        || (maximizeImage_[1] == NULL)
        || (maximizeImage_[2] == NULL)
        || (minimizeImage_[0] == NULL)
        || (minimizeImage_[1] == NULL)
        || (minimizeImage_[2] == NULL)
        || (iconImage_ == NULL)
        || (titleEdgeImage_ == NULL)) {
        std::cerr << "Photon theme: Failed to load images" << std::endl;
        return false;
    }

    // Create title patterns.
    titlePattern_[0] = cairo_pattern_create_linear(
        0.0, 0.0, 0.0, FrameMetrics::TopHeight - 2);
    cairo_pattern_add_color_stop_rgb(titlePattern_[0], 0, 0.74, 0.87, 0.96);
    cairo_pattern_add_color_stop_rgb(titlePattern_[0], 1, 0.32, 0.55, 0.84);

    titlePattern_[1] = cairo_pattern_create_linear(
        0.0, 0.0, 0.0, FrameMetrics::TopHeight - 2);
    cairo_pattern_add_color_stop_rgb(titlePattern_[1], 0, 1, 1, 1);
    cairo_pattern_add_color_stop_rgb(titlePattern_[1], 1, 0.71, 0.77, 0.87);
    return true;
}

}

extern "C"
{

ScreenWM::Theme *LoadTheme()
{
    return new ScreenWM::PhotonTheme();
}

}
