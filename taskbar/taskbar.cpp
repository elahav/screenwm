/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "context.h"
#include "taskbar.h"
#include "clock.h"
#include "ipaddr.h"
#include "tooltip.h"
#include "windowlist.h"
#include <filesystem>
#include <regex>

namespace ScreenWM
{

/**
 * @brief   A timer for hiding the tooltip after a certain period
 */
class TooltipTimer : public Timer
{
public:
    /**
     * Class constructor.
     * @param   taskbar The owner taskbar object
     */
    TooltipTimer(TaskBar *taskbar) : Timer(), taskbar_(taskbar) {}

    /**
     * Timer action.
     * Destroys the tooltip.
     */
    void callback() {
        delete taskbar_->tooltip_;
        taskbar_->tooltip_ = nullptr;
    }

private:
    /** The owner taskbar object. */
    TaskBar *taskbar_;
};

/**
 * Class constructor.
 */
TaskBar::TaskBar() : CairoWindow(), tooltip_(nullptr), tooltipTasklet_(nullptr)
{
    tooltipTimer_ = new TooltipTimer(this);
}

/**
 * Create the task bar's window.
 * @return  true if successful, false otherwise
 */
bool
TaskBar::create()
{
    // Create the window.
    if (!LocalWindow::create(WindowData::Frameless | WindowData::TopLevel)) {
        return false;
    }

    // Get screen size to position the taskbar.
    Rect screenRect;
    Context::getContext().getDesktopArea(screenRect);

    windowRect_.x_ = 0;
    windowRect_.y_ = screenRect.height_ - TaskBarMetrics::Height;
    windowRect_.width_ = screenRect.width_;
    windowRect_.height_ = TaskBarMetrics::Height;
    setGeometry(windowRect_);

    Size size(windowRect_.width_, windowRect_.height_);
    if (!CairoWindow::create(size)) {
        return false;
    }

    // Create the various tasklets.
    addTasklets();

    // Draw the bar.
    draw();

    // Make it visible.
    setProperty(SCREEN_PROPERTY_VISIBLE, 1);
    return true;
}

/**
 * Redraw the task bar if anything has changed.
 * Called periodically.
 */
void
TaskBar::update()
{
    bool needUpdate = false;

    for (auto tsk : tasklets_) {
        if (tsk->update()) {
            needUpdate = true;
        }
    }

    if (needUpdate) {
        draw();
    }
}

/**
 * Handles a pointer event on the task bar.
 * @param   event   Pointer event
 * @param   buttons Button state
 * @return  true if successful, false otherwise
 */
bool
TaskBar::handlePointer(const Event *event, int buttons)
{
    // Determine which tasklet, if any, is under the pointer.
    int posArray[2];
    if (!event->getProperty(SCREEN_PROPERTY_SOURCE_POSITION, posArray)) {
        return false;
    }

    Point pos(posArray[0], posArray[1]);

    Tasklet *tasklet = nullptr;
    for (auto tsk : tasklets_) {
        if (tsk->within(pos)) {
            tasklet = tsk;
            break;
        }
    }

    // Delete tooltip window if the tasklet has changed (including from non-null
    // to null).
    if (tooltipTasklet_ != tasklet) {
        delete tooltip_;
        tooltip_ = nullptr;
        tooltipTimer_->disarm();
    }

    if (tasklet == nullptr) {
        return true;
    }

    // Check for a click event (button press or release).
    if (tasklet->clicked(buttons)) {
        delete tooltip_;
        tooltip_ = nullptr;
        draw();
        post(RectArray());
        return true;
    }

    // Display tooltip if just hovering over a tasklet.
    if (tooltip_ == nullptr) {
        const char *text = tasklet->tooltipText();
        if (text != nullptr) {
            tooltip_ = new ToolTip();
            int y = windowRect_.y_ - TaskBarMetrics::PopupGap;
            tooltip_->create(Point(pos.x_, y), windowRect_.width_, text);
            tooltipTasklet_ = tasklet;

            tooltipTimer_->arm(2000000000UL, Timer::Relative);
        }
    }

    return true;
}

/**
 * Check if any app icon is waiting for a window that has just been posted.
 */
void
TaskBar::handleWindowPosted(const Window &window)
{
    pid_t   pid;
    if (!window.getProperty(SCREEN_PROPERTY_OWNER_PID, &pid)) {
        return;
    }

    for (auto tasklet : tasklets_) {
        AppIcon *app = dynamic_cast<AppIcon *>(tasklet);
        if ((app != nullptr) && app->windowPosted(pid)) {
            app->draw(cairo_);
        }
    }
}

/**
 * Create tasklets.
 */
void
TaskBar::addTasklets()
{
    int x = 10;

    // Add the QNX logo.
    AppIcon *qnx = new AppIcon;
    qnx->setImage("./images/qnx_logo.png");
    tasklets_.push_back(qnx);
    qnx->setPosition(Point(x, -1));
    x += qnx->width() + TaskBarMetrics::TaskletSpacing;

    // Add a window list.
    int y = windowRect_.y_ - TaskBarMetrics::PopupGap;
    WindowListTasklet *windowlist = new WindowListTasklet(Point(x, y));
    tasklets_.push_back(windowlist);
    windowlist->setPosition(Point(x, -1));
    x += windowlist->width();

    // Find and load all applications specified via .desktop files.
    const std::filesystem::path curdir = std::filesystem::current_path();
    const std::regex desktop_suffix(".*[.]desktop");
    for (auto dirent : std::filesystem::directory_iterator(curdir)) {
        if (std::regex_match(dirent.path().string(), desktop_suffix)) {
            // Found a desktop file, create the application icon.
            AppIcon *app = new AppIcon();
            if (app->loadDesktopFile(dirent.path())) {
                tasklets_.push_back(app);
                app->setPosition(Point(x, -1));
                x += app->width() + TaskBarMetrics::TaskletSpacing;
            } else {
                delete app;
            }
        }
    }

    // Add a clock tasklet.
    Clock *clock = new Clock(this);
    x = windowRect_.width_ - clock->width() - TaskBarMetrics::TaskletSpacing;
    clock->setPosition(Point(x , -1));

    // Add an IP address tasklet.
    IPAddr *ipaddr = new IPAddr;
    x = x - ipaddr->width() - TaskBarMetrics::TaskletSpacing;
    ipaddr->setPosition(Point(x , -1));

    tasklets_.push_back(ipaddr);
    tasklets_.push_back(clock);
}

/**
 * Draw the task bar.
 */
void
TaskBar::draw()
{
    // Draw background.
    cairo_set_source_rgb(cairo_, .95, .95, .95);
    cairo_rectangle(cairo_, 0, 0, windowRect_.width_, windowRect_.height_);
    cairo_fill(cairo_);

    // Draw all tasklets
    for (auto tasklet : tasklets_) {
        tasklet->draw(cairo_);
    }

    post(RectArray());
}

}
