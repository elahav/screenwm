/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "clock.h"
#include "taskbar.h"
#include "cairowindow.h"
#include "slog2.h"

namespace ScreenWM
{

// Pick some value as a way to know that the clock has not been set and time()
// returns a value close to the beginning of the epoch time.
static constexpr time_t MIN_EPOCH = 156297600UL;

/**
 * Class constructor
 */
Clock::Clock(TaskBar *taskbar)
    : Tasklet(), Timer(), taskbar_(taskbar)
{
    // Determin the size of needed to draw the text.
    CairoWindow::calcTextSize("88:88", "Sans", TaskBarMetrics::LargeFont,
                              size_);
    update();
}

/**
 * Draw the clock.
 * @param   cr  Cairo object for drawing
 */
void
Clock::draw(cairo_t *cr)
{
    cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
                           CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cr, TaskBarMetrics::LargeFont);
    cairo_set_source_rgb(cr, 0, 0, 0);

    char buf[10];
    if (lastTime_ != 0) {
        auto tm = localtime(&lastTime_);
        snprintf(buf, sizeof(buf), "%.2d:%.2d", tm->tm_hour, tm->tm_min);
    } else {
        snprintf(buf, sizeof(buf), "--:--");
    }

    cairo_move_to(cr, pos_.x_, pos_.y_ + size_.height_);
    cairo_show_text(cr, buf);
}

/**
 * Update the current time.
 * @return  true if the clock has changed, false otherwise
 */
bool
Clock::update()
{
    // Get the current time.
    time_t cur = time(NULL);
    if (cur < MIN_EPOCH) {
        // Time has not been set yet (waiting for NTP or a manual date command).
        // Check again in 5 seconds.
        arm(5 * 1000000000UL, Timer::Relative);
        lastTime_ = 0;
        return false;
    }

    bool rc = (cur / 60) != (lastTime_ / 60);

    // Re-arm the timer to fire on the next turn of the minute.
    time_t secs_to_next_minute = 60 - (cur % 60);
    arm(secs_to_next_minute * 1000000000UL, Timer::Relative);

    lastTime_ = cur;
    return rc;
}

/**
 * Generate the tooltip text for the clock.
 * This is just the current date.
 */
const char *
Clock::tooltipText()
{
    if (lastTime_ != 0) {
        auto tm = localtime(&lastTime_);
        strftime(date_, sizeof(date_), "%A %F", tm);
    } else {
        snprintf(date_, sizeof(date_), "Clock not set yet");
    }
    return date_;
}

/**
 * Timer callback.
 */
void
Clock::callback()
{
    taskbar_->update();
}

}
