/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "windowlist.h"
#include "taskbar.h"
#include "context.h"
#include "frame.h"

namespace ScreenWM
{

/**
 * Class constructor.
 */
WindowList::WindowList(WindowListTasklet *tasklet)
    : CairoWindow(), tasklet_(tasklet), size_(0, 0), curItem_(-1)
{
}

/**
 * Class destructor.
 */
WindowList::~WindowList()
{
    tasklet_->windowList_ = nullptr;
}

/**
 * Create the window list window.
 * @param   pos     The position of the window. For a bottom task bar, this is
 *                  the bottom left corner of the window
 */
bool
WindowList::create(const Point &pos)
{
    // Create the window.
    if (!LocalWindow::create(WindowData::Frameless | WindowData::TopLevel)) {
        return false;
    }

    // Determine the size of the window.
    itemHeight_ = 0;
    for (auto frame : Frame::zorderList()) {
        Size linesize;
        calcTextSize(frame->getTitle(), "Sans", TaskBarMetrics::SmallFont,
                     linesize);
        if (linesize.width_ > size_.width_) {
            size_.width_ = linesize.width_;
        }

        if (linesize.height_ > itemHeight_) {
            itemHeight_ = linesize.height_;
        }

        frames_.push_back(frame);
    }

    // FIXME:
    // Handle a list that is too big for the screen size.
    itemHeight_ += TaskBarMetrics::WindowListSpacing;
    size_.width_ += TaskBarMetrics::WindowListSpacing * 2;
    size_.height_ = itemHeight_ * frames_.size();
    size_.height_ += TaskBarMetrics::WindowListSpacing;
    int y = pos.y_ - size_.height_;

    Rect rect = { .x_ = pos.x_, .y_ = y,
                  .width_ = size_.width_, .height_ = size_.height_ };
    if (!setGeometry(rect)) {
        return false;
    }

    if (!CairoWindow::create(size_)) {
        return false;
    }

    draw();
    post(RectArray());

    // Make it visible.
    setProperty(SCREEN_PROPERTY_VISIBLE, 1);
    return true;
}

/**
 * Handle a mouse pointer event over the window list.
 * @param   event   Pointer event
 * @param   buttons Button state
 * @return  true if successful, false otherwise
 */
bool
WindowList::handlePointer(const Event *event, int buttons)
{
    // Determine the item under the mouse pointer.
    int pos[2];
    if (!event->getProperty(SCREEN_PROPERTY_SOURCE_POSITION, pos)) {
        return false;
    }

    int item = pos[1] / itemHeight_;
    if (item >= (int)frames_.size()) {
        item = -1;
    }

    if ((buttons & SCREEN_LEFT_MOUSE_BUTTON) != 0) {
        // Left click on an item, activate the frame and close the window list.
        // FIXME:
        // With the current implementation it is possible for a frame to be
        // closed and deleted while the list is displayed, which will lead to a
        // bad frame pointer being passed to activate().
        // Either make the list modal, or handle frames being deleted.
        if (item != -1) {
            Context::getContext().activate(frames_[item]);
        }
        delete this;
        return true;
    }

    if (item == curItem_) {
        return true;
    }

    // Redraw the list, to show a frame around the active item.
    curItem_ = item;
    draw();
    post(RectArray());

    return true;
}

/**
 * Draw the window list.
 */
void
WindowList::draw()
{
    // Draw background.
    cairo_set_source_rgb(cairo_, .95, .95, .95);
    cairo_rectangle(cairo_, 0, 0, size_.width_, size_.height_);
    cairo_fill(cairo_);

    // Draw frame titles.
    cairo_select_font_face(cairo_, "Sans", CAIRO_FONT_SLANT_NORMAL,
                           CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cairo_, TaskBarMetrics::SmallFont);
    cairo_set_source_rgb(cairo_, 0, 0, 0);

    int y = 0;
    int item = 0;
    for (auto frame : frames_) {
        // Draw the current item in blue, all others in black.
        if (item == curItem_) {
            cairo_set_source_rgb(cairo_, 0, 0, 1);
        } else {
            cairo_set_source_rgb(cairo_, 0, 0, 0);
        }
        item++;

        std::string title = frame->getTitle();
        cairo_text_extents_t extents;
        cairo_text_extents(cairo_, title.c_str(), &extents);

        y += (int)extents.height + TaskBarMetrics::WindowListSpacing;
        cairo_move_to(cairo_, TaskBarMetrics::WindowListSpacing, y);
        cairo_show_text(cairo_, title.c_str());
    }
}

/**
 * Class constructore
 * @param   windowListPos   Position of the window list, when clicked
 */
WindowListTasklet::WindowListTasklet(const Point &windowListPos) :
    Tasklet(),
    windowList_(nullptr),
    windowListPos_(windowListPos),
    pressed_(false)
{
    size_.width_ = 30;
    size_.height_ = 30;
}

/**
 * Draw the tasklet in the taskbar.
 * @param   cr  Cairo drawing object
 */
void
WindowListTasklet::draw(cairo_t *cr)
{
    int x = pos_.x_ + 5;
    int y = pos_.y_ + 5;
    if (pressed_) {
        x++;
        y++;
    }

    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_rectangle(cr, x, y, 16, 16);
    cairo_stroke(cr);
    cairo_rectangle(cr, x + 4, y + 4, 16, 16);
    cairo_stroke(cr);
}

/**
 * Handles a left-mouse button event on the tasklet.
 * Releasing the button toggles between showing and hiding the window list.
 * @param   buttons     Mouse button state
 * @return  true if the tasklet was clicked, false otherwise
 */
bool
WindowListTasklet::clicked(int buttons)
{
    if ((buttons & SCREEN_LEFT_MOUSE_BUTTON) != 0) {
        // Left mouse button pressed.
        if (!pressed_) {
            pressed_ = true;
            return true;
        }
    } else {
        // Left mouse button released.
        if (pressed_) {
            pressed_ = false;
            if (windowList_ == nullptr) {
                windowList_ = new WindowList(this);
                if (!windowList_->create(windowListPos_)) {
                    delete windowList_;
                    windowList_ = nullptr;
                }
            } else {
                delete windowList_;
                windowList_ = nullptr;
            }

            return true;
        }
    }

    return false;
}

}
