/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ipaddr.h"
#include "taskbar.h"
#include "cairowindow.h"
#include "context.h"
#include "slog2.h"
#include <thread>
#include <ifaddrs.h>
#include <sys/socket.h>
#include <net/if.h>
#include <net/route.h>
#include <arpa/inet.h>

namespace ScreenWM
{

/**
 * Class constructor
 */
IPAddr::IPAddr() : Tasklet(), ifIndex_(0)
{
    // Create the routing socket.
    routeSocket_ = socket(PF_ROUTE, SOCK_RAW, PF_INET);
    if (routeSocket_ != -1) {
        new std::thread(&IPAddr::monitorIPChanges, this);
    } else {
        Slog2(SLOG2_ERROR).perror("Failed to create routing socket");
    }

    getIP();

    // Determine the size needed to draw the text.
    CairoWindow::calcTextSize("888.888.888.888", "Sans",
                              TaskBarMetrics::LargeFont, size_);

}

/**
 * Draw the IP address.
 * @param   cr  Cairo object for drawing
 */
void
IPAddr::draw(cairo_t *cr)
{
    cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
                           CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cr, TaskBarMetrics::LargeFont);
    cairo_set_source_rgb(cr, 0, 0, 0);

    char buf[16];
    if (addr_.s_addr == 0) {
        strcpy(buf, "---.---.---.---");
    } else {
        strcpy(buf, inet_ntoa(addr_));
    }

    cairo_move_to(cr, pos_.x_, pos_.y_ + size_.height_);
    cairo_show_text(cr, buf);
}

/**
 * Update the current IP address.
 * @return  true if the address has changed, false otherwise
 */
bool
IPAddr::update()
{
    return true;
}

/**
 * Generate the tooltip text for tasklet.
 */
const char *
IPAddr::tooltipText()
{
    return "IP Address";
}

/**
 * Look for an IP address when the tasklet starts.
 */
void
IPAddr::getIP()
{
    addr_.s_addr = 0;

    // Get all network addresses.
    ifaddrs *addrs;
    if (getifaddrs(&addrs) == -1) {
        Slog2(SLOG2_ERROR).perror("getifaddrs");
        return;
    }

    // Look for a PF_INET address for an interface that is UP and not LOOPBACK.
    for (ifaddrs *addr = addrs; addr != nullptr; addr = addr->ifa_next) {
        if (addr->ifa_addr->sa_family != PF_INET) {
            continue;
        }

        if ((addr->ifa_flags & (IFF_UP | IFF_LOOPBACK)) == IFF_UP) {
            addr_ = reinterpret_cast<sockaddr_in *>(addr->ifa_addr)->sin_addr;
            ifIndex_ = if_nametoindex(addr->ifa_name);
            break;
        }
    }

    freeifaddrs(addrs);
}

/**
 * A thread that listens for IP changes.
 */
void
IPAddr::monitorIPChanges()
{
    struct {
        struct ifa_msghdr   hdr;
        char                data[256];
    } msg;

    Event event;
    event.setUserData(Event::TaskbarUpdate, 0);

    for (;;) {
        // Wait for notifications.
        ssize_t len = recv(routeSocket_, &msg, sizeof(msg), 0);
        if (len == -1) {
            Slog2(SLOG2_ERROR).perror("Failed to receive on routing socket");
            std::this_thread::sleep_for(std::chrono::seconds(10));
            continue;
        }

        if (msg.hdr.ifam_type == RTM_NEWADDR) {
            // New address available.
            // Only handle it if there isn't already an address.
            if (addr_.s_addr != 0) {
                continue;
            }

            // Iterate over all sockaddr structures in the payload.
            char *ptr = msg.data;
            char *end = reinterpret_cast<char *>(&msg) + len;
            int addr_index = 0;
            for (;;) {
                sockaddr_in *addr = reinterpret_cast<struct sockaddr_in *>(ptr);
                if ((msg.hdr.ifam_addrs & (1U << addr_index)) != 0) {
                    // sockaddr structure is applicable.
                    if (addr->sin_family == PF_INET) {
                        addr_ = addr->sin_addr;
                        ifIndex_ = msg.hdr.ifam_index;
                        Context::getContext().sendEvent(event);
                        break;
                    }
                }
                ptr += reinterpret_cast<struct sockaddr *>(ptr)->sa_len;
                if (ptr >= end) {
                    break;
                }

                addr_index++;
            }
        } else if (msg.hdr.ifam_type == RTM_DELADDR) {
            // Address disappeared.
            if (ifIndex_ == 0) {
                continue;
            }

            if (msg.hdr.ifam_index == ifIndex_) {
                addr_.s_addr = 0;
                Context::getContext().sendEvent(event);
                ifIndex_ = -1;
            }
        }
    }
}

}
