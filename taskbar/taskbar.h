/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_TASKBAR_H
#define SCREENWM_TASKBAR_H

#include "screenwm.h"
#include "cairowindow.h"
#include "appicon.h"
#include "event.h"
#include "timer.h"

namespace ScreenWM
{

class AppIcon;
class ToolTip;

/**
 * Constants for task bar dimensions.
 */
struct TaskBarMetrics
{
    /** Height of the task bar. */
    static const int    Height = 40;
    /** Font size for normal text. */
    static const int    SmallFont = 14;
    /** Font size for tasklets. */
    static const int    LargeFont = 20;
    /** Space around tasklets. */
    static const int    TaskletSpacing = 10;
    /** Space around tooltip text. */
    static const int    ToolTipSpacing = 3;
    /** Space around window list text. */
    static const int    WindowListSpacing = 20;
    /** Space above task bar for tooltips and window list. */
    static const int    PopupGap = 2;
};

/**
 * @brief   Simple menu and status bar
 */
class TaskBar : public CairoWindow
{
public:
    TaskBar();

    bool create();
    void update();
    bool handlePointer(const Event *event, int buttons) override;
    void handleWindowPosted(const Window &window);

private:
    /** The position and size of the window. */
    Rect            windowRect_;

    /** A list of all tasklets shown in the bar. */
    Tasklet::List   tasklets_;

    /** Active tooltip window, if any. */
    ToolTip         *tooltip_;

    /** The tasklet for which the tooltip is shown. */
    Tasklet         *tooltipTasklet_;

    /** Timer to hide the tooltip. */
    Timer           *tooltipTimer_;

    void addTasklets();
    void draw();

    friend class TooltipTimer;
};

}

#endif
