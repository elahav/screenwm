/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_APPICON_H
#define SCREENWM_APPICON_H

#include "screenwm.h"
#include "window.h"
#include "tasklet.h"

namespace ScreenWM
{

class AppIcon : public Tasklet
{
public:
    AppIcon();
    ~AppIcon();

    void draw(cairo_t *cr) override;
    bool clicked(int buttons);
    bool loadDesktopFile(std::string path);
    bool setImage(std::string filepath);
    bool windowPosted(pid_t pid);

    const char *tooltipText() override {
        return tooltip_.empty() ? nullptr : tooltip_.c_str();
    }

    static void loadAll(List &apps, Point pos);

private:
    enum State {
        Normal,
        Clicked,
        Wait
    };

    std::string     tooltip_;
    char            **execargs_;
    cairo_surface_t *image_;
    State           state_;
    pid_t           pid_;

    void exec();
    void setExecArgs(std::string execline);
};

}

#endif
