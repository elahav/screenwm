/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_TASKLET_H
#define SCREENWM_TASKLET_H

#include "screenwm.h"
#include <cairo/cairo.h>

namespace ScreenWM
{

/**
 * @brief   Base class for any item in the task bar
 */
class Tasklet
{
public:
    Tasklet() : pos_(0, 0), size_(0, 0) {}
    virtual ~Tasklet() {}

    typedef std::vector<Tasklet *> List;

    void setPosition(const Point &pos);

    /**
     * @return  The width of the tasklet
     */
    int width() const { return size_.width_; }

    /**
     * Determine if the given position is within the icon's area.
     * @param   pos     The mouse position
     * @return  true if the position is inside the icon's borders, false
     *          otherwise
     */
    bool within(const Point &pos) {
        return (pos.x_ >= pos_.x_) &&
               (pos.x_ < (pos_.x_ + size_.width_)) &&
               (pos.y_ >= pos_.y_) &&
               (pos.y_ < (pos_.y_ + size_.height_));
    }

    virtual void draw(cairo_t *cr) = 0;
    virtual bool clicked(int button) { return false; }
    virtual bool update() { return false; }
    virtual const char *tooltipText() { return nullptr; }

protected:
    Point   pos_;
    Size    size_;
};

}

#endif
