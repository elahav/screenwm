/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "appicon.h"
#include "context.h"
#include "settings.h"
#include <spawn.h>

namespace ScreenWM
{

/**
 * Class constructor.
 */
AppIcon::AppIcon() : Tasklet(), execargs_(nullptr), image_(nullptr),
                     state_(Normal), pid_(-1)
{
}

/**
 * Class destructor.
 */
AppIcon::~AppIcon()
{
    if (image_ != nullptr) {
        cairo_surface_destroy(image_);
    }
}

/**
 * Load the image to be used by this icon.
 * @param   filepath    The path of the image
 * @return  true if successful, false otherwise
 */
bool
AppIcon::setImage(std::string filepath)
{
    if (image_ != nullptr) {
        return false;
    }

    image_ = cairo_image_surface_create_from_png(filepath.c_str());
    if (cairo_surface_status(image_) != CAIRO_STATUS_SUCCESS) {
        cairo_surface_destroy(image_);
        image_ = nullptr;
        Slog2(SLOG2_ERROR).log("Failed to load image %s\n", filepath.c_str());
        return false;
    }

    size_.width_ = cairo_image_surface_get_width(image_);
    size_.height_ = cairo_image_surface_get_height(image_);
    Slog2(SLOG2_DEBUG1).log("Loaaded image %s (%d,%d)", filepath.c_str(),
                            size_.width_, size_.height_);
    return true;
}

/**
 * Set the argument array for launching the application associated with this
 * icon.
 * @param   execline    Command line for the application
 */
void
AppIcon::setExecArgs(std::string execline)
{
    // Tokenize command.
    std::vector<char *> execvec;
    char *line = strdup(execline.c_str());
    char *arg = strtok(line, " ");
    for (;;) {
        dprint("Token: %s\n", arg == nullptr ? "null" : arg);
        execvec.push_back(arg);
        if (arg == nullptr) {
            break;
        }

        arg = strtok(nullptr, " ");
    }

    // Create a NULL-terminated argument array, as expected by posix_spawn().
    execargs_ = new char*[execvec.size()];
    int i = 0;
    for (char *arg : execvec) {
        execargs_[i++] = arg;
    }
}

/**
 * Draw the icon on the task bar.
 * @param   cr  Cairo context
 */
void
AppIcon::draw(cairo_t *cr)
{
    // Draw the icon.
    // When clicked, the icon is offset to the botton-right.
    if (state_ == Clicked) {
        cairo_set_source_surface(cr, image_, pos_.x_ + 1, pos_.y_ + 1);
    } else {
        cairo_set_source_surface(cr, image_, pos_.x_, pos_.y_);
    }

    cairo_paint(cr);

    // While waiting for the process to start, make the icon shaded.
    if (state_ == Wait) {
        cairo_set_source_rgba(cr, 1, 1, 1, 0.5);
        cairo_rectangle(cr, pos_.x_, pos_.y_, size_.width_, size_.height_);
        cairo_fill(cr);
    }
}

/**
 * Handles a click on the icon.
 * If an application is associated with the icon, it is executed.
 * @param   buttons Mouse button state
 * @return  true if the icon's state has changed, false otherwise
 */
bool
AppIcon::clicked(int buttons)
{
    if ((buttons & SCREEN_LEFT_MOUSE_BUTTON) != 0) {
        // Left mouse button pressed.
        if (state_ == Normal) {
            state_ = Clicked;
            return true;
        }
    } else {
        // Left mouse button released.
        if (state_ == Clicked) {
            exec();
            return true;
        }
    }

    return false;
}

/**
 * Read application information from a .desktop file.
 */
bool
AppIcon::loadDesktopFile(std::string path)
{
    Settings desktop(path.c_str());
    Slog2(SLOG2_DEBUG1).log("Loading %s", path.c_str());
    if (!desktop.isValid()) {
        return false;
    }

    std::string execline = desktop.getValue("Desktop Entry", "Exec");
    if (execline.empty()) {
        return false;
    }

    Slog2(SLOG2_DEBUG1).log(" Exec %s", execline.c_str());

    std::string icon = desktop.getValue("Desktop Entry", "Icon");
    if (icon.empty()) {
        return false;
    }

    Slog2(SLOG2_DEBUG1).log(" Icon %s", icon.c_str());

    setExecArgs(execline);
    if (!setImage(icon)) {
        return false;
    }

    tooltip_ = desktop.getValue("Desktop Entry", "Comment");
    if (tooltip_.empty()) {
        tooltip_ = desktop.getValue("Desktop Entry", "Name");
    }

    return true;
}

/**
 * Called when a new window is posted.
 * If this window belongs to the last process launched by this icon then the
 * icon's state is set back to normal.
 * @param   pid     The ID of the process that owns the window.
 */
bool
AppIcon::windowPosted(pid_t pid)
{
#if 0
    // FIXME:
    // Matching on pid does not work for the browser, because the window is
    // posted by a different process than the one launched.
    // Find a better solution.
    if (pid_ != pid) {
        return false;
    }
#else
    if (pid_ == -1) {
        return false;
    }
#endif

    state_ = Normal;
    pid_ = -1;
    return true;
}

/**
 * Execute the application associated with this icon.
 */
void
AppIcon::exec()
{
    if (execargs_ == nullptr) {
        return;
    }

    Slog2(SLOG2_DEBUG1).log("Execing %s", execargs_[0]);

    pid_t pid;
    int rc = posix_spawn(&pid, execargs_[0], NULL, NULL, execargs_, NULL);
    if (rc == 0) {
        pid_ = pid;
        state_ = Wait;
    } else {
        Slog2(SLOG2_ERROR).log("Failed to launch %s: %s", execargs_[0],
                               strerror(rc));
    }
}

}
