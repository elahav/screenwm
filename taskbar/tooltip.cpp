/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "tooltip.h"
#include "taskbar.h"
#include "context.h"

namespace ScreenWM
{

/**
 * Create the tooltip window.
 * @param   pos         The position of the window
 * @param   text        The text to display
 * @param   screenWidth The maximum x position for the tooltip
 * @return  true if successful, false otherwise
 */
bool
ToolTip::create(const Point &pos, int screenWidth, std::string text)
{
    // Create the window.
    if (!LocalWindow::create(WindowData::Frameless | WindowData::TopLevel)) {
        return false;
    }

    // Determin the size of needed to draw the text.
    calcTextSize(text, "Sans", TaskBarMetrics::SmallFont, size_);
    size_.width_ += TaskBarMetrics::ToolTipSpacing * 2;
    size_.height_ += TaskBarMetrics::ToolTipSpacing * 2;

    // Make sure the tooltip does not extend beyond the edge of the screen.
    int x = pos.x_;
    if ((pos.x_ + size_.width_) > screenWidth) {
        x = screenWidth - size_.width_;
    }

    // Set the size and position of the window.
    Rect rect = { .x_ = x, .y_ = pos.y_ - size_.height_,
                  .width_ = size_.width_, .height_ = size_.height_ };
    if (!setGeometry(rect)) {
        return false;
    }

    if (!setProperty(SCREEN_PROPERTY_ZORDER, ZOrder::TopLevel + 1)) {
        return false;
    }

    // Create the Cairo drawing objects.
    if (!CairoWindow::create(size_)) {
        return false;
    }

    // Draw the window.
    draw(text);
    post(RectArray());

    return true;
}

/**
 * Draw the tooltip.
 * @param   text    The text to draw
 */
void
ToolTip::draw(std::string text)
{
    cairo_set_source_rgb(cairo_, 1, 1, .75);
    cairo_rectangle(cairo_, 0, 0, size_.width_, size_.height_);
    cairo_fill(cairo_);

    cairo_set_source_rgb(cairo_, 1, 1, 1);
    cairo_rectangle(cairo_, 0, 0, size_.width_, size_.height_);
    cairo_stroke(cairo_);

    cairo_select_font_face(cairo_, "Sans", CAIRO_FONT_SLANT_NORMAL,
                           CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cairo_, TaskBarMetrics::SmallFont);
    cairo_set_source_rgb(cairo_, 0, 0, 0);

    cairo_move_to(cairo_, TaskBarMetrics::ToolTipSpacing,
                  size_.height_ - TaskBarMetrics::ToolTipSpacing);
    cairo_show_text(cairo_, text.c_str());
}

}
