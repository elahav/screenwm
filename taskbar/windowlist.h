/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_WINDOWLIST_H
#define SCREENWM_WINDOWLIST_H

#include "screenwm.h"
#include "cairowindow.h"
#include "tasklet.h"

namespace ScreenWM
{

class WindowListTasklet;

/**
 * @brief   A list of open windows
 *
 * This window displays a list with the title of all current frames. Clicking on
 * a title activates the matching frame.
 */
class WindowList : public CairoWindow
{
public:
    WindowList(WindowListTasklet *tasklet);
    ~WindowList();

    bool create(const Point &pos);
    bool handlePointer(const Event *event, int buttons) override;

private:
    /** A pointer to the tasklet. */
    WindowListTasklet       *tasklet_;

    /** The size of the window. */
    Size                    size_;

    /** The frames shown in the list. */
    std::vector<Frame *>    frames_;

    /** The height, in pixel, of every displayed item. */
    int                     itemHeight_;

    /** The index of the title under the pointer. */
    int                     curItem_;

    void draw();
};

/**
 * @brief   A taskbar tasklet for showing the window list
 */
class WindowListTasklet : public Tasklet
{
public:
    WindowListTasklet(const Point &windowListPos);

    void draw(cairo_t *cr) override;
    bool clicked(int buttons) override;
    const char *tooltipText() override { return "Window List"; }

private:
    WindowList  *windowList_;
    Point       windowListPos_;
    bool        pressed_;

    friend class WindowList;
};

}

#endif
