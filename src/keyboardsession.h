/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_KEYBOARD_SESSION_H
#define SCREENWM_KEYBOARD_SESSION_H

#include "screenwm.h"
#include "session.h"
#include "event.h"

namespace ScreenWM
{

class FrameCycle;

/**
 * @brief   Session to accept keyboard events
 *
 * A single such object is used as a top-level session which receives the events
 * from all keyboard devices. This allows the window manager to trap and handle
 * special keys before they are delivered to other windows.
 */
class KeyboardSession : public Session
{
public:
    KeyboardSession();
    ~KeyboardSession();

    bool create();
    bool handleEvent(Event &event, Window activeWindow);
    bool handleDevice(Event &event);

private:
    /**
     * The display for this session.
     */
    screen_display_t    display_;

    FrameCycle          *frameCycle_;

    bool setDeviceFocus(screen_device_t device);
};

}

#endif
