/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_SLOG2_H
#define SCREENWM_SLOG2_H

#include <string>
#include <sys/slog2.h>

namespace ScreenWM
{

/**
 * @brief   C++ wrapper for the slogger2 logging API
 *
 * The expected usage of this class is for objects to be allocated on demand,
 * for single-shot logging, using the appropriate level. For example:
 *
 *   Slog2(SLOG2_INFO).log("Hello World!");
 */
class Slog2
{
public:
    Slog2(int level) : level_(level) {}

    void log(const char *fmt, ...);
    void perror(const char *msg, int err = -1);

    static bool init(int level = SLOG2_INFO);

private:
    /** Logging level associated with this object. */
    int                     level_;

    /** Buffer handle. */
    static slog2_buffer_t   handle_;
};

}

#endif
