/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "keyboardsession.h"
#include "context.h"
#include "frame.h"
#include "framecycle.h"
#include "slog2.h"
#include <sys/keycodes.h>

namespace ScreenWM
{

/**
 * Class constructor.
 */
KeyboardSession::KeyboardSession() : Session(), frameCycle_(NULL)
{
}

/**
 * Class destructor.
 */
KeyboardSession::~KeyboardSession()
{
}

/**
 * Creates and initializes the session.
 * Finds all keyboard devices and directs their input to this session.
 * @return  true if successful, false otherwise
 */
bool
KeyboardSession::create()
{
    // Create a Keyboard session to focus keyboard devices to.
    if (!Context::getContext().createSession(SCREEN_EVENT_KEYBOARD, *this)) {
        return false;
    }

    // Enumerate devices.
    int ndevices;
    if (!Context::getContext().getProperty(SCREEN_PROPERTY_DEVICE_COUNT,
                                           ndevices)) {
        return false;
    }

    // Allocate space for the devices...
    screen_device_t *devices = new screen_device_t[ndevices];

    // And retrieve them.
    if (!Context::getContext().getProperty(SCREEN_PROPERTY_DEVICES,
                                           reinterpret_cast<void **>(devices))) {
        return false;
    }

    if (!getProperty(SCREEN_PROPERTY_DISPLAY,
                     reinterpret_cast<void **>(&display_))) {
        return false;
    }

    // Set the focus of any connected Keyboard device.
    bool    rc = true;
    for (int i = 0; i < ndevices; i++) {
        if (!setDeviceFocus(devices[i])) {
            rc = false;
            break;
        }
    }

    delete [] devices;
    return rc;
}

/**
 * Traps keyboard events.
 * Events not handled by this session are propagated back to screen to choose
 * the appropriate session.
 * @param   event           Keyboard event
 * @param   activeFrame     If not nullptr, the frame that manages the window
 *                          that should receive keyboard events not consumed by
 *                          the window manager
 * @return  true if successful, false otherwise
 */
bool
KeyboardSession::handleEvent(Event &event, Window activeWindow)
{
    int cap;
    if (!event.getProperty(SCREEN_PROPERTY_KEY_CAP, cap)) {
        return false;
    }

    int flags;
    if (!event.getProperty(SCREEN_PROPERTY_FLAGS, flags)) {
        return false;
    }

    int modifiers;
    if (!event.getProperty(SCREEN_PROPERTY_MODIFIERS, modifiers)) {
        return false;
    }

    dprint("Keyboard event: %x %x %x\n", cap, flags, modifiers);

    if (flags & KEY_DOWN) {
        switch (cap) {
        case KEYCODE_TAB:
            if (modifiers & KEYMOD_ALT) {
                // ALT+TAB pressed.
                // Activate the next frame.
                // The Z-order list is sorted in ascending order. When ALT+TAB
                // is pressed the first time we activate the frame before
                // last. If it is pressed again without releasing ALT we
                // activate the frame before that, and so on.
                dprint("Raise next frame\n");
                if (frameCycle_ == NULL) {
                    frameCycle_ = new FrameCycle;
                }

                frameCycle_->next();

                // Consume the key.
                return true;
            }
            break;
        }
    } else {
        switch (cap) {
        case KEYCODE_TAB:
            if (modifiers & KEYMOD_ALT) {
                // TAB released but ALT is still pressed.
                // Consume the key.
                return true;
            }
            break;

        case KEYCODE_DELETE:
            if ((modifiers & (KEYMOD_CTRL | KEYMOD_ALT))
                == (KEYMOD_CTRL | KEYMOD_ALT)) {
                // Ctrl+Alt+Del: lock screen.
                Context::getContext().lockScreen();
                return true;
            }
            break;
        }
    }

    if (frameCycle_ != NULL) {
        dprint("Activate frame\n");
        delete frameCycle_;
        frameCycle_ = NULL;
    }

    // For some reason, the event's device ID needs to be cleared before it is
    // propagated to the display.
    void *param = nullptr;
    screen_set_event_property_pv(event, SCREEN_PROPERTY_DEVICE, &param);

    if (activeWindow.isValid()) {
        // Send the keyboard event to the active window.
        // Screen always messes up the keyboard focus, so the window manager
        // just delivers the keyboard event to whichever window it thinks is
        // active.
        if (Context::getContext().sendEvent(event, activeWindow)) {
            return true;
        }

        dprint("Cannot send keyboard event to active window\n");
    }

    // No active window, let Screen route the event.
    if (screen_inject_event(display_, static_cast<screen_event_t>(event)) != 0) {
        perror("screen_inject_event");
        return false;
    }

    return true;
}

/**
 * Handles a SCREEN_EVENT_DEVICE event, which notifies the window manager that a
 * new device is available.
 * If this is a keyboard device then its input focus is set to this session.
 * @param   event   Device event
 * @return  true if successful, false otherwise
 */
bool
KeyboardSession::handleDevice(Event &event)
{
    screen_device_t device;
    if (!event.getProperty(SCREEN_PROPERTY_DEVICE,
                           reinterpret_cast<void **>(&device))) {
        return false;
    }

    return setDeviceFocus(device);
}

/**
 * Sets the focus of any keyboard input device to this session.
 * @param   device  The input device
 * @return  true if successful, false otherwise
 */
bool
KeyboardSession::setDeviceFocus(screen_device_t device)
{
    int type;
    int rc;
    rc = screen_get_device_property_iv(device, SCREEN_PROPERTY_TYPE,
                                       &type);
    if (rc != 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get input device property");
        return false;
    }

    Slog2(SLOG2_INFO).log("Found input device %p type %d", device, type);

    if (type != SCREEN_EVENT_KEYBOARD) {
        return true;
    }

    rc = screen_set_device_property_pv(device,
                                       SCREEN_PROPERTY_SESSION,
                                       (void **)&handle_);
    if (rc != 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set input device property");
        return false;
    }

    return true;
}

}
