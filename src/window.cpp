/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "window.h"
#include "context.h"
#include "frame.h"

namespace ScreenWM
{

/**
 * Class constructor.
 * @param   handle  Native window identifier
 */
Window::Window(screen_window_t handle) : handle_(handle)
{
}

/**
 * Class destructor.
 */
Window::~Window()
{
}

/**
 * Associates a native window with this object.
 * Can only be called if the object is not already associated with a window.
 * @param   handle  Native window identifier
 * @return  true if successful, false otherwise
 */
bool
Window::setHandle(screen_window_t handle)
{
    if (handle_) {
        Slog2(SLOG2_ERROR).log("Window already attached\n");
        return false;
    }

    handle_ = handle;
    return true;
}

/**
 * Creates a new window group with the given name.
 * The native window associated with this object becomes the owner of the group.
 * @param   name    New group name
 * @return  A handle to the created group, if successful, NULL otherwise
 */
screen_group_t
Window::createGroup(const char *name)
{
    if (screen_create_window_group(handle_, name) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to create group");
        return NULL;
    }

    screen_group_t  group;
    if (!getProperty(SCREEN_PROPERTY_GROUP, reinterpret_cast<void **>(&group))) {
        return NULL;
    }

    return group;
}

/**
 * Adds the native window to a group with the given name (owned by another
 * window).
 * @param   name    Existing group name
 * @return  true if successful, false otherwise
 */
bool
Window::joinGroup(const char *name)
{
    if (screen_join_window_group(handle_, name) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to join root window group");
        return false;
    }

    return true;
}

/**
 * Sets an integer to the value of the window property with the given name.
 * @param   name    Property identifier
 * @param   value   Variable to set
 * @return  true if successful, false otherwise
 */
bool
Window::getProperty(int name, int &value) const
{
    if (screen_get_window_property_iv(handle_, name, &value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get window property");
        dprint("Failed to get window property name=%d\n", name);
        return false;
    }

    return true;
}

/**
 * Fills an integer array with the values of the window property with the given
 * name.
 * @param   name    Property identifier
 * @param   values  Array to fill
 * @return  true if successful, false otherwise
 */
bool
Window::getProperty(int name, int values[]) const
{
    if (screen_get_window_property_iv(handle_, name, values) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get window property");
        dprint("Failed to get window property name=%d\n", name);
        return false;
    }

    return true;
}

/**
 * Fills a pointer array with the values of the window property with the given
 * name.
 * @param   name    Property identifier
 * @param   values  Array to fill
 * @return  true if successful, false otherwise
 */
bool
Window::getProperty(int name, void *values[]) const
{
    if (screen_get_window_property_pv(handle_, name, values) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get window property");
        dprint("Failed to get window property name=%d\n", name);
        return false;
    }

    return true;
}

/**
 * Fills a character buffer with the values of the window property with the
 * given name.
 * @param   name    Property identifier
 * @param   value   Buffer to fill
 * @param   len     Buffer size
 * @return  true if successful, false otherwise
 */
bool
Window::getProperty(int name, char *value, int len) const
{
    if (screen_get_window_property_cv(handle_, name, len, value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get window property");
        dprint("Failed to get window property name=%d\n", name);
        return false;
    }

    return true;
}

/**
 * Assigns a new integer value to the given property
 * @param   name    Property identifier
 * @param   value   New property value
 * @return  true if successful, false otherwise
 */
bool
Window::setProperty(int name, int value)
{
    if (screen_set_window_property_iv(handle_, name, &value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set window property");
        return false;
    }

    return true;
}

/**
 * Assigns a new integer array value to the given property
 * @param   name    Property identifier
 * @param   value   New property value
 * @return  true if successful, false otherwise
 */
bool
Window::setProperty(int name, int values[])
{
    if (screen_set_window_property_iv(handle_, name, values) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set window property");
        return false;
    }

    return true;
}

/**
 * Assigns a new string value to the given property
 * @param   name    Property identifier
 * @param   value   New property value
 * @return  true if successful, false otherwise
 */
bool
Window::setProperty(int name, const char *value, size_t len)
{
    if (len == 0) {
        len = strlen(value) + 1;
    }

    if (screen_set_window_property_cv(handle_, name, len, value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set window property");
        return false;
    }

    return true;
}

/**
 * Fills a Point structure with the window's screen coordinates.
 * @param   pos Structure to fill
 * @return  true if successful, false otherwise
 */
bool
Window::getPosition(Point &pos) const
{
    int posArray[2];
    if (!getProperty(SCREEN_PROPERTY_POSITION, posArray)) {
        return false;
    }

    pos.x_ = posArray[0];
    pos.y_ = posArray[1];
    return true;
}

/**
 * Changes the window's screen position to the given coordinates.
 * @param   pos New window coordinates
 * @return  true if successful, false otherwise
 */
bool
Window::setPosition(const Point &pos)
{
    int posArray[2] = { pos.x_, pos.y_ };
    if (!setProperty(SCREEN_PROPERTY_POSITION, posArray)) {
        return false;
    }

    return true;
}

/**
 * Fills a Rect structure with the window's screen coordinates and size.
 * @param   rect    Structure to fill
 * @return  true if successful, false otherwise
 */
bool
Window::getGeometry(Rect &rect) const
{
    int pos[2];
    if (!getProperty(SCREEN_PROPERTY_POSITION, pos)) {
        return false;
    }

    int size[2];
    if (!getProperty(SCREEN_PROPERTY_SIZE, size)) {
        return false;
    }

    rect.x_ = pos[0];
    rect.y_ = pos[1];
    rect.width_ = size[0];
    rect.height_ = size[1];

    return true;
}

/**
 * Changes the window's screen position and size to the given values.
 * @param   rect    New window coordinates and size
 * @return  true if successful, false otherwise
 */
bool
Window::setGeometry(const Rect &rect)
{
    int pos[2] = { rect.x_, rect.y_ };
    if (!setProperty(SCREEN_PROPERTY_POSITION, pos)) {
        return false;
    }

    int size[2] = { rect.width_, rect.height_ };
    if (!setProperty(SCREEN_PROPERTY_SIZE, size)) {
        return false;
    }

    return true;
}

/**
 * Returns the current screen buffer associated with this window.
 * @return  Buffer  Initialized buffer object if successful, uninitialized
 *                  object otherwise
 */
Buffer
Window::getBuffer() const
{
    screen_buffer_t handle;
    if (!getProperty(SCREEN_PROPERTY_BUFFERS, (void **)&handle)) {
        return Buffer();
    }

    return Buffer(handle);
}

/**
 * Returns the frame associated with this window.
 * For a managed native window, this is the frame drawn around it by the window
 * manager. For a frame window, this is a pointer to the window pointer itself.
 * @return  Pointer to the frame associated with this window, NULL on error or
 *          if no frame is assigned
 */
Frame *
Window::getFrame() const
{
    WindowData *data = getWindowData();
    if (data == nullptr) {
        return nullptr;
    }

    return static_cast<Frame *>(data->getFrame());
}

/**
 * Returns the local Window object associated with this window, if any.
 * Local windows are those created by the window manager. The function allows
 * the Window object for such a window to be retrieved from a temporary object
 * that wraps a native Screen handle.
 * @return  Pointer to the local Window object associated with this window,
 *          nullptr on error or if the window is not local
 */
LocalWindow *
Window::getLocalWindow() const
{
    WindowData *data = getWindowData();
    if (data == nullptr) {
        return nullptr;
    }

    return static_cast<LocalWindow *>(data->getLocalWindow());
}

/**
 * Invalidates the rectangular areas in the given array such that these get
 * redrawn on the screen.
 * @param   rectArray   Array of rectangular areas to invalidate. An empty
 *                      results in the invalidation of the entire window area
 * @return  true if successful, false otherwise
 */
bool
Window::post(const RectArray& rectArray)
{
    screen_buffer_t bufHandle;
    if (!getProperty(SCREEN_PROPERTY_BUFFERS, (void **)&bufHandle)) {
        return false;
    }

    const int *rects = NULL;
    if (rectArray.size() > 0) {
        rects = reinterpret_cast<const int *>(&rectArray[0]);
    }

    if (screen_post_window(handle_, bufHandle, rectArray.size(), rects, 0)
        < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to post window");
        return false;
    }

    return true;
}

/**
 * @return  true if the window should not have a frame, false otherwise.
 */
bool
Window::isFrameless() const
{
    WindowData *data = getWindowData();
    if (data == nullptr) {
        return false;
    }

    return data->hasFlag(WindowData::Frameless);
}

/**
 * Called when a window is posted to finalize the window's initialization.
 * Specifically, if the window is associated with a frame, the frame is created.
 * @return  true if successful, false otherwise
 */
bool
Window::posted()
{
    WindowData *data = getWindowData();
    if (data == nullptr) {
        return false;
    }

    data->setFlags(WindowData::Posted);

    setProperty(SCREEN_PROPERTY_VISIBLE, 1);

    if (data->hasFlag(WindowData::TopLevel)) {
        // Top-level window, such as tool-tips or popup menus.
        if (!setProperty(SCREEN_PROPERTY_ZORDER, ZOrder::TopLevel)) {
            return false;
        }
        Context::getContext().flush();
    }

    if (data->hasFlag(WindowData::Locker)) {
        // A locker window must have the highest Z-order.
        if (!setProperty(SCREEN_PROPERTY_ZORDER, ZOrder::Locker)) {
            return false;
        }
        Context::getContext().flush();
    }

    if (data->hasFlag(WindowData::Frameless)) {
        // Nothing more to do for frameless windows.
        return true;
    }

    return createFrame(data);
}

/**
 * Handles a window's initial request to be managed.
 * Client changes to a window's SCREEN_PROPERTY_MANAGER_STRING result in the
 * client blocking until the window manager acknowledges the change by updating
 * the same property.
 * @return  true if successful, false otherwise
 */
bool
Window::manage()
{
    char    str[64];
    if (!getProperty(SCREEN_PROPERTY_MANAGER_STRING, str, sizeof(str))) {
        return false;
    }

    str[sizeof(str) - 1] = '\0';

    if (!manage(str)) {
        return false;
    }

    // Release the client by setting MANAGER_STRING the property.
    setProperty(SCREEN_PROPERTY_MANAGER_STRING, "ScreenWM");
    Context::getContext().flush();

    return true;
}

/**
 * Handles client messages to change window attributes.
 * These messages are strings in the form of "Attribute=Value", and result from
 * two types of client calls:
 * 1. screen_manage_window(): an initial request from a window to be managed
 *    (synchronous)
 * 2. screen_inject_event(SCREEN_EVENT_MANAGER): subsequent attribute changes
 *    (asynchronous)
 * The list of supported attributes are:
 * - 'Frame': whether a window needs a frame (the string is expected to be
 *            passed to the initial request for a window to be managed). The
 *            value is either 'Y' or 'N'.
 * - 'Title': sets the window title (the value is a string).
 * - 'Pos': sets the window's position (the value is a pair of comma-delimited
 *          coordinates)
 * - 'Visible': shows/hide a window, after it has been posted. The value is 'Y'
 *              or 'N'.
 * @return  true if successful, false otherwise
 */
bool
Window::manage(const char *str)
{
    dprint("Manager string: %s\n", str);

    WindowData *data = getWindowData();
    if (data == nullptr) {
        return false;
    }

    if (strncmp(str, "Frame=", 6) == 0) {
        if (str[6] == 'Y') {
            if (data->getFrame() != nullptr) {
                // Window is already associated with a frame.
                return true;
            }

            // Allocate a new frame.
            // The frame will not be associated with the window until it is
            // created. Otherwise, the window manager may try to handle
            // operations on frames that are not ready.
            Frame   *frame = new Frame(*this, true);
            data->setFrame(frame);
        } else if (str[6] == 'N') {
            // FIXME:
            // For now frameless implies top-level. Need to decouple into
            // separate properties.
            data->setFlags(WindowData::Frameless | WindowData::TopLevel);
        } else {
            Slog2(SLOG2_ERROR).log("Unknown manager string: %s\n", str);
            return false;
        }
    } else if (strncmp(str, "Title=", 6) == 0) {
        Frame   *frame = static_cast<Frame *>(data->getFrame());
        if (frame != nullptr) {
            std::string title(&str[6]);
            frame->setTitle(title);
        }
    } else if (strncmp(str, "Pos=", 4) == 0) {
        int pos[2];
        if (sscanf(&str[4], "%d,%d", &pos[0], &pos[1]) == 2) {
            setProperty(SCREEN_PROPERTY_POSITION, pos);
        }
    } else if (strncmp(str, "Visible=", 8) == 0) {
        if (data->hasFlag(WindowData::Posted)) {
            int visible = str[8] == 'Y';
            dprint("%s window", visible ? "Showing" : "Hiding");
            setProperty(SCREEN_PROPERTY_VISIBLE, visible);
            Context::getContext().flush();
        }
    } else {
        dprint("Unknown manager string: %s\n", str);
    }

    return true;
}

/**
 * Attempts to close a window.
 * A managed window is sent a SCREEN_EVENT_MANAGER event with th window handle
 * and the SCREEN_EVENT_CLOSE subtype, and is expected to close the window
 * itself. For a non-managed window the function sends the SIGTERM signal to the
 * process.
 * @param   tryEvent    Whether to try and send an close event to the window
 * @return  true if successful, false otherwise
 */
bool
Window::close(bool tryEvent)
{
    pid_t   pid;
    if (!getProperty(SCREEN_PROPERTY_OWNER_PID, &pid)) {
        return false;
    }

    if (tryEvent) {
        // Try to send a window manager event to the process.
        Event event;
        event.setType(SCREEN_EVENT_MANAGER);
        event.setProperty(SCREEN_PROPERTY_WINDOW, handle_);
        event.setProperty(SCREEN_PROPERTY_SUBTYPE, SCREEN_EVENT_CLOSE);

        if (Context::getContext().sendEvent(event, pid)) {
            return true;
        }

        dprint("Failed to send close event\n");
    }

    // Unmanaged window, or failed to send event.
    // Kill the process.
    if (kill(pid, SIGTERM) == -1) {
        dprint("Failed to kill process %d\n", pid);
        return false;
    }

    return true;
}

/**
 * Mark this window as a screen locker.
 * @return  true if successful, false otherwise
 */
bool
Window::setLocker()
{
    WindowData *data = getWindowData();
    if (data == nullptr) {
        return false;
    }

    data->setFlags(WindowData::Locker | WindowData::Frameless);
    return true;
}

/**
 * Marks the window as a dock, which means that it is frameless and top-level.
 * @return  true if successful, false otherwise
 */
bool
Window::setDock()
{
    WindowData *data = getWindowData();
    if (data == nullptr) {
        return false;
    }

    data->setFlags(WindowData::Frameless | WindowData::TopLevel);
    return true;
}

/**
 * Get the WindowData object associated with the native window, if any
 * @return  Pointer to a WindowData object if successful, nullptr otherwise
 */
WindowData *
Window::getWindowData() const
{
    void    *ptr;
    if (!getProperty(SCREEN_PROPERTY_USER_HANDLE, ptr)) {
        return nullptr;
    }

    return reinterpret_cast<WindowData *>(ptr);
}

/**
 * Associated a WindowData object with a native window.
 * @param   data    The object to associate
 * @return  true if successful, false otherwise
 */
bool
Window::setWindowData(WindowData *data)
{
    if (!setProperty(SCREEN_PROPERTY_USER_HANDLE,
                     static_cast<void *>(data))) {
        return false;
    }

    return true;
}

/**
 * Destroys the native window associated with this object.
 * When the function returns the object is no longer associated with any native
 * window.
 */
void
Window::destroy()
{
    if (handle_ != NULL) {
        dprint("Destroying %p\n", handle_);
        if (screen_destroy_window(handle_) < 0) {
            Slog2(SLOG2_ERROR).perror("Failed to destroy window");
        }
        handle_ = NULL;
    }
}

/**
 * Create a frame for a window when it is posted.
 * Managed windows already have the frame allocated. Unmanaged windows need a
 * new frame at this point.
 * @param   data    WindowData structure for the window
 */
bool
Window::createFrame(WindowData *data)
{
    Frame   *frame = static_cast<Frame *>(data->getFrame());
    if (frame == nullptr) {
        // Need a new frame, the window is unmanaged.
        frame = new Frame(*this, false);
        data->setFrame(frame);
    }

    // Create the frame's native window.
    if (!frame->create()) {
        return false;
    }

    dprint("Created frame %p for window %p\n",
           static_cast<screen_window_t>(*frame), handle_);

    // Draw the frame.
    if (!frame->draw()) {
        return false;
    }

    return true;
}

}
