/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_WINDOW_H
#define SCREENWM_WINDOW_H

#include "screenwm.h"
#include "windowdata.h"
#include "buffer.h"
#include "slog2.h"
#include <cassert>

namespace ScreenWM
{

class Frame;
class LocalWindow;

/**
 * @brief   Representation of a native window.
 *
 * A Window object is associated with a native window handle. That handle may
 * come from an existing window or a newly created one. The former is used to
 * wrap handles of top-level windows that the window manager learns about via
 * events, while the latter is used to create frames and the root window.
 */
class Window
{
public:
    Window(screen_window_t handle = nullptr);
    virtual ~Window();

    bool setHandle(screen_window_t handle);
    screen_group_t createGroup(const char *name);
    WindowData *getWindowData() const;
    bool setWindowData(WindowData *data);
    bool joinGroup(const char *name);
    bool getProperty(int name, int &value) const;
    bool getProperty(int name, int values[]) const;
    bool getProperty(int name, void *values[]) const;
    bool getProperty(int name, char *value, int len) const;
    bool setProperty(int name, int value);
    bool setProperty(int name, int values[]);
    bool setProperty(int name, const char *value, size_t len = 0);
    bool getPosition(Point &pos) const;
    bool setPosition(const Point &pos);
    bool getGeometry(Rect &rect) const;
    bool setGeometry(const Rect &rect);
    Buffer getBuffer() const;
    Frame *getFrame() const;
    LocalWindow *getLocalWindow() const;
    bool post(const RectArray &rectArray);
    bool isFrameless() const;
    bool posted();
    bool manage();
    bool manage(const char *str);
    bool close(bool tryEvent);
    bool setLocker();
    bool setDock();

    /**
     * Assigns a new pointer value to the given property
     * @param   name    Property identifier
     * @param   value   New property value
     * @return  true if successful, false otherwise
     */
    template<typename T>
    bool setProperty(int name, T *value) {
        void    *ptr = reinterpret_cast<void *>(value);
        if (screen_set_window_property_pv(handle_, name, &ptr) < 0) {
            Slog2(SLOG2_ERROR).perror("Failed to set window property");
            return false;
        }

        return true;
    }

    /**
     * Fills a pointer array with the values of the window property with the given
     * name.
     * @param   name    Property identifier
     * @param   values  Array to fill
     * @return  true if successful, false otherwise
     */
    template<typename T>
    bool getProperty(int name, T *&value) const {
        void **ptr = reinterpret_cast<void **>(&value);
        if (screen_get_window_property_pv(handle_, name, ptr) < 0) {
            Slog2(SLOG2_ERROR).perror("Failed to get window property");
            dprint("name=%d\n", name);
            return false;
        }

        return true;
    }

    /**
     * Determines whether the window is associated with a native handle.
     */
    bool isValid() const { return handle_ != NULL; }

    /**
     * Determines if two objects refer to the same window.
     * @param   other   Window to compare
     * @return  true if the two objects have the same handle, false otherwise
     */
    bool operator==(const Window& other) const {
        return handle_ == other.handle_;
    }

    /**
     * Used to cast a Window object into a native handle.
     */
    operator screen_window_t() const { return handle_; }

    bool isVisible() const {
        int visible;
        if (!getProperty(SCREEN_PROPERTY_VISIBLE, visible)) {
            return false;
        }

        return visible != 0;
    }

    void destroy();

protected:
    /** Native window handle. */
    screen_window_t     handle_;

private:
    bool createFrame(WindowData *data);
};

}

#endif
