/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_STARTUP_H
#define SCREENWM_STARTUP_H

#include "screenwm.h"
#include "settings.h"
#include "window.h"
#include <map>

namespace ScreenWM
{

/**
 * @brief   Startup manager
 *
 * Enables the launching of various processes when the window manager starts.
 * These processes are defined in a startup.ini file residing in the current
 * directory.
 */
class Startup : public Settings
{
public:
    Startup();
    ~Startup();

    bool startAll();
    bool handleWindowPosted(Window &window);
    bool isDone() const { return procMap_.empty(); }

private:
    enum Type
    {
        Normal,
        Dock,
        Locker
    };

    struct ProcessInfo
    {
        ProcessInfo() : pos_(0, 0) {}
        Point   pos_;
        Type    type_;
    };

    std::map<pid_t, ProcessInfo *>  procMap_;

    void getPosition(const GroupIterator &itr, Point &pos);
};

}

#endif
