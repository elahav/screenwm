/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "settings.h"
#include "slog2.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

namespace ScreenWM
{

/**
 * Class constructor.
 * Loads and parses the given INI file to create the varius key-value mappings.
 * @param   path    The path to the file to load
 */
Settings::Settings(const char *path) : valid_(false)
{
    // Open the settings file.
    int         fd = open(path, O_RDONLY);
    if (fd < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to open settings file\n");
        return;
    }

    // Get the file size.
    struct stat st;
    if (fstat(fd, &st) < 0) {
        close(fd);
        Slog2(SLOG2_ERROR).perror("Failed to get file statistics\n");
        return;
    }

    // Map the file.
    void  *buf = (mmap(0, st.st_size, PROT_READ, MAP_SHARED, fd, 0));
    if (buf == MAP_FAILED) {
        Slog2(SLOG2_ERROR).perror("Failed to mmap() settings file");
        close(fd);
        return;
    }

    close(fd);

    // Parse the file.
    valid_ = parse(reinterpret_cast<const char *>(buf), st.st_size);
    munmap(buf, st.st_size);
}

/**
 * Class destructor.
 * Frees all memory allocated to this object.
 */
Settings::~Settings()
{
    for (GroupMap::iterator itr = groups_.begin();
         itr != groups_.end();
         ++itr) {
        delete (*itr).second;
    }
}

/**
 * Looks up a value for the given key and group.
 * @param   group   Group name
 * @param   key     Key name
 * @return  Value if successful, empty string otherwise
 */
std::string
Settings::getValue(std::string group, std::string key) const
{
    GroupMap::const_iterator    itr = groups_.find(group);
    if (itr == groups_.end()) {
        return "";
    }

    KeyMap  *keyMap = (*itr).second;
    if (keyMap == NULL) {
        return "";
    }

    return (*keyMap)[key];
}

/**
 * Looks up a value for the given key and group.
 * @param   itr     Group iterator
 * @param   key     Key name
 * @return  Value if successful, empty string otherwise
 */
std::string
Settings::getValue(const GroupIterator &itr, std::string key) const
{
    if (itr == groups_.end()) {
        return "";
    }

    KeyMap  *keyMap = (*itr).second;
    if (keyMap == NULL) {
        return "";
    }

    return (*keyMap)[key];
}

/**
 * @param   A group iterator
 * @return  The name of the group pointed to by the given iterator
 */
std::string
Settings::getGroup(const GroupIterator &itr) const
{
    if (itr == groups_.end()) {
        return "";
    }

    return (*itr).first;
}

/**
 * INI file parser.
 * @param   buf     Pointer to a buffer holding the file's contents
 * @param   size    Buffer length
 * @return  true if successful, false on a parse error
 */
bool
Settings::parse(const char *buf, size_t size)
{
    const char      *start = NULL;
    std::string     key;
    std::string     value;
    KeyMap          *keyMap = NULL;
    int             line = 1;

    enum {
        FindKey,
        Key,
        Equals,
        FindValue,
        Value,
        Group,
        FindNewLine,
        NewLine,
        Comment
    }           state = FindKey;

    for (size_t pos = 0; pos < size;) {
        switch (state) {
        case FindKey:
            switch (buf[pos]) {
            case ' ':
            case '\t':
                // Ignore leading whitespace.
                break;

            case '[':
                state = Group;
                start = &buf[pos + 1];
                break;

            case '#':
                // Comment line.
                state = Comment;
                break;

            case '\n':
                // Empty line.
                state = NewLine;
                break;

            default:
                state = Key;
                start = &buf[pos];
                break;
            }
            break;

        case Key:
            switch (buf[pos]) {
            case ' ':
            case '\t':
                key = std::string(start, &buf[pos] - start);
                state = Equals;
                break;

            case '=':
                key = std::string(start, &buf[pos] - start);
                state = FindValue;
                break;

            case '\n':
            case '#':
                // Key without a value.
                Slog2(SLOG2_ERROR).log("Parse error on line %d (%d)\n", line, __LINE__);
                return false;

            default:
                // Next key character.
                break;
            }
            break;

        case Equals:
            switch (buf[pos]) {
            case ' ':
            case '\t':
                // Ignore whitespace.
                break;

            case '=':
                state = FindValue;
                break;

            default:
                // Missing '=' after key.
                Slog2(SLOG2_ERROR).log("Parse error on line %d (%d)\n", line, __LINE__);
                return false;
            }
            break;

        case FindValue:
            switch (buf[pos]) {
            case ' ':
            case '\t':
                // Ignore leading whitespace.
                break;

            case '\n':
                // Empty value followed by a comment new line.
                state = NewLine;
                break;

            case '#':
                // Empty value followed by a comment.
                state = Comment;
                break;

            case '=':
                Slog2(SLOG2_ERROR).log("Parse error on line %d (%d)\n", line, __LINE__);
                return false;

            default:
                start = &buf[pos];
                state = Value;
                break;
            }
            break;

        case Value:
            switch (buf[pos]) {
            case ' ':
            case '\t':
                value = std::string(start, &buf[pos] - start);
                state = FindNewLine;
                break;

            case '\n':
                value = std::string(start, &buf[pos] - start);
                state = NewLine;
                break;

            case '#':
                value = std::string(start, &buf[pos] - start);
                state = Comment;
                break;

            default:
                // Next value character.
                break;
            }
            break;

        case Group:
            if (buf[pos] == ']') {
                // Create a new group.
                std::string group = std::string(start, &buf[pos] - start);
                keyMap = groups_[group];
                if (keyMap == nullptr) {
                    keyMap = new KeyMap;
                    groups_[group] = keyMap;
                }
                state = FindNewLine;
            } else if (isgraph(buf[pos])) {
                // Accumulate group name.
            } else if (buf[pos] == ' ') {
                // FIXME:
                // Allow all spaces?
            } else {
                Slog2(SLOG2_ERROR).log("Parse error on line %d (%d)\n", line, __LINE__);
                return false;
            }
            break;

        case FindNewLine:
            switch (buf[pos]) {
            case ' ':
            case '\t':
                // Ignore whitespace.
                break;

            case '\n':
                state = NewLine;
                break;

            case '#':
                state = Comment;
                break;

            default:
                state = Value;
                break;
            }
            break;

        case NewLine:
            if (!key.empty()) {
                if (keyMap == NULL) {
                    Slog2(SLOG2_ERROR).log("Key without group on line %d\n", line);
                    return false;
                }
                (*keyMap)[key] = value;
                key = "";
                value = "";
            }
            line++;
            state = FindKey;
            break;

        case Comment:
            if (buf[pos] == '\n') {
                state = NewLine;
            }
            break;
        }

        if (state != NewLine) {
            pos++;
        }
    }

    return true;
}

}
