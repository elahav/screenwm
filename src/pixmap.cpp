/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "pixmap.h"
#include "context.h"
#include "slog2.h"

namespace ScreenWM
{

Pixmap::Pixmap(screen_pixmap_t handle) : handle_(handle), autoDestroy_(false)
{
}

Pixmap::~Pixmap()
{
    if (autoDestroy_) {
        screen_destroy_pixmap(handle_);
    }
}

bool
Pixmap::setHandle(screen_pixmap_t handle, bool created)
{
    if (handle_) {
        Slog2(SLOG2_ERROR).log("Pixmap already attached\n");
        return false;
    }

    handle_ = handle;
    autoDestroy_ = created;
    return true;
}

bool
Pixmap::getProperty(int name, int values[]) const
{
    if (screen_get_pixmap_property_iv(handle_, name, values) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get pixmap property");
        dprint("name=%d\n", name);
        return false;
    }

    return true;
}

bool
Pixmap::getProperty(int name, void *values[]) const
{
    if (screen_get_pixmap_property_pv(handle_, name, values) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get pixmap property");
        dprint("name=%d\n", name);
        return false;
    }

    return true;
}

bool
Pixmap::setProperty(int name, int value)
{
    if (screen_set_pixmap_property_iv(handle_, name, &value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set pixmap property");
        return false;
    }

    return true;
}

bool
Pixmap::setProperty(int name, int values[])
{
    if (screen_set_pixmap_property_iv(handle_, name, values) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set pixmap property");
        return false;
    }

    return true;
}

bool
Pixmap::setProperty(int name, void *value)
{
    if (screen_set_pixmap_property_pv(handle_, name, &value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set pixmap property");
        return false;
    }

    return true;
}

bool
Pixmap::createBuffer(const Size &size)
{
    int sizeArray[2] = { size.width_, size.height_ };
    if (!setProperty(SCREEN_PROPERTY_BUFFER_SIZE, sizeArray)) {
        return false;
    }

    if (screen_create_pixmap_buffer(handle_) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to create pixmap buffer");
        return false;
    }

    return true;
}

Buffer
Pixmap::getBuffer() const
{
    screen_buffer_t handle;
    if (!getProperty(SCREEN_PROPERTY_BUFFERS, (void **)&handle)) {
        return Buffer();
    }

    return Buffer(handle);
}

void
Pixmap::destroy()
{
    if (handle_ != NULL) {
        dprint("Destroying %p\n", handle_);
        if (screen_destroy_pixmap(handle_) < 0) {
            Slog2(SLOG2_ERROR).perror("Failed to destroy pixmap");
        }
        handle_ = NULL;
    }
}

}
