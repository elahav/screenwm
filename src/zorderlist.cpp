/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "frame.h"

namespace ScreenWM
{

/**
 * Class constructor.
 */
ZOrderList::ZOrderList()
{
}

/**
 * Class destructor.
 */
ZOrderList::~ZOrderList()
{
}

/**
 * Adds a frame to the list.
 * The frame is assumed to be the active one, and so is added with the highest Z
 * order value at the end of the list. If the frame is already on the list with
 * a lower value, all frames with values between the old and new ones are
 * updated.
 * @param   frame   Frame to insert
 * @return  true if successful, false otherwise
 */
bool
ZOrderList::insert(Frame *frame)
{
    // Very naive algorithm for updating the Z-order when a frame becomes
    // active: go over the ordered list of frames, find the new active and
    // propagate it up the list, updating all frames in the middle. Can probably
    // do much better...
    int zorder;
    if (!bringToBack(frame, zorder)) {
        push_back(frame);
    }

    // Update the Z order of the inserted frame.
    frame->setZOrder(zorder);

    return true;
}

/**
 * Removes a frame from the list.
 * All frames with a Z-order value greater than the one being removed are
 * updated.
 * @param   frame   The frame to remove
 * @return  true if successful, false otherwise
 */
bool
ZOrderList::remove(Frame *frame)
{
    int zorder;
    if (bringToBack(frame, zorder)) {
        pop_back();
    }

    return true;
}

/**
 * Returns the frame at the given list position.
 * @param   idx     List index
 * @return  Frame at the requested position, or nullptr if the position is not
 *          occupied
 */
Frame *
ZOrderList::frameAt(unsigned idx) const
{
    Frame *frame;
    if (idx < size()) {
        frame = at(idx);
    } else {
        frame = nullptr;
    }

    return frame;
}

/**
 * Returns the frame at the top of the z-order list (the last frame on the
 * vector).
 * @return  The top frame or nullptr if the list is empty
 */
Frame *
ZOrderList::topFrame() const
{
    if (empty()) {
        return nullptr;
    }

    return back();
}

/**
 * Returns the frame that is a candidate to replace the top frame.
 * This frame is the next visible one in reverse order on the list.
 * @return  The next top frame or nullptr if the list has fewer than two
 *          elements
 */
Frame *
ZOrderList::nextTop() const
{
    criterator itr = rbegin();
    if (itr == rend()) {
        return nullptr;
    }

    // Skip the top frame.
    ++itr;

    // Find the next visible frame.
    while (itr != rend()) {
        if ((*itr)->isVisible()) {
            return *itr;
        }

        ++itr;
    }

    return nullptr;
}

/**
 * Finds the given frame and propagates it to the end of the list.
 * @note    Assumes that the list is locked
 * @param   frame   Frame to bring to the back
 * @param   zorder  Holds the Z-order value of the frame once it has been moved
 * @return  true if the frame was found, false otherwise
 */
bool
ZOrderList::bringToBack(Frame *frame, int &zorder)
{
    zorder = 1;
    for (iterator itr = begin(); itr != end(); ++itr) {
        if (*itr == frame) {
            if ((itr + 1) == end()) {
                return true;
            }

            Frame   *other = *(itr + 1);
            *(itr + 1) = frame;
            *itr = other;
            other->setZOrder(zorder);
        }

        zorder += 2;
    }

    return false;
}

}
