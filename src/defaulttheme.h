/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_DEFAULTTHEME_H
#define SCREENWM_DEFAULTTHEME_H

#include "theme.h"
#include <cairo/cairo.h>

namespace ScreenWM
{


/**
 * Implements the default theme for the Screen Window Manager.
 */
class DefaultTheme : public Theme
{
public:
    DefaultTheme() {}
    ~DefaultTheme() {}

    virtual Theme *create() { return new DefaultTheme; }
    virtual void destroy() { delete this; }
    virtual int getFormat() { return SCREEN_FORMAT_RGBA8888; }

    virtual void drawFrame(const Frame &frame, const Rect &rect, bool active);
    virtual void drawElement(const Frame &frame, Element element,
                             ButtonState state, Rect &rect);
    virtual int getElementSize(Element) const;
    virtual Element getElementFromPoint(const Frame &frame,
                                        const Point &pos) const;

protected:
    virtual bool init();

private:
    static cairo_surface_t  *closeImage_[2];
    static cairo_surface_t  *maximizeImage_[2];
    static cairo_surface_t  *minimizeImage_[2];

    void drawTab(const Frame &frame, double left, double top, double right,
                 double bottom, bool active, cairo_t *cr);
};

}

#endif
