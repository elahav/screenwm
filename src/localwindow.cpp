/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "localwindow.h"
#include "context.h"

namespace ScreenWM
{

LocalWindow::LocalWindow() : Window()
{
}

LocalWindow::~LocalWindow()
{
    if (handle_ != nullptr) {
        screen_destroy_window(handle_);
    }
}

/**
 * Create a native window for this object.
 * @param   flags   A combination of WindowData::Flags for this window
 */
bool
LocalWindow::create(uint64_t flags)
{
    if (!Context::getContext().createWindow(*this)) {
        return false;
    }

    WindowData *data = new WindowData;
    data->setLocalWindow(this);
    data->setFlags(flags);
    if (!setWindowData(data)) {
        return false;
    }

    return true;
}

/**
 * Allocates new screen memory buffers for this window.
 * @param   num Number of buffers to allocate (typically one or two, depending
 *              on the drawing mode)
 * @return  true if successful, false otherwise
 */
bool
LocalWindow::createBuffers(int num)
{
    if (screen_create_window_buffers(handle_, num) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to create window buffers");
        return false;
    }

    return true;
}

}
