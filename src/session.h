/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_SESSION_H
#define SCREENWM_SESSION_H

#include "screenwm.h"
#include "window.h"

namespace ScreenWM
{

/**
 * @brief   Sub-window input region
 *
 * A session describes input parameters used for an area within a window.
 * In the window manager, these are used to display a different cursor shape
 * when the mouse is over a frame's border.
 */
class Session
{
public:
    Session(screen_session_t handle = NULL);
    Session(const Session &other);
    ~Session();

    bool create(const Window &window, int cursorShape);
    bool getProperty(int name, void *values[]);
    bool setProperty(int name, int value);
    bool setProperty(int name, void *value);
    bool setGeometry(const Rect &rect);
    bool destroy();

    /**
     * Determines whether the session is associated with a native handle.
     */
    bool isValid() const { return handle_ != NULL; }

    operator screen_session_t() const { return handle_; }

protected:
    /**
     * Native session handle.
     */
    screen_session_t    handle_;
};

}

#endif
