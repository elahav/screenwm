/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_EVENT_H
#define SCREENWM_EVENT_H

#include "screenwm.h"
#include "window.h"
#include <string>

namespace ScreenWM
{

/**
 * @brief   Provides access to the properties of an event returned by
 *          screen_get_event()
 */
class Event
{
public:
    Event();
    ~Event();

    enum UserType {
        Activate,
        TaskbarUpdate
    };

    bool getProperty(int name, int &value) const;
    bool getProperty(int name, int values[]) const;
    bool getProperty(int name, void *values[]) const;
    bool getProperty(int name, Point &pos) const;
    bool setProperty(int name, int value);
    bool setProperty(int name, void *value);
    bool setProperty(int name, int values[]);
    bool setProperty(int name, void *values[]);
    int getType() const;
    bool setType(int type);
    Window getWindow() const;
    bool setWindow(const Window &window);
    bool getUserData(UserType &type, uintptr_t &value) const;
    bool setUserData(UserType type, uintptr_t value);
    bool getUserData(std::string &value) const;

    /**
     * @return  true if the event is associated with a valid handle, false
     *          otherwise
     */
    bool isValid() { return handle_ != NULL; }

    /**
     * Allows for static casting to the native handle.
     */
    operator screen_event_t() const { return handle_; }

private:
    /** Native screen handle. */
    screen_event_t  handle_;
};

}

#endif
