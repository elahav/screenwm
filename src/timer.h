/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_TIMER_H
#define SCREENWM_TIMER_H

#include <list>
#include <time.h>

namespace ScreenWM
{

/**
 * @brief   ScreenWM timer
 *
 * A list of timer objects is maintained by the window manager. The next timer
 * to expire is used for setting the timeout in Context::eventLoop(). The event
 * loop invokes the callbacks of all timers on the list that are past their
 * expiration time.
 */
class Timer
{
public:
    /** Value for an unarmed timer. */
    static constexpr uint64_t NO_TIMEOUT = ~0UL;

    enum TimeoutAbsRel {
        /** Timeout is specified as an absolute value. */
        Absolute,
        /** Timeout is specified relative to the current time. */
        Relative
    };

    /** Class constructor */
    Timer() : timeout_(NO_TIMEOUT) {}

    /** Class destructor. */
    virtual ~Timer() {}

    /** Function to invoke when the timer expires. */
    virtual void callback() {}

    void arm(uint64_t timeout, TimeoutAbsRel absrel);
    void disarm();

    static uint64_t nextTimeout(TimeoutAbsRel absrel);
    static void fire();

private:
    typedef std::list<Timer *> List;

    /** Timeout, in absolute nanoseconds. */
    uint64_t        timeout_;

    /** Position on the list. */
    List::iterator  itr_;

    /** Timer list, sorted by expiration time. */
    static List     list_;

    static void insert(Timer *timer);
};

}

#endif
