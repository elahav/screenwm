/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <dlfcn.h>
#include "defaulttheme.h"

namespace ScreenWM
{

typedef Theme * (*LoadTheme)(void);

Theme *Theme::globalTheme_ = NULL;

/**
 * Sets a new global theme.
 * @param   name    The name of the theme to set
 * @return  true if successful, false otherwise
 */
bool
Theme::setTheme(const char *name)
{
    if (globalTheme_) {
        delete globalTheme_;
        globalTheme_ = NULL;
    }

    Theme   *theme = NULL;
    if (strcmp(name, "default") != 0) {
        dprint("Loading %s\n", name);
        dlerror();
        void    *handle = dlopen(name, RTLD_NOW | RTLD_GLOBAL);
        if (handle != NULL) {
            dprint("Opened %s\n", name);
            LoadTheme func =
                reinterpret_cast<LoadTheme>(dlsym(handle, "LoadTheme"));
            if (func != NULL) {
                theme = func();
            } else {
                dprint("No symbol \"LoadTheme\"");
            }
        } else {
            dprint("Failed to load: %s\n", dlerror());
        }
    }

    if (theme == NULL) {
        theme = new DefaultTheme;
    }

    if (!theme->init()) {
        delete theme;
        return false;
    }

    globalTheme_ = theme;
    return true;
}

/**
 * Returns a pointer to the currently selected theme.
 */
Theme *
Theme::getTheme()
{
    return globalTheme_->create();
}

}
