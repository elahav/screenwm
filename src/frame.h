/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_FRAME_H
#define SCREENWM_FRAME_H

#include "screenwm.h"
#include "localwindow.h"
#include "context.h"
#include "zorderlist.h"
#include "session.h"
#include <string>

namespace ScreenWM
{

class Theme;

/**
 * @brief   A frame to control a top-level window.
 *
 * A frame is a window that draws a title bar and a border around a top-level
 * window. These are used to control the position and size of that window.
 */
class Frame : public LocalWindow
{
public:
    Frame(Window& window, bool managed);
    ~Frame();

    bool create();
    bool draw();
    bool setPosition(Point &pos);
    bool setPositionFromWindow();
    std::string getTitle() const { return title_; }
    void setTitle(std::string title);
    bool handlePointer(const Event *event, int buttons) override;
    bool updateGeometry();
    bool setActive(bool active);
    bool raise();
    bool unraise();
    bool show();
    bool hide();
    const Window &getWindow() const { return window_; }

    bool isVisible() const { return (state_ & (Hidden | Minimized)) == 0; }

    static const ZOrderList& zorderList() { return zorderList_; }

    static Frame    *NoFrame;

private:
    enum State
    {
        Normal = 0x0,
        Maximized = 0x1,
        Minimized = 0x2,
        Hidden = 0x4,
        Dragged = 0x8,
        ResizingBottom = 0x10,
        ResizingLeft = 0x20,
        ResizingRight = 0x40,
        Resizing = ResizingBottom | ResizingLeft | ResizingRight
    };

    /**
     * The controlled window.
     */
    Window      window_;

    /** Whether the frame is currently active (raised). */
    bool        active_;

    /** Window title. */
    std::string title_;

    /**
     * Whether the frame handles a managed window (one that knows about the
     * window manager protocol).
     */
    bool        managed_;

    /**
     * When dragged, keeps track of the last mouse pointer position.
     * Used to determine the difference in position to apply to both the frame
     * and the controlled window.
     */
    Point       dragPos_;

    /**
     * The current state (pressed or not) of the close button.
     */
    ButtonState closeButtonState_;

    /**
     * The current state (pressed or not) of the maximize button.
     */
    ButtonState maximizeButtonState_;

    /**
     * The current state (pressed or not) of the minimize button.
     */
    ButtonState minimizeButtonState_;

    /**
     * Position and size to restore to when the window is un-maximized.
     */
    Rect        normalGeometry_;

    /**
     * Current window state (normal, maximized or minimized).
     */
    int         state_;

    /**
     * The element (button, title, border) that was last clicked.
     */
    int         activeElement_;

    /**
     * The Z order value of the frame.
     */
    int         zorder_;

    /**
     * Left-border input session.
     */
    Session     leftSession_;

    /**
     * Right-border input session.
     */
    Session     rightSession_;

    /**
     * Right-border input session.
     */
    Session     bottomSession_;

    /**
     * Theme information.
     */
    Theme       *theme_;

    /**
     * A list of all frames ordered by their relative Z-order (depth) value.
     */
    friend class        ZOrderList;
    static ZOrderList   zorderList_;

    void closeButtonClicked(int buttons);
    void maximizeButtonClicked(int buttons);
    void minimizeButtonClicked(int buttons);
    bool drag(const Point &pos, int buttons);
    bool resize(const Point &pos, int buttons, unsigned flags);
    bool setGeometry(const Rect &rect, bool resizeBuffer = true);
    bool setGeometryFromWindow(Rect &rect);
    bool setZOrder(int zorder);
    bool createSessions();
    bool updateSessions(const Rect& rect);
    void setSessionCursors(bool enable);
};

}

#endif
