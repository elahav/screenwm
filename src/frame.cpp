/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "frame.h"
#include "theme.h"

namespace ScreenWM
{

ZOrderList      Frame::zorderList_;
Frame          *Frame::NoFrame = reinterpret_cast<Frame *>(1);

/**
 * Class constructor.
 * Note that you need to call @create() in order to realize the frame's window.
 */
Frame::Frame(Window& window, bool managed)
    : LocalWindow(),
      window_(window),
      active_(false),
      managed_(managed),
      closeButtonState_(ButtonState::Normal),
      maximizeButtonState_(ButtonState::Normal),
      minimizeButtonState_(ButtonState::Normal),
      state_(Normal),
      activeElement_(-1),
      theme_(nullptr)
{
}

Frame::~Frame()
{
    if (theme_) {
        theme_->destroy();
    }
    leftSession_.destroy();
    rightSession_.destroy();
    bottomSession_.destroy();
    zorderList_.remove(this);
}

/**
 * Creates and initializes the native window for the frame.
 * @return  true if successful, false othewise
 */
bool
Frame::create()
{
    // Create the native window.
    if (!LocalWindow::create(WindowData::Frameless)) {
        return false;
    }

    if (!joinGroup("screenwm")) {
        return false;
    }

    // Associate with a theme object.
    theme_ = Theme::getTheme()->create();
    if (theme_ == NULL) {
        return false;
    }

    // Set frame geometry to follow the child window's.
    Rect    rect;
    if (!setGeometryFromWindow(rect)) {
        return false;
    }

    // Set format and usage.
    if (!setProperty(SCREEN_PROPERTY_USAGE,
                     SCREEN_USAGE_WRITE | SCREEN_USAGE_NATIVE)) {
        return false;
    }

    if (!setProperty(SCREEN_PROPERTY_FORMAT, theme_->getFormat())) {
        return false;
    }

    if (!setProperty(SCREEN_PROPERTY_SENSITIVITY,
                     SCREEN_SENSITIVITY_MASK_NO_FOCUS)) {
        return false;
    }

    // Create the window buffer.
    if (!createBuffers(1)) {
        return false;
    }

    if (!createSessions()) {
        return false;
    }

    if (!updateSessions(rect)) {
        return false;
    }

    // If the frame's title is not already set, use the window's ID string.
    if (title_ == "") {
        char  title[64];
        if (window_.getProperty(SCREEN_PROPERTY_ID_STRING, title, sizeof(title))
            && (title[0] != '\0')) {
            title_= title;
        } else {
            title_ = "Untitled";
        }
    }

    return true;
}

/**
 * Draws the frame using the current theme.
 */
bool
Frame::draw()
{
    // Get the frame's size.
    Rect    rect;
    if (!getGeometry(rect)) {
        return false;
    }

    theme_->drawFrame(*this, rect, active_);
    return post(RectArray());
}

/**
 * Changes the position of the frame and, along with it, of the controlled
 * window.
 * @param   x   New horizontal position, relative to the root window
 * @param   y   New vertical position, relative to the root window
 * @return  true if successful, false otherwise
 */
bool
Frame::setPosition(Point &pos)
{
    // Re-position the frame.
    if (!Window::setPosition(pos)) {
        return false;
    }

    // Re-position the controlled window.
    pos.x_ += theme_->getElementSize(Theme::LeftFrame);
    pos.y_ += theme_->getElementSize(Theme::TitleBar);
    if (!window_.setPosition(pos)) {
        return false;
    }

    return true;
}

/**
 * Repositions a frame to follow the position of its window.
 * @return  true if successful, false otherwise
 */
bool
Frame::setPositionFromWindow()
{
    Rect    rect;
    return setGeometryFromWindow(rect);
}

/**
 * Sets a new title for the frame.
 * Redraws the frame if it is already fully created.
 * @param   title   New title
 */
void
Frame::setTitle(std::string title)
{
    title_ = title;
    if (handle_ != nullptr) {
        draw();
        Context::getContext().windowTitleChanged(*this);
    }
}

/**
 * Reacts to mouse pointer events.
 * @param   event   Mouse pointer event properties
 * @param   buttons Mouse button state
 */
bool
Frame::handlePointer(const Event *event, int buttons)
{
    // Get the current pointer position.
    // Always use absolute screen coordinates, as some events do not have the
    // right source position.
    Point pos;
    if (!event->getProperty(SCREEN_PROPERTY_POSITION, pos)) {
        return false;
    }

    if (!active_ && (buttons & SCREEN_LEFT_MOUSE_BUTTON)) {
        Context::getContext().activate(this);
    }

    if (state_ & Dragged) {
        return drag(pos, buttons);
    }

    if (state_ & Resizing) {
        return resize(pos, buttons, state_ & Resizing);
    }

    Point   relPos;
    if (!getPosition(relPos)) {
        return false;
    }

    // Dispatch to element-specific handling.
    // Need to translate the absolute pointer position to a window-relative one.
    relPos.x_ = pos.x_ - relPos.x_;
    relPos.y_ = pos.y_ - relPos.y_;

    // Determine the frame element that was clicked.
    int element = theme_->getElementFromPoint(*this, relPos);

    if (element != activeElement_) {
        // Handle the case of the mouse leaving the active element without
        // finishing the action.
        switch (activeElement_) {
        case Theme::CloseButton:
            closeButtonClicked(-1);
            break;
        case Theme::MaximizeButton:
            maximizeButtonClicked(-1);
            break;
        case Theme::MinimizeButton:
            minimizeButtonClicked(-1);
            break;
        default:
            break;
        }

        activeElement_ = element;
    }

    // Element-specific handling.
    switch (element) {
    case Theme::CloseButton:
        closeButtonClicked(buttons);
        break;

    case Theme::MaximizeButton:
        maximizeButtonClicked(buttons);
        break;

    case Theme::MinimizeButton:
        minimizeButtonClicked(buttons);
        break;

    case Theme::TitleBar:
        return drag(pos, buttons);

    case Theme::BottomFrame:
        return resize(pos, buttons, ResizingBottom);

    case Theme::LeftFrame:
        return resize(pos, buttons, ResizingLeft);

    case Theme::RightFrame:
        return resize(pos, buttons, ResizingRight);

    default:
        break;
    }

    state_ &= ~Dragged;
    return true;
}

/**
 * Raise or lower the frame.
 * @param   active  Whether the frame becomes active or inactive
 * @return  true if successful, false otherwise
 */
bool
Frame::setActive(bool active)
{
    dprint("Frame %p setActive: %d (%d)\n", handle_, active, active_);

    // Check if anything has changed.
    if (active_ == active) {
        // If the last actiation was from a temporary raising of the frame it
        // now needs to be get its proper Z order value.
        if (zorder_ & 0x1000) {
            zorderList_.insert(this);
        }
        return true;
    }

    // Check for deactivation.
    // If visibile, redraw the frame to reflect the change.
    if (!active) {
        active_ = false;
        if (isVisible()) {
            draw();
        }

        return true;
    }

    // Restore the window if minimized.
    if (state_ & Minimized) {
        window_.setProperty(SCREEN_PROPERTY_VISIBLE, 1);
        setProperty(SCREEN_PROPERTY_VISIBLE, 1);
        state_ &= ~Minimized;
    }

    // Give the frame its Z order value.
    zorderList_.insert(this);

    active_ = true;
    draw();

    return true;
}

/**
 * Temporarily brings a frame to the foreground.
 * This is used for cycling through frames (e.g., with ALT+TAB) without
 * permanently making the frame the active one.
 */
bool
Frame::raise()
{
    setZOrder(zorder_ | 0x1000);
    active_ = true;

    if (state_ & Minimized) {
        window_.setProperty(SCREEN_PROPERTY_VISIBLE, 1);
        setProperty(SCREEN_PROPERTY_VISIBLE, 1);
        state_ &= ~Minimized;
    }

    draw();
    return true;
}

/**
 * Undo a raise() call, restoring the frame to its previous position in the Z
 * order.
 */
bool
Frame::unraise()
{
    setZOrder(zorder_ & ~0x1000);
    active_ = false;
    draw();
    return true;
}

/**
 * Makes the frame visible.
 * @return  true if successful, false otherwise
 */
bool
Frame::show()
{
    if ((state_ & Hidden) == 0) {
        // No change.
        return true;
    }

    if (!setProperty(SCREEN_PROPERTY_VISIBLE, 1)) {
        return false;
    }

    state_ &= ~Hidden;
    zorderList_.insert(this);
    return true;
}

/**
 * Makes the frame invisible.
 * @return  true if successful, false otherwise
 */
bool
Frame::hide()
{
    if ((state_ & Hidden) != 0) {
        // No change.
        return true;
    }

    if (!setProperty(SCREEN_PROPERTY_VISIBLE, 0)) {
        return false;
    }

    state_ |= Hidden;
    zorderList_.remove(this);
    return true;
}

/**
 * Update the Z-Order value of the frame, which determines the way windows are
 * arranged on top of each other.
 * The given value is for the frame, while the controlled window is given the
 * value @zorder + 1, in order to display it above the frame.
 * @param   zorder  New value for the frame
 * @return  true if successful, false otherwise
 */
bool
Frame::setZOrder(int zorder)
{
    dprint("Frame %p zorder: %d\n", handle_, zorder);

    // Apply Z order value to the frame.
    if (!setProperty(SCREEN_PROPERTY_ZORDER, zorder)) {
        return false;
    }

    // Apply Z order value to the controlled window.
    if (!window_.setProperty(SCREEN_PROPERTY_ZORDER, zorder + 1)) {
        return false;
    }

    zorder_ = zorder;
    return true;
}

/**
 * Handle a mouse event over the frame's close button.
 * @param   buttons Which mouse buttons are currently pressed
 *          -1 to release the button without performing the action
 */
void
Frame::closeButtonClicked(int buttons)
{
    ButtonState state;
    bool close = false;

    // Check for a button state change.
    if (buttons == SCREEN_LEFT_MOUSE_BUTTON) {
        state = ButtonState::Pressed;
    } else if (buttons == 0) {
        state = ButtonState::Normal;
        close = true;
    } else {
        state = ButtonState::Normal;
    }

    if (state == closeButtonState_) {
        return;
    }

    Rect    rect;
    theme_->drawElement(*this, Theme::CloseButton, state, rect);

    // TODO:
    // Only post the modified rectangle.
    post(RectArray());
    Context::getContext().flush();

    closeButtonState_ = state;

    // Attempt to close the controlled window.
    if (close) {
        window_.close(managed_);
    }
}

/**
 * Handle a mouse event over the frame's maximize button.
 * @param   buttons Which mouse buttons are currently pressed
 *          -1 to release the button without performing the action
 */
void
Frame::maximizeButtonClicked(int buttons)
{
    ButtonState buttonState;
    bool maximize = false;

    // Check for a button state change.
    if (buttons == SCREEN_LEFT_MOUSE_BUTTON) {
        buttonState = ButtonState::Pressed;
    } else if (buttons == 0) {
        buttonState = ButtonState::Normal;
        maximize = true;
    } else {
        buttonState = ButtonState::Normal;
    }

    if (buttonState == maximizeButtonState_) {
        return;
    }

    dprint("Button state=%d Window state=%d\n", buttonState, state_);

    // Redraw the button.
    Rect    rect;
    theme_->drawElement(*this, Theme::MaximizeButton, buttonState, rect);

    maximizeButtonState_ = buttonState;

    if (maximize) {
        // Button released, change size.
        if (!(state_ & Maximized)) {
            // Maximize the frame.
            getGeometry(normalGeometry_);
            Context::getContext().getDesktopArea(rect);
            state_ |= Maximized;
            setSessionCursors(false);
            dprint("Window maximized\n");
        } else {
            // Restore previous frame geometry.
            rect = normalGeometry_;
            state_ &= ~Maximized;
            setSessionCursors(true);
            dprint("Window restored\n");
        }

        setGeometry(rect);
        theme_->drawFrame(*this, rect, active_);
        updateSessions(rect);

        // Cancel dragging.
        state_ &= ~Dragged;
    }

    // TODO:
    // Only post the modified rectangles.
    post(RectArray());
    Context::getContext().flush();
}

/**
 * Handle a mouse event over the frame's minimize button.
 * @param   buttons Which mouse buttons are currently pressed
 *          -1 to release the button without performing the action
 */
void
Frame::minimizeButtonClicked(int buttons)
{
    ButtonState buttonState;
    bool minimize = false;

    // Check for a button state change.
    if (buttons == SCREEN_LEFT_MOUSE_BUTTON) {
        buttonState = ButtonState::Pressed;
    } else if (buttons == 0) {
        buttonState = ButtonState::Normal;
        minimize = true;
    } else {
        buttonState = ButtonState::Normal;
    }

    if (buttonState == minimizeButtonState_) {
        return;
    }

    dprint("Button state=%d Window state=%d\n", buttonState, state_);

    // Redraw the button.
    Rect    rect;
    theme_->drawElement(*this, Theme::MinimizeButton, buttonState, rect);

    minimizeButtonState_ = buttonState;

    if (minimize) {
        // Button released, hide the window.
        window_.setProperty(SCREEN_PROPERTY_VISIBLE, 0);
        setProperty(SCREEN_PROPERTY_VISIBLE, 0);
        state_ |= Minimized;
        Context::getContext().activate(zorderList_.nextTop());
    }

    // TODO:
    // Only post the modified rectangles.
    post(RectArray());
    Context::getContext().flush();
}

/**
 * Handle a mouse event over the frame's title bar.
 * If the left mouse button is pressed, the frame's position changes according
 * to the position of the pointer relative to its previous one from a call to
 * this method.
 * @param   event   Event parameters
 * @param   buttons Which mouse buttons are currently pressed
 * @return  true if successful, false otherwise
 */
bool
Frame::drag(const Point &pos, int buttons)
{
    // If not from a left mouse button, discard the last drag action.
    if (buttons != SCREEN_LEFT_MOUSE_BUTTON) {
        state_ &= ~Dragged;
        return true;
    }

    // Do not drag a maximized frame.
    if (state_ & Maximized) {
        return true;
    }

    // Check for first click. If so, just record the current position of the
    // pointer.
    if (!(state_ & Dragged)) {
        dragPos_ = pos;
        state_ |= Dragged;
        return true;
    }

    // Calculate the diff from the last position and move the frame accordingly.
    Rect    rect;
    if (!getGeometry(rect)) {
        return false;
    }

    rect.x_ += (pos.x_ - dragPos_.x_);
    rect.y_ += (pos.y_ - dragPos_.y_);

    // Change the frame's position (and with it the controlled window's).
    if (!setGeometry(rect, false)) {
        return false;
    }

    Context::getContext().flush();

    // Remember the new position.
    dragPos_ = pos;
    return true;
}

/**
 * Handle a mouse event over one of the frame's edges.
 * @param   event   Event parameters
 * @param   buttons Which mouse buttons are currently pressed
 * @param   flags   The resizing direction
 * @return  true if successful, false otherwise
 */
bool
Frame::resize(const Point &pos, int buttons, unsigned flags)
{
    // Check if any resizing operation should be performed.
    if ((buttons != SCREEN_LEFT_MOUSE_BUTTON) && !(state_ & Resizing)) {
        return true;
    }

    // Do not resize a maximized frame.
    if (state_ & Maximized) {
        return true;
    }

    Rect    rect;
    if (!getGeometry(rect)) {
        return false;
    }

    // Finish a resize operation when the left mouse button is released.
    if (buttons != SCREEN_LEFT_MOUSE_BUTTON) {
        state_ &= ~Resizing;

        // Update the size, including the buffer.
        setGeometry(rect);
        updateSessions(rect);

        // Redraw the frame and window in their new size.
        theme_->drawFrame(*this, rect, active_);
        post(RectArray());
        return true;
    }

    if ((state_ & Resizing) == 0) {
        dprint("resize %x\n", flags);
        state_ |= flags;
    }

    // Calculate a new geometry.
    if (flags & ResizingBottom) {
        rect.height_ = pos.y_ - rect.y_;
    }

    if (flags & ResizingLeft) {
        const int   right = rect.x_ + rect.width_;
        rect.x_ = pos.x_;
        rect.width_ = right - rect.x_;
    }

    if (flags & ResizingRight) {
        rect.width_ = pos.x_ - rect.x_;
    }

    // Resize the frame and the controlled window.
    setGeometry(rect, false);
    Context::getContext().flush();
    return true;
}

/**
 * Sets a new position and size for the frame and its managed window.
 * @param   rect    New frame geometry
 * @return  true if successful, false otherwise
 */
bool
Frame::setGeometry(const Rect &rect, bool resizeBuffer)
{
    // Update and redraw the frame.
    Window::setGeometry(rect);

    if (resizeBuffer) {
        int size[2] = { rect.width_, rect.height_ };
        setProperty(SCREEN_PROPERTY_BUFFER_SIZE, size);
    }

    int const   top = theme_->getElementSize(Theme::TitleBar);
    int const   left = theme_->getElementSize(Theme::LeftFrame);
    int const   right = theme_->getElementSize(Theme::RightFrame);
    int const   bottom = theme_->getElementSize(Theme::BottomFrame);

    // Set the controlled window geometry.
    Rect    windowRect = {
        .x_ = rect.x_ + left,
        .y_ = rect.y_ + top,
        .width_ = rect.width_ - (left + right),
        .height_ = rect.height_ - (top + bottom)
    };

    return window_.setGeometry(windowRect);
}

/**
 * Set the frame's position and size to match that of the controlled window.
 * @param   rect    Filled with the frame's new position and size, on return
 * @return  true if successful, false otherwise
 */
bool
Frame::setGeometryFromWindow(Rect &rect)
{
    // Get the controlled window geometry.
    if (!window_.getGeometry(rect)) {
        return false;
    }

    bool    updateWindow = false;
    Rect    desktopRect;
    Context::getContext().getDesktopArea(desktopRect);

    // Set frame geometry.
    int     top = theme_->getElementSize(Theme::TitleBar);
    int     left = theme_->getElementSize(Theme::LeftFrame);
    int     right = theme_->getElementSize(Theme::RightFrame);
    int     bottom = theme_->getElementSize(Theme::BottomFrame);

    // If the window does not have an explicit position, give it a default value
    // that does not intersect with a dock.
    // FIXME:
    // Use some algorithm to find a good position for the new window.
    if ((rect.x_ == 0) && (rect.y_ == 0)) {
        rect.x_ = desktopRect.x_ + 10;
        rect.y_ = desktopRect.y_ + 10;
        updateWindow = true;
    } else {
        rect.x_ -= left;
        rect.y_ -= top;
    }

    rect.width_ += (left + right);
    rect.height_ += (top + bottom);

    // Clip geometry to window size.
    // If the window size is greater than the desktop size then treat the window
    // as initially maximized.
    int     maximize = 0;
    if (rect.width_ >= desktopRect.width_) {
        rect.width_ = desktopRect.width_;
        updateWindow = true;
        maximize++;
    }

    if (rect.height_ >= desktopRect.height_) {
        rect.height_ = desktopRect.height_;
        updateWindow = true;
        maximize++;
    }

    if (maximize == 2) {
        rect.x_ = desktopRect.x_;
        rect.y_ = desktopRect.y_;
        state_ |= Maximized;
        normalGeometry_ = rect;
    }

    if (updateWindow) {
        // Set both the frame size and the managed window's size.
        return setGeometry(rect);
    } else {
        return Window::setGeometry(rect);
    }
}

/**
 * Initialize the input sessions used to display resize cursors over the frame's
 * borders.
 * Note that in order for these to work the cursor shapes need to be initialized
 * in screen's configuration file. The cursor shape names are arbitrary and
 * should be assigned bitmaps that portray a horizontal double-sided arrow (for
 * "home") and a vertical double-sided arrow (for "menu").
 * @return  true if successful, false otherwise
 */
bool
Frame::createSessions()
{
    if (!leftSession_.create(*this, SCREEN_CURSOR_SHAPE_HOME)) {
        return false;
    }

    if (!rightSession_.create(*this, SCREEN_CURSOR_SHAPE_MENU)) {
        return false;
    }

    if (!bottomSession_.create(*this, SCREEN_CURSOR_SHAPE_CROSS)) {
        return false;
    }

    return true;
}

/**
 * Changes the geometry of the various sessions to follow a resize of the frame.
 * @param   rect    New frame geometry
 * @return  true if successful, false otherwise
 */
bool
Frame::updateSessions(const Rect &rect)
{
    Rect    sessionRect;

    sessionRect.x_ = 0;
    sessionRect.y_ = 0;
    sessionRect.width_ = theme_->getElementSize(Theme::LeftFrame);
    sessionRect.height_ = rect.height_;

    if (!leftSession_.setGeometry(sessionRect)) {
        return false;
    }

    sessionRect.width_ = theme_->getElementSize(Theme::RightFrame);
    sessionRect.x_ = rect.width_ - sessionRect.width_;

    if (!rightSession_.setGeometry(sessionRect)) {
        return false;
    }

    sessionRect.x_ = 0;
    sessionRect.width_ = rect.width_;
    sessionRect.height_ = theme_->getElementSize(Theme::BottomFrame);
    sessionRect.y_ = rect.height_ - sessionRect.height_;

    if (!bottomSession_.setGeometry(sessionRect)) {
        return false;
    }

    return true;
}

/**
 * Update the per-session resize cursors when a window is maximized/restored.
 * @param   enable  true enable resize cursors, false to disable
 */
void
Frame::setSessionCursors(bool enable)
{
    if (enable) {
        leftSession_.setProperty(SCREEN_PROPERTY_CURSOR,
                                 SCREEN_CURSOR_SHAPE_HOME);

        rightSession_.setProperty(SCREEN_PROPERTY_CURSOR,
                                  SCREEN_CURSOR_SHAPE_MENU);

        bottomSession_.setProperty(SCREEN_PROPERTY_CURSOR,
                                   SCREEN_CURSOR_SHAPE_HAND);

    } else {
        leftSession_.setProperty(SCREEN_PROPERTY_CURSOR,
                                 SCREEN_CURSOR_SHAPE_NONE);

        rightSession_.setProperty(SCREEN_PROPERTY_CURSOR,
                                  SCREEN_CURSOR_SHAPE_NONE);

        bottomSession_.setProperty(SCREEN_PROPERTY_CURSOR,
                                   SCREEN_CURSOR_SHAPE_NONE);
    }
}

}
