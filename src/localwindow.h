/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_LOCAL_WINDOW_H
#define SCREENWM_LOCAL_WINDOW_H

#include "screenwm.h"
#include "window.h"
#include "event.h"
#include "slog2.h"

namespace ScreenWM
{

/**
 * @brief   A class for Windows created by the window manager
 *
 * The window manager mostly deals with windows created by other processes.
 * These are associated with temporary Window objects that wrap the native
 * Screen window handle for as long as access to the handle is needed by the
 * manager.
 * However, there are several windows created by the manager itself, for which
 * it needs Window objects that last for as long as the window exists. Examples
 * include:
 * - The root window, used to display the background
 * - Frames for managed windows
 * - Menus, task bars, tool tips, etc.
 */
class LocalWindow : public Window
{
public:
    LocalWindow();
    virtual ~LocalWindow();

    bool create(uint64_t flags);
    bool createBuffers(int num);

    virtual bool handlePointer(const Event *event, int buttons) { return true; }
};

}

#endif
