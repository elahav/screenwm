/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "slog2.h"
#include <iostream>
#include <cstdarg>
#include <cstring>
#include <cerrno>

namespace ScreenWM
{

slog2_buffer_t  Slog2::handle_;

/**
 * Register with slogger2.
 * @param   level   The minimum level to log
 * @return  true if successful, false otherwise
 */
bool
Slog2::init(int level)
{
    slog2_buffer_set_config_t   config;
    config.buffer_set_name = "screenwm";
    config.num_buffers = 1;
    config.verbosity_level = level;
    config.buffer_config[0].buffer_name = "log";
    config.buffer_config[0].num_pages = 4;

    if (slog2_register(&config, &handle_, 0) == -1) {
        std::cerr << "Failed to register slog2" << std::endl;
        return false;
    }

    return true;
}

/**
 * Log a formatted string, using the object's level.
 * @param   fmt Format string
 */
void
Slog2::log(const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    vslog2f(handle_, 0, level_, fmt, args);
    va_end(args);
}

/**
 * Emulates a perror() call, by printing a message followed by the relevant
 * error string.
 * @param   msg     Message string
 * @param   err     (Optional) error code. If not specified, uses errno.
 */
void
Slog2::perror(const char *msg, int err)
{
    if (err == -1) {
        err = errno;
    }

    slog2f(handle_, 0, level_, "%s: %s", msg, strerror(err));
}

}
