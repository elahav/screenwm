/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <unistd.h>
#include "context.h"
#include "window.h"
#include "frame.h"
#include "theme.h"
#include "settings.h"
#include "startup.h"
#include "slog2.h"
#include "display.h"
#include "../resmgr/resmgr.h"
#include "../public/screenwm.h"
#include "../taskbar/taskbar.h"

namespace ScreenWM
{

Context *Context::globalContext_ = nullptr;

/**
 * @brief   Timer that expires when there is no activity
 */
class IdleTimer : public Timer
{
public:
    IdleTimer(uint64_t idle_ns) : Timer(), idle_ns_(idle_ns) {}

    void callback() {
        Context::getContext().idleTimeout();
    }

    void arm() {
        Timer::arm(idle_ns_, Timer::Relative);
    }

private:
    uint64_t idle_ns_;
};

/**
 * Class constructor.
 */
Context::Context() : handle_(nullptr),
                     event_(),
                     needFlush_(false),
                     activeFrame_(nullptr),
                     startup_(nullptr),
                     lockerWindow_(0),
                     idleTimer_(nullptr),
                     isIdle_(false),
                     taskbar_(nullptr)
{
    // Initialize the context.
    if (!init()) {
        destroy();
        return;
    }

    // Initialize the theme engine.
    if (!Theme::setTheme("./theme.so")) {
        destroy();
        return;
    }
}

/**
 * Main event loop.
 * Only returns when an unrecoverable error has been encountered.
 */
int
Context::eventLoop()
{
    Slog2(SLOG2_INFO).log("Starting");

    for (;;) {
        // TODO:
        // Set timeout based on whether a flush is needed, so that all changes
        // are flushed together at a regular interval.
        if (screen_get_event(handle_,
                             static_cast<screen_event_t>(event_),
                             Timer::nextTimeout(Timer::Relative))
            < 0) {
            Slog2(SLOG2_ERROR).perror("Failed to get event");
            break;
        }

        if (!handleEvent()) {
            Slog2(SLOG2_ERROR).log("Failed to handle event %d",
                                   event_.getType());
#ifdef NDEBUG
            // There are still occasional failures in handling various events.
            // Don't exit ScreenWM unless in debug mode.
            continue;
#else
            break;
#endif
        }

        // TODO:
        // Flush on interval (SCREEN_EVENT_NONE) instead.
        if (needFlush_) {
            screen_flush_context(handle_, 0);
            needFlush_ = false;
        }
    }

    return 1;
}

/**
 * Creates a new native window and associates the resulting handle with the
 * given Window object.
 * A Window object initialized this way has the native window automatically
 * destroyed in its destructor.
 * @param   window  Window object to associate the native window with
 * @return  true if successful, false otherwise
 */
bool
Context::createWindow(Window &window)
{
    screen_window_t winHandle;

    // Create the native window.
    if (screen_create_window(&winHandle, handle_) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to create window");
        return false;
    }

    // Associate handle with Window object.
    if (!window.setHandle(winHandle)) {
        screen_destroy_window(winHandle);
        return false;
    }

    return true;
}

/**
 * Creates a new native pixmap for off-screen rendering and associates the
 * resulting handle with the given Pixmap object.
 * @param   pixmap  Pixmap object to associate the native pixmap with
 * @return  true if successful, false otherwise
 */
bool
Context::createPixmap(Pixmap &pixmap)
{
    screen_pixmap_t pixHandle;

    // Create the native pixmap.
    if (screen_create_pixmap(&pixHandle, handle_) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to create pixmap");
        return Pixmap();
    }

    // Associate handle with Pixmap object.
    if (!pixmap.setHandle(pixHandle, true)) {
        screen_destroy_pixmap(pixHandle);
        return false;
    }

    return true;
}

bool
Context::createSession(int type, Session& session)
{
    screen_session_t    sessionHandle;
    if (screen_create_session_type(&sessionHandle, handle_, type) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to create session");
        return false;
    }

    session = Session(sessionHandle);
    return true;
}

/**
 * Fills a rectangle with the geometry of a maximized window.
 * @param   rect    Rectangle object to fill
 */
void
Context::getDesktopArea(Rect &rect) const
{
    rootWindow_.getDesktopArea(rect);
}

/**
 * Updates a destination buffer with the contents of a source buffer.
 * @param   dst     Destination buffer
 * @param   src     Source buffer
 * @param   attribs List of attributes controlling the operation
 */
void
Context::blit(const Buffer &dst, const Buffer &src, int attribs[])
{
    if (screen_blit(handle_, dst, src, attribs) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to blit");
    }
}

/**
 * Fills the destination buffer with a solid colour.
 * @param   dst     Destination buffer
 * @param   attribs List of attributes controlling the operation
 */
void
Context::fill(const Buffer &dst, int attribs[])
{
    if (screen_fill(handle_, dst, attribs) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to fill");
    }
}

/**
 * Sets the given frame as the active one.
 * The frame is raised and the previously active frame, if any, is lowered.
 * @param   frame   The frame to make active.
 */
void
Context::activate(Frame *frame)
{
    if (frame == activeFrame_) {
        if (activeFrame_) {
            // Make sure the frame is displayed as active.
            activeFrame_->setActive(true);
        }
        return;
    }

    if (activeFrame_) {
        activeFrame_->setActive(false);
    }

    if (frame) {
        frame->setActive(true);
        rootWindow_.setFocus(frame->getWindow());
    }

    activeFrame_ = frame;

    // Notify the resource manager.
    screen_window_t const   winHandle =
        frame ? static_cast<screen_window_t>(*frame) : nullptr;
    screenwm_resmgr_deliver_event(SCREENWM_EVENT_WINDOW_LIST,
                                  SCREENWM_WINDOW_LIST_ACTIVE_CHANGED,
                                  winHandle);

    flush();
}

/**
 * Sends a screen event to a given process.
 * @param   event   The event to send
 * @param   pid     The destination process, or 0 for the local process (for
 *                  internal window manager events).
 * @return  true if successful, false otherwise
 */
bool
Context::sendEvent(Event &event, pid_t pid)
{
    if (pid == 0) {
        pid = getpid();
    }

    if (screen_send_event(handle_, static_cast<screen_event_t>(event),
                          pid) < 0) {
        Slog2(SLOG2_ERROR).perror("screen_send_event");
        return false;
    }

    return true;
}

/**
 * Sends a screen event to a given window.
 * @param   event   The event to send
 * @param   window  The destination window
 * @return  true if successful, false otherwise
 */
bool
Context::sendEvent(Event &event, Window window)
{
    pid_t   pid;
    if (!window.getProperty(SCREEN_PROPERTY_OWNER_PID, &pid)) {
        return false;
    }

    if (!event.setProperty(SCREEN_PROPERTY_WINDOW,
                           static_cast<screen_window_t>(window))) {
        return false;
    }

    if (screen_send_event(handle_, static_cast<screen_event_t>(event),
                          pid) < 0) {
        Slog2(SLOG2_ERROR).perror("screen_send_event");
        return false;
    }

    return true;
}

bool
Context::getProperty(int name, int &value) const
{
    if (screen_get_context_property_iv(handle_, name, &value) == -1) {
        Slog2(SLOG2_ERROR).perror("screen_get_context_property_iv");
        return false;
    }

    return true;
}

bool
Context::getProperty(int name, void *values[]) const
{
    if (screen_get_context_property_pv(handle_, name, values) == -1) {
        Slog2(SLOG2_ERROR).perror("screen_get_context_property_pv");
        return false;
    }

    return true;
}

/**
 * Informs the resource manager that a window's title has changed.
 * @param   frame   The frame for which the title has changed
 */
void
Context::windowTitleChanged(const Frame &frame)
{
    screenwm_resmgr_add_window(frame.getTitle().c_str(),
                               static_cast<screen_window_t>(frame));
}

/**
 * Sets a window as a dock, a top-level, frameless, window.
 * @param   window  The dock window to add
 */
void
Context::addDockWindow(Window &window)
{
    rootWindow_.addDockWindow(window);
    window.setDock();
}

/**
 * Sets a window as a screen locker.
 * Only the first such window is accepted.
 * @param   window  The locker window
 */
void
Context::setLockerWindow(Window &window)
{
    if (lockerWindow_.isValid()) {
        return;
    }

    lockerWindow_ = window;
    window.setLocker();
    flush();
}

/**
 * Activates the locker window.
 */
void
Context::lockScreen()
{
    Slog2(SLOG2_INFO).log("Locking screen");
    screenwm_resmgr_deliver_event(SCREENWM_EVENT_LOCK, 0, nullptr);
}

/**
 * Initialize the window manager.
 * Creates the context, main event and root window.
 * @return  true if successful, false otherwise.
 */
bool
Context::init()
{
    // Create the window manager context.
    if (screen_create_context(&handle_,
                              SCREEN_WINDOW_MANAGER_CONTEXT |
                              SCREEN_INPUT_PROVIDER_CONTEXT) != 0) {
        Slog2(SLOG2_ERROR).perror("Failed to create context");
        return false;
    }

    globalContext_ = this;

    // Ensure that the main event was created.
    if (!event_.isValid()) {
        return false;
    }

    if (screenwm_resmgr_init(handle_) != 0) {
        Slog2(SLOG2_ERROR).perror("Failed to initialize resource manager");
        return false;
    }

#if 1
    // TODO:
    // The window manager currently needs the setuid bit set in order to start
    // as root (both to create a window manager context and to attach the
    // resource manager). However, this causes us to lose the LD_LIBRARY_PATH
    // value.
    // For now, drop back to the user that started the process and manually set
    // the library path. Yuck.
    const uid_t uid = getuid();
    const uid_t euid = geteuid();
    dprint("uid=%d euid=%d\n", uid, euid);
    if ((euid == 0) && (uid != 0)) {
        if (setuid(uid) < 0) {
            Slog2(SLOG2_ERROR).perror("seteuid");
            return false;
        }

        const char  *ld_path = getenv("BACKUP_LD_LIBRARY_PATH");
        if (ld_path == nullptr) {
            Slog2(SLOG2_ERROR).log("BACKUP_LD_LIBRARY_PATH is not set");
            return false;
        }

        dprint("Setting LD_LIBRARY_PATH=%s\n", ld_path);
        setenv("LD_LIBRARY_PATH", ld_path, 1);
    }
#endif

    if (!keyboardSession_.create()) {
        return false;
    }

    // Create a root window as the parent of all other top-level windows.
    Settings settings("screenwm.ini");
    if (!rootWindow_.create("screenwm", settings)) {
        return false;
    }

    if (!rootWindow_.post(RectArray())) {
        return false;
    }

    // Create a native taskbar, if requested.
    createTaskBar(settings);

    // Get the display for the root window.
    // This is just a hack, assuming a single display.
    // In the future we need to enumerate the displays.
    screen_display_t handle;
    if (!rootWindow_.getProperty(SCREEN_PROPERTY_DISPLAY,
                                 reinterpret_cast<void **>(&handle))) {
        return true;
    }

    display_ = new Display(handle);
    display_->setPowerMode(true);

    std::string idleString = settings.getValue("General", "screenIdle");
    if (!idleString.empty() && std::stoul(idleString) != 0) {
        uint64_t idle_ns = std::stoul(idleString) * 60000000000ULL;
        idleTimer_ = new IdleTimer(idle_ns);
        idleTimer_->arm();
        Slog2(SLOG2_INFO).log("Idle=%lu", idle_ns);
    }

    // Launch startup programs.
    startup_ = new Startup;
    if (!startup_->startAll()) {
        delete startup_;
        startup_ = nullptr;
    }

    return true;
}

/**
 * Main event dispatcher.
 * @return  true if successful, false otherwise
 */
bool
Context::handleEvent()
{
    int type = event_.getType();

    switch (type) {
    case SCREEN_EVENT_NONE:
        return handleTimeout();

    case SCREEN_EVENT_CREATE:
        return handleCreated();

    case SCREEN_EVENT_POST:
        return handleWindowPosted();

    case SCREEN_EVENT_PROPERTY:
        return handlePropertyChange();

    case SCREEN_EVENT_POINTER:
        return handlePointer();

    case SCREEN_EVENT_CLOSE:
        return handleWindowClosed();

    case SCREEN_EVENT_KEYBOARD:
        return handleKeyboard();

    case SCREEN_EVENT_USER:
        return handleUserEvent();

    case SCREEN_EVENT_MANAGER:
        return handleManagerEvent();

    default:
        dprint("Unhandled event %d\n", type);
        break;
    }

    return true;
}

/**
 * Handle the NONE event, which occurs on a timeout.
 * Invoke the callback for any expired timers.
 */
bool
Context::handleTimeout()
{
    dprint("Firing timers\n");
    Timer::fire();
    return true;
}

/**
 * Handles the CREATED event.
 * The object type associated with the event determine what kind of object was
 * created.
 * @return  true if successful, false otherwise
 */
bool
Context::handleCreated()
{
    int objtype;
    if (!event_.getProperty(SCREEN_PROPERTY_OBJECT_TYPE, objtype)) {
        return false;
    }

    switch (objtype) {
    case SCREEN_OBJECT_TYPE_WINDOW:
        return handleWindowCreated();

    case SCREEN_OBJECT_TYPE_DEVICE:
        return keyboardSession_.handleDevice(event_);

    default:
        break;
    }

    return true;
}

/**
 * Called when a window is created.
 * @return  true if successful, false otherwise
 */
bool
Context::handleWindowCreated()
{
    Window  window = event_.getWindow();
    if (!window.isValid()) {
        return false;
    }

    WindowData *data = window.getWindowData();
    if (data == nullptr) {
        data = new WindowData;
        if (!window.setWindowData(data)) {
            return false;
        }
    }

    dprint("Window %p created\n", static_cast<screen_window_t>(window));

    // Make the window a child of the root window.
    if (!window.joinGroup("screenwm")) {
        return false;
    }

    window.setProperty(SCREEN_PROPERTY_VISIBLE, 0);
    return true;
}

/**
 * Called when a top-level window is posted for the first time.
 * Creates a frame to control the window.
 * @return  true if successful, false otherwise
 */
bool
Context::handleWindowPosted()
{
    Window  window = event_.getWindow();
    if (!window.isValid()) {
        // TODO:
        // Not sure how we get these events. Ignore them for now.
        return true;
    }

    // Check if this is a window from a process created by the startup manager.
    // If so, let it handle the window first.
    if (startup_ != nullptr) {
        startup_->handleWindowPosted(window);
        if (startup_->isDone()) {
            delete startup_;
            startup_ = nullptr;
        }
    }

    dprint("Window %p posted\n", static_cast<screen_window_t>(window));

    // Finish window initialization.
    if (!window.posted()) {
        return false;
    }

    if (taskbar_ != nullptr) {
        taskbar_->handleWindowPosted(window);
    }

    // Check for frameless windows.
    if (window.isFrameless()) {
        return true;
    }

    Frame   *frame = window.getFrame();
    if (frame == nullptr) {
        return false;
    }

    // Notify the resource manager.
    screenwm_resmgr_add_window(frame->getTitle().c_str(),
                               static_cast<screen_window_t>(*frame));

    // Make the new frame the active one.
    activate(frame);
    return true;
}

bool
Context::handlePropertyChange()
{
    int objtype;

    if (!event_.getProperty(SCREEN_PROPERTY_OBJECT_TYPE, &objtype)) {
        return false;
    }

    int name;
    if (!event_.getProperty(SCREEN_PROPERTY_NAME, &name)) {
        return false;
    }

    switch (objtype) {
    case SCREEN_OBJECT_TYPE_WINDOW:
        return handleWindowProperty(name);

    default:
        break;
    }

    return true;
}

/**
 * Called when a window property changes.
 * @param   name    The property that changed
 * @return  true if change handled successfully, false otherwise
 */
bool
Context::handleWindowProperty(int name)
{
    Window  window = event_.getWindow();
    if (!window.isValid()) {
        dprint("Property %d changed for invalid window\n", name);
        return true;
    }

    dprint("Property %d changed for window %p\n", name,
           static_cast<screen_window_t>(window));

    Frame   *frame = window.getFrame();
    if ((frame != nullptr) && (*frame == window)) {
        // Ignore property changes on frames.
        return true;
    }

    switch (name) {
    case SCREEN_PROPERTY_POSITION:
        // Window moved.
        // If the window has a frame, change the frame position to match the
        // window's.
        if (frame == nullptr) {
            return true;
        }

        if (!frame->isValid()) {
            // Frame was not initialized yet.
            return true;
        }

        if (!frame->setPositionFromWindow()) {
            return false;
        }
        break;

    case SCREEN_PROPERTY_MANAGER_STRING:
        window.manage();
        break;

    case SCREEN_PROPERTY_ZORDER:
        // Z-order value changed.
        // Make sure that frameless windows stay on top.
#ifndef NDEBUG
        int zorder;
        window.getProperty(SCREEN_PROPERTY_ZORDER, &zorder);
        dprint("New zorder=%d\n", zorder);
#endif

        if (window.isFrameless()) {
            window.setProperty(SCREEN_PROPERTY_ZORDER, ZOrder::TopLevel);
            flush();
        }
        break;

    case SCREEN_PROPERTY_STATUS:
        if (!handleWindowStatusChanged(window, frame)) {
            return false;
        }
        break;

    default:
        break;
    }

    return true;
}

/**
 * Handles a mouse pointer event, emitted whenever the cursor moves and/or a
 * mouse button changes state.
 * The event is directed to the appropriate frame.
 * @return  true if successful, false otherwise
 */
bool
Context::handlePointer()
{
    // Get the button state.
    int buttons;
    if (!event_.getProperty(SCREEN_PROPERTY_BUTTONS, &buttons)) {
        return false;
    }

    // Re-arm idle timer.
    if (idleTimer_ != nullptr) {
        idleTimer_->arm();
    }

    // Get a frame pointer for the window on which the button was clicked, if
    // any.
    Window  window = event_.getWindow();
    if (!window.isValid()) {
        dprint("Pointer event for invalid window\n");
        return true;

    }

    // Check if this window belongs to the window manager. If so, route the
    // event to it.
    LocalWindow *localWindow = window.getLocalWindow();
    if (localWindow == nullptr) {
        return true;
    }

    localWindow->handlePointer(&event_, buttons);

    return true;
}

/**
 * Handle a SCREEN_EVENT_CLOSE event.
 * If the window has a frame the frame is destroyed.
 * @return  true if successful, false otherwise
 */
bool
Context::handleWindowClosed()
{
    // Get a frame pointer for the closed window.
    Window  window = event_.getWindow();
    if (!window.isValid()) {
        dprint("Closing invalid window\n");
        return true;
    }

    WindowData *data = window.getWindowData();
    if (data == nullptr) {
        return true;
    }

    Frame   *frame = static_cast<Frame *>(data->getFrame());
    dprint("Window %p closed, frame=%p\n", static_cast<screen_window_t>(window),
           frame);

    if (frame != nullptr) {
        // If this was the active frame, activate another.
        if (frame == activeFrame_) {
            activeFrame_ = nullptr;
        }

        // Notify the resource manager.
        screenwm_resmgr_remove_window(static_cast<screen_window_t>(*frame));

        delete frame;

        if (activeFrame_ == nullptr) {
            activate(Frame::zorderList().topFrame());
        }
    }

    flush();

    delete data;
    return true;
}

/**
 * Handles a keyboard event.
 * The event is propagated to the active window.
 * @return  true if successful, false otherwise
 */
bool
Context::handleKeyboard()
{
    // Exit idle mode, if needed.
    if (isIdle_) {
        dprint("Exit idle at %ld\n", time(NULL));
        Slog2(SLOG2_INFO).log("Exit idle mode");
        isIdle_ = false;
        display_->setPowerMode(true);
    }

    // Re-arm idle timer.
    if (idleTimer_ != nullptr) {
        idleTimer_->arm();
    }

    Window  activeWindow;
    if ((lockerWindow_.isValid()) && (lockerWindow_.isVisible())) {
        // Locker window takes precendence.
        activeWindow = lockerWindow_;
    } else if (activeFrame_ != nullptr) {
        activeWindow = activeFrame_->getWindow();
    }

    return keyboardSession_.handleEvent(event_, activeWindow);
}

/**
 * Handles an internal event.
 * Internal events are used for asynchronous notifications from other threads in
 * this process, such as the resource manager.
 * @return  true if successful, false otherwise
 */
bool
Context::handleUserEvent()
{
    Event::UserType type;
    uintptr_t       value;
    if (!event_.getUserData(type, value)) {
        return false;
    }

    dprint("handleUserEvent %d %lx\n", type, value);

    switch (type) {
    case Event::Activate:
    {
        // Translate the handle into a local window.
        // If it is a frame, activate it.
        Window window(reinterpret_cast<screen_window_t>(value));
        LocalWindow *localWindow = window.getLocalWindow();
        if (localWindow != nullptr) {
            Frame *frame = dynamic_cast<Frame *>(localWindow);
            if (frame != nullptr) {
                activate(frame);
            }
        }
        break;

    }

    case Event::TaskbarUpdate:
        if (taskbar_ != nullptr) {
            taskbar_->update();
        }
        break;

    default:
        return false;
    }

    return true;
}

/**
 * Handles changes to a window's visibility.
 * If the window has a frame, then its visibility status needs to follow that of
 * the window.
 * @param   Window  The window that changed status
 * @param   Frame   The window's frame, nullptr if the window is frameless
 * @return  true if successful, false otherwise
 */
bool
Context::handleWindowStatusChanged(Window &window, Frame *frame)
{
    int status;
    window.getProperty(SCREEN_PROPERTY_STATUS, &status);
    dprint("New status=%d\n", status);

    switch (status) {
    case SCREEN_STATUS_INVISIBLE:
        if (frame == nullptr) {
            // A frameless window became invisible.
            // Make sure the active frame has the keyboard focus.
            if (activeFrame_) {
                dprint("Resetting focus to %p\n",
                       static_cast<screen_window_t>(activeFrame_->getWindow()));
                rootWindow_.setFocus(activeFrame_->getWindow());
            }
            break;
        }

        if (!frame->isVisible()) {
            break;
        }

        if (!frame->hide()) {
            return false;
        }

        screenwm_resmgr_remove_window(static_cast<screen_window_t>(*frame));

        if (frame == activeFrame_) {
            activeFrame_ = nullptr;
            activate(Frame::zorderList().topFrame());
        }
        break;

    case SCREEN_STATUS_VISIBLE:
    case SCREEN_STATUS_FULLY_VISIBLE:
        if (frame == nullptr) {
            break;
        }

        if (frame->isVisible()) {
            break;
        }

        if (!frame->show()) {
            return false;
        }

        screenwm_resmgr_add_window(frame->getTitle().c_str(),
                                   static_cast<screen_window_t>(*frame));

        activate(Frame::zorderList().topFrame());
        break;

    default:
        break;
    }

    return true;
}

/**
 * Handles events of type SCREEN_EVENT_MANAGER.
 * These events are sent by clients with a call to screen_inject_event(), and
 * are used to request changes to window attributes (@see Window::manage()).
 * @return  true if successful, false otherwise
 */
bool
Context::handleManagerEvent()
{
    Window  window = event_.getWindow();
    if (!window.isValid()) {
        dprint("Manager event for invalid window\n");
        return true;
    }

    std::string str;
    if (!event_.getUserData(str)) {
        return false;
    }

    return window.manage(str.c_str());
}

/**
 * Create a native taskbar.
 * The taskbar is only created if the "General" section of the screenwm.ini
 * contains a key called "taskbar".
 * @param   settings    An open screenwm.ini file
 */
void
Context::createTaskBar(const Settings &settings)
{
    std::string taskbar = settings.getValue("General", "taskbar");
    if (taskbar.empty()) {
        return;
    }

    dprint("Creating native taskbar\n");
    taskbar_ = new TaskBar;
    taskbar_->create();
    addDockWindow(*taskbar_);
}

/**
 * Enter idle state when the idle timer expires.
 */
void
Context::idleTimeout()
{
    if (isIdle_) {
        // Already idle.
        return;
    }

    dprint("Idle timeout at %ld\n", time(NULL));
    Slog2(SLOG2_INFO).log("Enter idle mode");
    isIdle_ = true;

    // Lock screen.
    lockScreen();

    // Power off display.
    display_->setPowerMode(false);
}

/**
 * Destroys the context handle.
 */
void
Context::destroy()
{
    if (handle_ != nullptr) {
        screen_destroy_context(handle_);
        handle_ = nullptr;
    }
}

}
