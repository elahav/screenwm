/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_PIXMAP_H
#define SCREENWM_PIXMAP_H

#include "screenwm.h"
#include "buffer.h"

namespace ScreenWM
{

/**
 * @brief   Off-screen rendering surface
 *
 * A pixmap provides a buffer that can be rendered into and then copied to a
 * window buffer with @Context::blit().
 */
class Pixmap
{
public:
    Pixmap(screen_pixmap_t handle = NULL);
    ~Pixmap();

    bool setHandle(screen_pixmap_t handle, bool created);
    bool getProperty(int name, int values[]) const;
    bool getProperty(int name, void *values[]) const;
    bool setProperty(int name, int value);
    bool setProperty(int name, int values[]);
    bool setProperty(int name, void *value);
    bool createBuffer(const Size &size);
    Buffer getBuffer() const;

    /**
     * Determines whether the pixmap is associated with a native handle.
     */
    bool isValid() const { return handle_ != NULL; }

    bool operator==(const Pixmap& other) const {
        return handle_ == other.handle_;
    }

    operator screen_pixmap_t() const { return handle_; }

    void destroy();

protected:
    /** Native pixmap handle. */
    screen_pixmap_t     handle_;

    /** Whether the handle should be destroyed when the object is deleted. */
    bool                autoDestroy_;
};

}

#endif
