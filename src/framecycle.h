/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_FRAMECYCLE_H
#define SCREENWM_FRAMECYCLE_H

#include "frame.h"

namespace ScreenWM
{

/**
 * @brief   Implements cycling through all frames.
 *
 * The class is used to select a frame by temporarily moving successive frames
 * to the foreground, without making the frame permanently active. The last
 * frame selected becomes the active one when the object is destroyed.
 */
class FrameCycle
{
public:
    /**
     * Class constructor.
     * Selects the foreground frame.
     */
    FrameCycle() {
        itr_ = Frame::zorderList().rbegin();
    }

    /**
     * Class destructor.
     * Makes the last selected frame active.
     */
    ~FrameCycle() {
        if (itr_ != Frame::zorderList().rend()) {
            Context::getContext().activate(*itr_);
        }
    }

    /**
     * Chooses the next frame.
     * The previously chosen frame is moved to its original Z order position,
     * while the newly selected one is brought to the foreground.
     */
    void next() {
        if (itr_ == Frame::zorderList().rend()) {
            // No frames.
            return;
        }

        (*itr_)->unraise();

        itr_++;
        if (itr_ == Frame::zorderList().rend()) {
            itr_ = Frame::zorderList().rbegin();
        }

        (*itr_)->raise();
    }

private:
    /**
     * A reverse iterator on the Z order list of frames.
     */
    ZOrderList::criterator   itr_;
};

}

#endif
