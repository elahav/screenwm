/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_THEME_H
#define SCREENWM_THEME_H

#include "screenwm.h"
#include "frame.h"

namespace ScreenWM
{

/**
 * @brief   Abstract base class for themes
 */
class Theme
{
public:
    /**
     * Names for various elements that the theme needs to draw.
     */
    typedef enum {
        None,
        CloseButton,
        MaximizeButton,
        MinimizeButton,
        TitleBar,
        LeftFrame,
        RightFrame,
        BottomFrame
    } Element;

    /**
     * Class constructor.
     */
    Theme() {}

    /**
     * Class destructor.
     */
    virtual ~Theme() {}

    virtual Theme *create() = 0;
    virtual void destroy() = 0;
    virtual int getFormat() = 0;
    virtual void drawFrame(const Frame &frame, const Rect &rect,
                           bool active) = 0;
    virtual void drawElement(const Frame &frame, Element element,
                             ButtonState state, Rect &rect) = 0;
    virtual int getElementSize(Element) const = 0;
    virtual Element getElementFromPoint(const Frame &frame,
                                        const Point &pos) const = 0;

    static bool setTheme(const char *name = "default");
    static Theme *getTheme();

protected:
    virtual bool init() = 0;

private:
    /** Pointer to the currently selected theme. */
    static Theme    *globalTheme_;
};

}

extern "C"
{

/**
 * Theme DLLs need to define this function.
 */
extern ScreenWM::Theme *LoadTheme(void);

}

#endif
