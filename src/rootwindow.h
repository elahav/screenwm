/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_ROOTWINDOW_H
#define SCREENWM_ROOTWINDOW_H

#include "screenwm.h"
#include "cairowindow.h"
#include "settings.h"

namespace ScreenWM
{

/**
 * @brief   The parent of all top-level windows.
 *
 * The window is used to create a parent-child relationship among all top-level
 * windows (frames and their controlled windows). It is also used to draw the
 * desktop background.
 * Using a common parent to all top-level windows allows us to establish a
 * proper Z order among all frames and frame-controlled windows.
 */
class RootWindow : public CairoWindow
{
public:
    RootWindow();

    bool create(const char *groupName, const Settings &settings);
    bool setFocus(const Window& window);
    void addDockWindow(const Window &window);
    bool getDisplayGeometry(Rect &rect) const;

    void getDesktopArea(Rect &rect) const { rect = desktopRect_; }

private:
    /** The display on which the root window appears. */
    screen_display_t    display_;

    /** Window group owned by the root window. */
    screen_group_t      group_;

    /** Useable size of the desktop area. */
    Rect                desktopRect_;

    /** Background image. */
    cairo_surface_t     *backgroundImage_;

    /** Background colour. */
    unsigned            backgroundColour_;

    void loadBackgroundImage(std::string background);
    void draw();
};

}

#endif
