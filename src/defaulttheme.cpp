/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "defaulttheme.h"
#include <iostream>

namespace ScreenWM
{

/**
 * Constants for the position and size of various frame elements.
 */
struct FrameMetrics
{
    static const int    TopHeight = 22;
    static const int    BottomHeight = 4;
    static const int    LeftWidth = 4;
    static const int    RightWidth = 4;
    static const int    ButtonWidth = 20;
    static const int    ButtonHeight = 16;
    static const int    ButtonPad = 0;
    static const int    ButtonTop = 2;
    static const int    ButtonBottom = ButtonTop + ButtonHeight;
    static const int    CloseButtonRight = -RightWidth;
    static const int    CloseButtonLeft = CloseButtonRight - ButtonWidth;
    static const int    MaximizeButtonRight = CloseButtonLeft - ButtonPad;
    static const int    MaximizeButtonLeft = MaximizeButtonRight - ButtonWidth;
    static const int    MinimizeButtonRight = MaximizeButtonLeft - ButtonPad;
    static const int    MinimizeButtonLeft = MinimizeButtonRight - ButtonWidth;
    static const int    TextSize = 14;
};

cairo_surface_t  *DefaultTheme::closeImage_[2];
cairo_surface_t  *DefaultTheme::maximizeImage_[2];
cairo_surface_t  *DefaultTheme::minimizeImage_[2];

/**
 * Draws a frame, along with all of its elements, into the buffer of the given
 * frame window.
 * @param   frame   Window to draw to
 */
void
DefaultTheme::drawFrame(const Frame &frame, const Rect &rect, bool active)
{
    // Get the window's buffer.
    Buffer          buffer = frame.getBuffer();
    if (!buffer.isValid()) {
        return;
    }

    unsigned char   *ptr
        = reinterpret_cast<unsigned char *>(buffer.getPointer());
    if (ptr == NULL) {
        return;
    }

    cairo_surface_t *surface =
        cairo_image_surface_create_for_data(ptr, CAIRO_FORMAT_ARGB32,
                                            rect.width_, rect.height_,
                                            buffer.getStride());

    cairo_t         *cr = cairo_create(surface);

    cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
                           CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cr, FrameMetrics::TextSize);

    if (active) {
        cairo_set_source_rgb(cr, .95, .95, .95);
    } else {
        cairo_set_source_rgb(cr, .85, .85, .85);
    }
    cairo_rectangle(cr, 0, 0, rect.width_, rect.height_);
    cairo_fill(cr);

    if (active) {
        cairo_set_source_rgb(cr, .3, .3, .3);
    } else {
        cairo_set_source_rgb(cr, .5, .5, .5);
    }

    cairo_move_to(cr, 6, 16);
    cairo_show_text(cr, frame.getTitle().c_str());

    cairo_set_source_surface(cr, closeImage_[0],
                             rect.width_ + FrameMetrics::CloseButtonLeft,
                             FrameMetrics::ButtonTop);
    cairo_paint(cr);

    cairo_set_source_surface(cr, maximizeImage_[0],
                             rect.width_ + FrameMetrics::MaximizeButtonLeft,
                             FrameMetrics::ButtonTop);
    cairo_paint(cr);

    cairo_set_source_surface(cr, minimizeImage_[0],
                             rect.width_ + FrameMetrics::MinimizeButtonLeft,
                             FrameMetrics::ButtonTop);
    cairo_paint(cr);

    cairo_destroy(cr);
    cairo_surface_destroy(surface);
}

/**
 * Draws a specific element into the buffer of the given frame window.
 * @param   frame   Window to draw to
 * @param   element Identifies the element to draw
 * @param   state   For buttons, whether the button is pressed or not
 * @param   rect    Holds the coordinates of the drawn area, upon return
 */
void
DefaultTheme::drawElement(const Frame &frame, Element element,
                          ButtonState state, Rect &rect)
{
    // Get the window's buffer.
    Buffer          buffer = frame.getBuffer();
    if (!buffer.isValid()) {
        return;
    }

    // Get the frame's size.
    if (!frame.getGeometry(rect)) {
        return;
    }

    unsigned char   *ptr
        = reinterpret_cast<unsigned char *>(buffer.getPointer());
    if (ptr == NULL) {
        return;
    }

    cairo_surface_t *surface =
        cairo_image_surface_create_for_data(ptr, CAIRO_FORMAT_ARGB32,
                                            rect.width_, rect.height_,
                                            buffer.getStride());

    cairo_t         *cr = cairo_create(surface);

    int x;
    int y = FrameMetrics::ButtonTop;
    cairo_surface_t *image = nullptr;
    switch (element) {
    case CloseButton:
        image = closeImage_[state];
        x = rect.width_ + FrameMetrics::CloseButtonLeft;
        break;

    case MaximizeButton:
        image = maximizeImage_[state];
        x = rect.width_ + FrameMetrics::MaximizeButtonLeft;
        break;

    case MinimizeButton:
        image = minimizeImage_[state];
        x = rect.width_ + FrameMetrics::MinimizeButtonLeft;
        break;

    default:
        break;
    }

    if (image != nullptr) {
        int width = cairo_image_surface_get_width(image);
        int height = cairo_image_surface_get_height(image);
        cairo_set_source_rgb(cr, .95, .95, .95);
        cairo_rectangle(cr, x, y, width, height);
        cairo_fill(cr);

        cairo_set_source_surface(cr, image, x, y);
        cairo_paint(cr);
    }

    cairo_destroy(cr);
    cairo_surface_destroy(surface);
}

/**
 * Returns the size, in pixels, of a specific element.
 * The meaning of the size value depends on the requested element.
 * @param   element Element to get the size for
 * @return  Element size in pixels
 */
int
DefaultTheme::getElementSize(Element element) const
{
    switch (element) {
    case TitleBar:
        return FrameMetrics::TopHeight;

    case LeftFrame:
        return FrameMetrics::LeftWidth;

    case RightFrame:
        return FrameMetrics::RightWidth;

    case BottomFrame:
        return FrameMetrics::BottomHeight;

    default:
        return -1;
    }
}

/**
 * Determines the frame element from a given mouse cursor position.
 * @param   frame   The frame
 * @param   pos     Cursor coordinates
 * @return  The element matching the given coordinates, Theme::None if the
 *          coordinates do not correspond to any element.
 */
Theme::Element
DefaultTheme::getElementFromPoint(const Frame &frame, const Point &pos) const
{
    // Get the window's size.
    Rect            rect;
    if (!frame.getGeometry(rect)) {
        return Theme::None;
    }

    // Check for a button.
    if ((pos.y_ >= FrameMetrics::ButtonTop)
        && (pos.y_ < FrameMetrics::ButtonBottom)) {
        // Vertical point matches the button row.
        if ((pos.x_ >= (rect.width_ + FrameMetrics::CloseButtonLeft))
            && (pos.x_ < (rect.width_ + FrameMetrics::CloseButtonRight))) {
            return Theme::CloseButton;
        }

        if ((pos.x_ >= (rect.width_ + FrameMetrics::MaximizeButtonLeft))
            && (pos.x_ < (rect.width_ + FrameMetrics::MaximizeButtonRight))) {
            return Theme::MaximizeButton;
        }

        if ((pos.x_ >= (rect.width_ + FrameMetrics::MinimizeButtonLeft))
            && (pos.x_ < (rect.width_ + FrameMetrics::MinimizeButtonRight))) {
            return Theme::MinimizeButton;
        }
    }

    // Check for the title bar.
    if (pos.y_ < FrameMetrics::TopHeight) {
        return Theme::TitleBar;
    }

    // Check for the bottom frame.
    if (pos.y_ > (rect.height_ - FrameMetrics::BottomHeight)) {
        return Theme::BottomFrame;
    }

    // Check for the left frame.
    if (pos.x_ < FrameMetrics::LeftWidth) {
        return Theme::LeftFrame;
    }

    // Check for the right frame.
    if (pos.x_ > (rect.width_ - FrameMetrics::RightWidth)) {
        return Theme::RightFrame;
    }

    return Theme::None;
}

/**
 * Initializes the theme.
 * Loads the necessary bitmaps for drawing the buttons.
 */
bool
DefaultTheme::init()
{
    closeImage_[0] =
        cairo_image_surface_create_from_png("images/smoky_close.png");
    closeImage_[1] =
        cairo_image_surface_create_from_png("images/smoky_close_pressed.png");
    maximizeImage_[0] =
        cairo_image_surface_create_from_png("images/smoky_max.png");
    maximizeImage_[1] =
        cairo_image_surface_create_from_png("images/smoky_max_pressed.png");
    minimizeImage_[0] =
        cairo_image_surface_create_from_png("images/smoky_min.png");
    minimizeImage_[1] =
        cairo_image_surface_create_from_png("images/smoky_min_pressed.png");
    if ((closeImage_[0] == NULL)
        || (closeImage_[1] == NULL)
        || (maximizeImage_[0] == NULL)
        || (maximizeImage_[1] == NULL)
        || (minimizeImage_[0] == NULL)
        || (minimizeImage_[1] == NULL)) {
        std::cerr << "Default theme: Failed to load images" << std::endl;
        return false;
    }

    return true;
}

}
