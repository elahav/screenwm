/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_ZORDERLIST_H
#define SCREENWM_ZORDERLIST_H

#include <vector>
#include <pthread.h>

namespace ScreenWM
{

class Frame;

/**
 * @brief   List of frames sorted by their Z-order value
 *
 * The list is always kept compact, with each frame having a unique Z order
 * value. Insetrion into the list makes the inserted frame the one with the
 * highest Z order value in the system.
 */
class ZOrderList : public std::vector<Frame *>
{
public:
    ZOrderList();
    ~ZOrderList();

    typedef std::vector<Frame *>::const_iterator            citerator;
    typedef std::vector<Frame *>::const_reverse_iterator    criterator;

    bool insert(Frame *frame);
    bool remove(Frame *frame);
    Frame *frameAt(unsigned idx) const;
    Frame *topFrame() const;
    Frame *nextTop() const;

private:
    bool bringToBack(Frame *frame, int &zorder);
};

}

#endif
