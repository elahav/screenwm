/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_SETTINGS_H
#define SCREENWM_SETTINGS_H

#include "screenwm.h"
#include <map>
#include <string>

namespace ScreenWM
{

/**
 * @brief   INI file parser
 *
 * Loads and parses configuration files that uses the INI format.
 * Lines of the form 'key=value' generate a set of values that can be queried by
 * their keys. The key-value pairs can also be grouped with lines of the form
 * '[group]', with all succeeding lines belonging to the specified group name.
 * Comments use the '#' prefix.
 */
class Settings
{
private:
    /** Per-group key-value map. */
    typedef std::map<std::string, std::string>  KeyMap;

    /** A mapping of group names to their key-value maps. */
    typedef std::map<std::string, KeyMap *>     GroupMap;

public:
    Settings(const char *path);
    ~Settings();

    typedef GroupMap::const_iterator    GroupIterator;

    bool isValid() const { return valid_; }
    std::string getValue(std::string group, std::string key) const;
    std::string getValue(const GroupIterator &itr, std::string key) const;
    std::string getGroup(const GroupIterator &itr) const;

    GroupIterator groupBegin() const { return groups_.begin(); }
    GroupIterator groupEnd() const { return groups_.end(); }

private:
    /** Whether the INI file was loaded successfully. */
    bool                        valid_;

    /** Main map, indexed by group name. */
    GroupMap                    groups_;

    bool parse(const char *buf, size_t size);
};

}

#endif
