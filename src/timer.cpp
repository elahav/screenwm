/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "timer.h"

namespace ScreenWM
{

Timer::List Timer::list_;


/**
 * Activates a timer to fire at some future time.
 * An armed timer is added to the timer list.
 * @param   timeout     Expiration time, in nanoseconds
 * @param   absrel      Whether the timeout is absolute or relative
 */
void
Timer::arm(uint64_t timeout, TimeoutAbsRel absrel)
{
    if (absrel == Relative) {
        timeout += clock_gettime_mon_ns();
    }

    uint64_t prevTimeout = timeout_;
    timeout_ = timeout;

    if (prevTimeout != NO_TIMEOUT) {
        // Timer is already armed.
        // Check if it needs to be repositioned on the list.
        if (timeout_ > prevTimeout) {
            // New timeout is later than current.
            // Check if at end of list or the next timer still has a larger
            // timeout value.
            List::iterator next = itr_;
            next++;
            if (next == list_.end()) {
                return;
            }

            if ((*next)->timeout_ >= timeout_) {
                return;
            }
        } else if (timeout_ < prevTimeout) {
            // New timeout is earlier than current.
            // Check if at end of list or the next timer still has a smaller
            // timeout value.
            List::iterator prev = itr_;
            if (prev == list_.begin()) {
                return;
            }

            prev--;
            if ((*prev)->timeout_ >= timeout_) {
                return;
            }
        } else {
            // No change.
            return;
        }

        list_.remove(this);
    }

    insert(this);
}

/**
 * Deactivates a timer, removing it from the list.
 */
void
Timer::disarm()
{
    timeout_ = NO_TIMEOUT;
    list_.remove(this);
}

/**
 * Determine the next timeout, based on the active timer with the closest
 * expiration time.
 * @param   absrel  Whether to return the absolute timeout, or a value relative
 *                  to the current time
 * @return  Timeout, in nanoseconds
 */
uint64_t
Timer::nextTimeout(TimeoutAbsRel absrel)
{
    if (list_.empty()) {
        return NO_TIMEOUT;
    }

    uint64_t timeout = list_.front()->timeout_;
    if (absrel == Absolute) {
        return timeout;
    }

    uint64_t now = clock_gettime_mon_ns();
    if (now > timeout) {
        return 0;
    }

    return timeout - now;
}

/**
 * Invoke the callback functions of all expired timers.
 * Expired timers are removed from the list.
 */
void
Timer::fire()
{
    uint64_t now = clock_gettime_mon_ns();

    for (;;) {
        if (list_.empty()) {
            break;
        }

        Timer *timer = list_.front();
        if (timer->timeout_ > now) {
            break;
        }

        list_.pop_front();
        timer->timeout_ = NO_TIMEOUT;
        timer->callback();
    }
}


/**
 * Add a timer to the list.
 * @param   timer   The timer to add
 */
void
Timer::insert(Timer *timer)
{
    List::iterator itr;
    for (itr = list_.begin(); itr != list_.end(); ++itr) {
        if (timer->timeout_ < (*itr)->timeout_) {
            break;
        }
    }
    timer->itr_ = list_.insert(itr, timer);
}

}
