/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "rootwindow.h"
#include "context.h"
#include "slog2.h"
#include <stdlib.h>

namespace ScreenWM
{

/**
 * Class constructor.
 */
RootWindow::RootWindow()
    : CairoWindow(), backgroundImage_(nullptr), backgroundColour_(0x2030d9)
{
}

/**
 * Creates and draws the root window.
 * @param   groupName   The name of a group to which all top-level windows will
 *                      join
 * @param   settings    Configuration file
 * @return  true if successful, false otherwise
 */
bool
RootWindow::create(const char *groupName, const Settings &settings)
{
    // Create the window.
    if (!LocalWindow::create(WindowData::Frameless)) {
        return false;
    }

    // Root window should never recieve keyboard events.
    if (!setProperty(SCREEN_PROPERTY_SENSITIVITY,
                     SCREEN_SENSITIVITY_MASK_NO_FOCUS)) {
        return false;
    }

    // Create a group for top-level windows to join.
    group_ = createGroup(groupName);
    if (group_ == NULL) {
        return false;
    }

    // Get the display for this window.
    if (!getProperty(SCREEN_PROPERTY_DISPLAY,
                     reinterpret_cast<void **>(&display_))) {
        return false;
    }

    // Resize to occupy the screen.
    if (!getDisplayGeometry(desktopRect_)) {
        return false;
    }

    if (!setGeometry(desktopRect_)) {
        destroy();
        return false;
    }

    dprint("Root window geometry: 0x0-%dx%d\n", desktopRect_.width_,
           desktopRect_.height_);

    // Determine the background.
    std::string background = settings.getValue("General", "background");
    loadBackgroundImage(background);

    // Calculate buffer size.
    // If an image was loaded, use its size, which would scale the contents to
    // the window's size.
    Size size(desktopRect_.width_, desktopRect_.height_);
    if (backgroundImage_ != nullptr) {
        size.width_ = cairo_image_surface_get_width(backgroundImage_);
        size.height_ = cairo_image_surface_get_height(backgroundImage_);
    }

    // Create the window buffers and Cairo drawing objects.
    if (!CairoWindow::create(size)) {
        return false;
    }

    // Draw the window.
    draw();

    return true;
}

/**
 * Sets the input focus to the given window.
 * Called when a frame is activated in order to direct input to its managed
 * window.
 * @param   window  The window to set the focus to
 * @return  true if successful, false otherwise
 */
bool
RootWindow::setFocus(const Window& window)
{
    // Set the group focus to the given window.
    screen_window_t winHandle = static_cast<screen_window_t>(window);
    if (screen_set_group_property_pv(group_, SCREEN_PROPERTY_FOCUS,
                                     reinterpret_cast<void **>(&winHandle))
        < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set group focus");
        return false;
    }

    dprint("Group focus set to %p\n", winHandle);

    // Set the display focus to this window (the owner of the group).
    if (screen_set_display_property_pv(display_, SCREEN_PROPERTY_FOCUS,
                                       reinterpret_cast<void **>(&handle_))
        < 0) {
        Slog2(SLOG2_ERROR).perror("screen_set_display_property_pv");
        return false;
    }

    dprint("Display focus set to %p for display %p\n", handle_, display_);
    return true;
}

/**
 * Updates desktop margins when a new dock window is posted.
 * Attempts to detect which margin the dock belongs to and how that margin
 * should be modified.
 * @param   window  Newly posted dock window
 */
void
RootWindow::addDockWindow(const Window &window)
{
    // Get dock position and size.
    Rect    rect;
    window.getGeometry(rect);

    int   left = desktopRect_.x_;
    int   top = desktopRect_.y_;
    int   right = desktopRect_.x_ + desktopRect_.width_;
    int   bottom = desktopRect_.y_ + desktopRect_.height_;

    // Determine the orientation of the dock.
    if (rect.width_ > rect.height_) {
        // Horizontal.
        if (rect.y_ > (bottom - top) / 2) {
            // Dock goes in the bottom margin.
            if (rect.y_ < bottom) {
                bottom = rect.y_;
            }
        } else {
            // Dock goes in the top margin.
            if (rect.y_ + rect.height_ > top) {
                top = rect.y_ + rect.height_;
            }
        }
    } else {
        // Vertical.
        if (rect.x_ > (right - left) / 2) {
            // Dock goes in the right margin.
            if (rect.x_ < right) {
                right = rect.x_;
            }
        } else {
            // Dock goes in the left margin.
            if (rect.x_ + rect.width_ > left) {
                left = rect.x_ + rect.width_;
            }
        }
    }

    // Update the desktop area.
    desktopRect_.x_ = left;
    desktopRect_.y_ = top;
    desktopRect_.width_ = right - left;
    desktopRect_.height_ = bottom - top;
}

/**
 * Determines the size of the current display.
 * @param   rect    Rectangle to update with the display geometry
 * @return  true if successful, false otherwise
 */
bool
RootWindow::getDisplayGeometry(Rect &rect) const
{
    // Query screen.
    int size[2];
    if (screen_get_display_property_iv(display_, SCREEN_PROPERTY_SIZE, size)
        < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get display size");
        return false;
    }

    // Update the output rectangle.
    rect.x_ = 0;
    rect.y_ = 0;
    rect.width_ = size[0];
    rect.height_ = size[1];
    return true;
}

/**
 * Determine the background to use for the desktop.
 * The "background" value from the screenwm.ini file is used to determine how to
 * draw the window:
 * 1. A PNG file name for an image
 * 2. A hexadecimal RGB value, prefix with '@'
 * 3. A default solid colour
 * @param   background  The value of the "background" key
 */
void
RootWindow::loadBackgroundImage(std::string background)
{
    dprint("background=%s\n", background.c_str());

    if (background.empty()) {
        // No background specified, use a default colour.
        return;
    }

    if (background[0] == '@') {
        // Solid colour.
        backgroundColour_ = strtoul(&background.c_str()[1], NULL, 16);
        return;
    }

    // Load an image.
    backgroundImage_ = cairo_image_surface_create_from_png(background.c_str());
    if (cairo_surface_status(backgroundImage_) != CAIRO_STATUS_SUCCESS) {
        backgroundImage_ = nullptr;
    }
}

/**
 * Draw the desktop background.
 */
void
RootWindow::draw()
{
    if (backgroundImage_ != nullptr) {
        cairo_set_source_surface(cairo_, backgroundImage_, 0, 0);
        cairo_paint(cairo_);
        return;
    }

    // Use a solid background colour.
    double red = (double)((backgroundColour_ >> 16) & 0xff) / 256.0;
    double green = (double)((backgroundColour_ >> 8) & 0xff) / 256.0;
    double blue = (double)(backgroundColour_ & 0xff) / 256.0;
    cairo_set_source_rgb(cairo_, red, green, blue);
    cairo_rectangle(cairo_, 0, 0, desktopRect_.width_, desktopRect_.height_);
    cairo_fill(cairo_);
}

}
