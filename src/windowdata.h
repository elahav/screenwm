/*
 * Copyright (C) 2024 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_WINDOWDATA_H
#define SCREENWM_WINDOWDATA_H

namespace ScreenWM
{

class Window;

/**
 * @brief   Window-manager information stored with a native window
 *
 * The window manager receives events from Screen where the relevant window is
 * identified by a native, opaque, window handle. In order to store
 * additional data for the window, the window manager allocates a WindowData
 * object for each window, and associated a pointer to the structure with the
 * native window handle by using the SCREEN_PROPERTY_USER_HANDLE property. The
 * structure can then be retrieved from the handle by getting the value
 * associated with this property.
 */
class WindowData
{
public:
    enum Flags {
        /** Top-level window. */
        TopLevel = 0x1,
        /** Window is used to lock the session. */
        Locker = 0x2,
        /** Window should not have a frame. */
        Frameless = 0x4,
        /** Window was posted. */
        Posted = 0x08
    };

    WindowData() : flags_(0), frame_(nullptr), localWindow_(nullptr) {}

    void setFrame(Window *frame) { frame_ = frame; }
    Window *getFrame() const { return frame_; }
    void setLocalWindow(Window *window) { localWindow_ = window; }
    Window *getLocalWindow() const { return localWindow_; }
    void setFlags(uint64_t flags) { flags_ |= flags; }
    bool hasFlag(uint64_t flag) { return (flags_ & flag) != 0; }

private:
    /** A bit-wise combination of WindowData::Flags. */
    uint64_t    flags_;

    /**
     * A pointer to the frame.
     * Non-null for framed windows, null for frameless.
     */
    Window      *frame_;

    /**
     * A pointer to a Window object allocated by the window manager.
     * This pointer is non-null only for windows created by the window manager
     * itself, such as frames, menus, native docks, etc.
     */
    Window      *localWindow_;
};

}

#endif
