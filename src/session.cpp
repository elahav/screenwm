/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "session.h"
#include "context.h"
#include "slog2.h"

namespace ScreenWM
{

/**
 * Class constructor.
 * @param   handle  Native session handle
 */
Session::Session(screen_session_t handle) : handle_(handle)
{
}

/**
 * Copy constructor.
 * @param   other   Object to copy the handle from
 */
Session::Session(const Session &other) : handle_(other.handle_)
{
}

/**
 * Class destructor.
 */
Session::~Session()
{
}

/**
 * Initializes the session, associating it with a window and a cursor shape.
 * @param   window      The window for which the session is defined
 * @param   cursorShape Shape to display when the mouse hovers over the session
 *                      region
 * @return  true if successful, false otherwise
 */
bool
Session::create(const Window &window, int cursorShape)
{
    if (!Context::getContext().createSession(SCREEN_EVENT_POINTER, *this)) {
        return false;
    }

    screen_window_t winHandle = static_cast<screen_window_t>(window);
    if (!setProperty(SCREEN_PROPERTY_WINDOW, static_cast<void *>(winHandle))) {
        destroy();
        return false;
    }

    setProperty(SCREEN_PROPERTY_CURSOR, cursorShape);
    return true;
}

bool
Session::getProperty(int name, void *values[])
{
    if (screen_get_session_property_pv(handle_, name, values) == -1) {
        Slog2(SLOG2_ERROR).perror("Failed to get session property");
        return false;
    }

    return true;
}

/**
 * Assigns a new integer value to the given property
 * @param   name    Property identifier
 * @param   value   New property value
 * @return  true if successful, false otherwise
 */
bool
Session::setProperty(int name, int value)
{
    if (screen_set_session_property_iv(handle_, name, &value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set session property");
        return false;
    }

    return true;
}

/**
 * Assigns a new pointer value to the given property
 * @param   name    Property identifier
 * @param   value   New property value
 * @return  true if successful, false otherwise
 */
bool
Session::setProperty(int name, void *value)
{
    if (screen_set_session_property_pv(handle_, name, &value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set session property");
        return false;
    }

    return true;
}

/**
 * Changes the session's position and size to the given values.
 * The position is relative to the owner window.
 * @param   rect    New window coordinates and size
 * @return  true if successful, false otherwise
 */
bool
Session::setGeometry(const Rect &rect)
{
    int pos[] = { rect.x_, rect.y_ };
    if (screen_set_session_property_iv(handle_, SCREEN_PROPERTY_POSITION, pos)
        < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set session property");
        return false;
    }

    int size[] = { rect.width_, rect.height_ };
    if (screen_set_session_property_iv(handle_, SCREEN_PROPERTY_SIZE, size)
        < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set session property");
        return false;
    }

    return true;
}

/**
 * Frees the native handle.
 * @return  true if successful, false otherwise
 */
bool
Session::destroy()
{
    if (handle_ == nullptr) {
        return true;
    }

    if (screen_destroy_session(handle_) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to destroy session");
        return false;
    }

    handle_ = nullptr;
    return true;
}

}
