/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "buffer.h"
#include "slog2.h"

namespace ScreenWM
{

/**
 * Class constructor.
 * @param   handle  Native buffer handle
 */
Buffer::Buffer(screen_buffer_t handle) : handle_(handle)
{
}

/**
 * Copy constructor.
 * Note that copying an object means that the same handle is now used by both.
 * @param   other   Object to copy the handle from
 */
Buffer::Buffer(const Buffer& other) : handle_(other.handle_)
{
}

/**
 * Obtains a pointer to the memory area of a properly-allocated buffer.
 * @return  Non-null pointer if successful, NULL otherwise
 */
void *
Buffer::getPointer() const
{
    void    *ptr;
    if (screen_get_buffer_property_pv(handle_, SCREEN_PROPERTY_POINTER,
                                      (void **)&ptr) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get buffer pointer");
        return NULL;
    }

    return ptr;
}

/**
 * Determines the number of bytes in a single row, to be used when advancing the
 * pointer across rows.
 * This may not be the same as the pixel width to which the buffer was created.
 * @return  Stride value if successful, 0 otherwise
 */
int
Buffer::getStride() const
{
    int stride;

    if (screen_get_buffer_property_iv(handle_, SCREEN_PROPERTY_STRIDE, &stride)
        < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get buffer stride");
        return 0;
    }

    return stride;
}

/**
 * @return  The size of the buffer in bytes, if successful, 0 otherwise
 */
int
Buffer::getSize() const
{
    int size;
    if (screen_get_buffer_property_iv(handle_, SCREEN_PROPERTY_SIZE, &size)
        < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get buffer size");
        return 0;
    }

    return size;
}

}
