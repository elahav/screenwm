/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "cairowindow.h"

namespace ScreenWM
{

/**
 * Class constructor.
 */
CairoWindow::CairoWindow() : LocalWindow(), surface_(nullptr), cairo_(nullptr)
{
}

/**
 * Class destructor.
 */
CairoWindow::~CairoWindow()
{
    if (cairo_ != nullptr) {
        cairo_destroy(cairo_);
    }

    if (surface_ != nullptr) {
        cairo_surface_destroy(surface_);
    }
}

/**
 * Create the native window object, the buffer and the Cairo objects.
 * @param   size    Window's buffer size
 */
bool
CairoWindow::create(const Size &size)
{
    // Set format and usage.
    if (!setProperty(SCREEN_PROPERTY_USAGE, SCREEN_USAGE_WRITE)) {
        return false;
    }

    if (!setProperty(SCREEN_PROPERTY_FORMAT, SCREEN_FORMAT_RGBA8888)) {
        return false;
    }

    int sizeArray[2] = { size.width_, size.height_ };
    if (!setProperty(SCREEN_PROPERTY_SOURCE_SIZE, sizeArray)) {
        return false;
    }

    // Create the window buffer.
    if (!createBuffers(1)) {
        return false;
    }

    // Set up a Cairo surface.
    Buffer buffer = getBuffer();
    uint8_t *ptr = reinterpret_cast<uint8_t *>(buffer.getPointer());
    if (ptr == NULL) {
        return false;
    }

    cairo_surface_t *surface =
        cairo_image_surface_create_for_data(ptr, CAIRO_FORMAT_ARGB32,
                                            size.width_, size.height_,
                                            buffer.getStride());
    if (surface == nullptr) {
        return false;
    }

    cairo_t *cr = cairo_create(surface);
    if (cr == nullptr) {
        return false;
    }

    surface_ = surface;
    cairo_ = cr;
    return true;
}

void
CairoWindow::calcTextSize(std::string text, std::string font, int fontsize,
                          Size &size)
{
    // Cairo needs a surface and a drawing object to determine text extents, but
    // we need those values before creating the window buffer.
    // Use a 0-sized temporary surface, which seems to work.
    cairo_surface_t *surface =
        cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 0, 0);

    cairo_t *cr = cairo_create(surface);
    cairo_select_font_face(cr, font.c_str(), CAIRO_FONT_SLANT_NORMAL,
                           CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cr, fontsize);

    cairo_text_extents_t extents;
    cairo_text_extents(cr, text.c_str(), &extents);

    cairo_destroy(cr);
    cairo_surface_destroy(surface);

    size.width_ = (int)extents.width;
    size.height_ = (int)extents.height;
}

}
