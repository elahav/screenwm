/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "startup.h"
#include "context.h"
#include <spawn.h>
#include <stdlib.h>

namespace ScreenWM
{

static pid_t
spawn(std::string exec)
{
    // Convert the command line string to an array of arguments.
    char * const    args = strdup(exec.c_str());
    unsigned        count = 0;
    unsigned        max = 16;
    char            prev = '\0';

    char          **argv = (char **)malloc(sizeof(char *) * max);

    for (unsigned i = 0; ; i++) {
        if (args[i] == '\0') {
            break;
        }

        if (isgraph(args[i])) {
            if (prev == '\0') {
                argv[count] = &args[i];
                count++;
                if (count == (max - 1)) {
                    max *= 2;
                    argv = (char **)realloc(argv, sizeof(char *) * max);
                }
            }
        } else {
            args[i] = '\0';
        }

        prev = args[i];
    }

    argv[count] = NULL;

    // Create the new process.
    pid_t pid;
    int const rc = posix_spawn(&pid, argv[0], NULL, NULL, argv, NULL);

    if (rc != 0) {
        Slog2(SLOG2_ERROR).log("Failed to launch %s: %s\n", exec.c_str(),
                               strerror(rc));
        pid = -1;
    }

    free(argv);
    free(args);

    return pid;
}

/**
 * Class constructor.
 */
Startup::Startup() : Settings("startup.ini")
{
}

/**
 * Class destructor.
 */
Startup::~Startup()
{
}

/**
 * Starts all programs mentioned in the startup file.
 * @return  true if successful, false otherwise
 */
bool
Startup::startAll()
{
    if (!isValid()) {
        return false;
    }

    for (Settings::GroupIterator itr = groupBegin();
         itr != groupEnd();
         ++itr) {
        std::string type = getValue(itr, "type");

        // Get the command line.
        std::string exec = getValue(itr, "exec");
        if (exec == "") {
            continue;
        }

        ProcessInfo *info = new ProcessInfo;

        // Get the window's position.
        getPosition(itr, info->pos_);

        // Start the program.
        pid_t const pid = spawn(exec);
        if (pid == -1) {
            delete info;
            continue;
        }

        if (type == "dock") {
            info->type_ = Dock;
        } else if (type == "locker") {
            info->type_ = Locker;
        } else {
            info->type_ = Normal;
        }

        procMap_[pid] = info;
    }

    return true;
}

/**
 * Called when a program started by this manager posts its first window.
 * This allows the startup manager to position the window according to the
 * coordinates specified in the startup file.
 */
bool
Startup::handleWindowPosted(Window &window)
{
    // Ensure that the window belongs to the process launched by the last call
    // to startNext().
    pid_t   pid;
    if (!window.getProperty(SCREEN_PROPERTY_OWNER_PID, &pid)) {
        return false;
    }

    auto    itr = procMap_.find(pid);
    if (itr == procMap_.end()) {
        return false;
    }

    ProcessInfo *info = itr->second;

    // Position the window.
    if ((info->pos_.x_ == -1) || (info->pos_.y_ == -1)) {
        Rect    screenRect;
        Rect    windowRect;
        if (Context::getContext().getDisplayGeometry(screenRect)
            && window.getGeometry(windowRect)) {
            if (info->pos_.x_ == -1) {
                info->pos_.x_ = screenRect.width_ - windowRect.width_;
            }
            if (info->pos_.y_ == -1) {
                info->pos_.y_ = screenRect.height_ - windowRect.height_;
            }
        }
    }

    window.setPosition(info->pos_);

    // Handle special windows.
    switch (info->type_) {
    case Dock:
        Context::getContext().addDockWindow(window);
        break;

    case Locker:
        Context::getContext().setLockerWindow(window);
        break;

    case Normal:
        break;
    }

    procMap_.erase(itr);
    delete info;

    return true;
}

/**
 * Extract the coordinate values from a "position" key.
 * @param   itr     The settings group to search for a key
 * @param   pos     The position structure to fill
 */
void
Startup::getPosition(const GroupIterator &itr, Point &pos)
{
    std::string posString = getValue(itr, "position");
    if (posString.empty()) {
        return;
    }

    const char *str = posString.c_str();
    char *end;
    int x = strtol(str, &end, 10);
    if ((end == str) || (*end != 'x')) {
        return;
    }

    str = end + 1;
    int y = strtol(str, &end, 10);
    if ((end == str) || (*end != '\0')) {
        return;
    }

    pos.x_ = x;
    pos.y_ = y;
}

}
