/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "event.h"
#include "window.h"
#include "slog2.h"

namespace ScreenWM
{

/**
 * Class constructor.
 */
Event::Event()
{
    handle_ = NULL;
    if (screen_create_event(&handle_) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to create event");
    }
}

/**
 * Class destructor.
 */
Event::~Event()
{
    if (handle_) {
        screen_destroy_event(handle_);
    }
}

/**
 * Retrieves the value of the named integer property from the event.
 * @param   name    Property name
 * @param   value   Holds the value, upon return
 * @return  true if successful, false otherwise
 */
bool
Event::getProperty(int name, int &value) const
{
    if (screen_get_event_property_iv(handle_, name, &value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get event property");
        return false;
    }

    return true;
}

/**
 * Fills an integer array witg the values of the named event property.
 * @param   name    Property name
 * @param   values  Array to fill
 * @return  true if successful, false otherwise
 */
bool
Event::getProperty(int name, int values[]) const
{
    if (screen_get_event_property_iv(handle_, name, values) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get event property");
        return false;
    }

    return true;
}

/**
 * Retrieves the values of the named integer array property from the event.
 * @param   name    Property name
 * @param   values  Holds the values, upon return
 * @return  true if successful, false otherwise
 */
bool
Event::getProperty(int name, void *values[]) const
{
    if (screen_get_event_property_pv(handle_, name, values) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get event property");
        return false;
    }

    return true;
}

/**
 * Retrieves the values of the named position property from the event.
 * @param   name    Property name
 * @param   pos     Holds the values, upon return
 * @return  true if successful, false otherwise
 */
bool
Event::getProperty(int name, Point &pos) const
{
    int value[2];

    if (!getProperty(name, value)) {
        return false;
    }

    pos.x_ = value[0];
    pos.y_ = value[1];
    return true;
}

/**
 * Gives a new value to an integer property.
 * @param   name    Property name
 * @param   value   New value
 * @return  true if successful, false otherwise
 */
bool
Event::setProperty(int name, int value)
{
    if (screen_set_event_property_iv(handle_, name, &value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set event property");
        return false;
    }

    return true;
}

/**
 * Gives a new value to an integer property.
 * @param   name    Property name
 * @param   value   New value
 * @return  true if successful, false otherwise
 */
bool
Event::setProperty(int name, void *value)
{
    if (screen_set_event_property_pv(handle_, name, &value) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set event property");
        return false;
    }

    return true;
}

/**
 * Gives new values to an integer array property.
 * @param   name    Property name
 * @param   values  New values
 * @return  true if successful, false otherwise
 */
bool
Event::setProperty(int name, int values[])
{
    if (screen_set_event_property_iv(handle_, name, values) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set event property");
        return false;
    }

    return true;
}

/**
 * Gives new values to a pointer array property.
 * @param   name    Property name
 * @param   values  New values
 * @return  true if successful, false otherwise
 */
bool
Event::setProperty(int name, void *values[])
{
    if (screen_set_event_property_pv(handle_, name, values) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to set event property");
        return false;
    }

    return true;
}

/**
 * Convenience function for retrieving the type property from the event.
 * @return  Event's type
 */
int
Event::getType() const
{
    int type;

    if (!getProperty(SCREEN_PROPERTY_TYPE, &type)) {
        return SCREEN_EVENT_NONE;
    }

    return type;
}

/**
 * Sets the type property of the event.
 * @param   type    Event type
 * @return  true if successful, false otherwise
 */
bool
Event::setType(int type)
{
    return setProperty(SCREEN_PROPERTY_TYPE, type);
}

/**
 * Convenience function for retrieving the window handle property from the
 * event.
 * @return  Window to which the event applies
 */
Window
Event::getWindow() const
{
    screen_window_t winHandle;

    if (!getProperty(SCREEN_PROPERTY_WINDOW,
                     reinterpret_cast<void **>(&winHandle))) {
        winHandle = NULL;
    }

    return Window(winHandle);
}

/**
 * Sets the window property of the event to the handle of the given window.
 * @param   window  Window to associate with the event
 * @return  true if successful, false otherwise
 */
bool
Event::setWindow(const Window &window)
{
    screen_window_t winHandle = static_cast<screen_window_t>(window);

    return setProperty(SCREEN_PROPERTY_WINDOW,
                       reinterpret_cast<void **>(&winHandle));
}

/**
 * Extracts data from a user event, used for internal communication with the
 * main event loop.
 * @param   type    Holds the event type, upon return
 * @param   value   Holds the event value, upon return
 * @return  true if successful, false otherwise
 */
bool
Event::getUserData(UserType& type, uintptr_t &value) const
{
    int     data[4];
    if (screen_get_event_property_iv(handle_, SCREEN_PROPERTY_USER_DATA, data)
        < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get event property");
        return false;
    }

    type = static_cast<UserType>(data[0]);
    value = static_cast<uintptr_t>(static_cast<unsigned>(data[1]))
        | (static_cast<uintptr_t>(static_cast<unsigned>(data[2])) << 32);
    return true;
}

/**
 * Extracts a string from a user event.
 * @param   value   Holds the event string, upon return
 * @return  true if successful, false otherwise
 */
bool
Event::getUserData(std::string &value) const
{
    int     len;
    if (screen_get_event_property_iv(handle_, SCREEN_PROPERTY_SIZE, &len) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get event data length");
        return false;
    }

    char    *str = new char[len];
    if (screen_get_event_property_cv(handle_, SCREEN_PROPERTY_USER_DATA, len,
                                     str) < 0) {
        Slog2(SLOG2_ERROR).perror("Failed to get event user string");
        return false;
    }

    value = std::string(str, len);
    delete [] str;
    return true;
}

/**
 * Initializes a user event, used for internal communication with the
 * main event loop.
 * @param   type    Event type
 * @param   value   Event value
 * @return  true if successful, false otherwise
 */
bool
Event::setUserData(UserType type, uintptr_t value)
{
    int data[4] = {
        type,
        static_cast<int>(value),
        static_cast<int>(value >> 32)
    };

    if (!setType(SCREEN_EVENT_USER)) {
        return false;
    }

    if (!setProperty(SCREEN_PROPERTY_USER_DATA, data)) {
        return false;
    }

    return true;
}

}
