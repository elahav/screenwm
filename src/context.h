/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_CONTEXT_H
#define SCREENWM_CONTEXT_H

#include "screenwm.h"
#include "window.h"
#include "pixmap.h"
#include "event.h"
#include "rootwindow.h"
#include "keyboardsession.h"
#include "display.h"
#include "localwindow.h"
#include "timer.h"

namespace ScreenWM
{

class Startup;
class TaskBar;
class IdleTimer;

/**
 * @brief   Implementation of the window manager interface to screen.
 *
 * The class provides the main event loop of the window manager, and wraps any
 * native calls that need a handle to a Screen context.
 */
class Context
{
public:
    Context();

    bool isValid() const { return handle_ != NULL; }
    int eventLoop();
    bool createWindow(Window &window);
    bool createPixmap(Pixmap &pixmap);
    bool createSession(int type, Session& session);
    void getDesktopArea(Rect &rect) const;
    void blit(const Buffer &dst, const Buffer &src, int attribs[]);
    void fill(const Buffer &dst, int attribs[]);
    void activate(Frame *frame);
    bool sendEvent(Event &event, pid_t pid = 0);
    bool sendEvent(Event &event, Window window);
    bool getProperty(int name, int &value) const;
    bool getProperty(int name, void *values[]) const;
    void windowTitleChanged(const Frame &frame);
    void setLockerWindow(Window &window);
    void lockScreen();
    void addDockWindow(Window &window);

    /**
     * Indicates that a flush is needed.
     * The window manager will call screen_flush_context() at some later time.
     */
    void flush() { needFlush_ = true; }

    /**
     * Gets the size of the display on which the root window is shown.
     * @param   rect    Rectangle to update with the display geometry
     * @return  true if successful, false otherwise
     */
    bool getDisplayGeometry(Rect &rect) const {
        return rootWindow_.getDisplayGeometry(rect);
    }

    static Context& getContext() { return *globalContext_; }

private:
    friend class IdleTimer;

    /** Native context handle. */
    screen_context_t    handle_;

    /** Native event handle. */
    Event               event_;

    /**
     * Keeps track of whether a flush is needed. Avoids excessive flushing in
     * reposone to a single event.
     */
    bool                needFlush_;

    /**
     * Active display.
     * FIXME:
     * Replace with a list of displays.
     */
    Display             *display_;

    /** The parent of all top-level windows. */
    RootWindow          rootWindow_;

    /** The currently active (raised) frame. */
    Frame               *activeFrame_;

    /**
     * Start-up manager for launching programs when the window manager starts.
     */
    Startup             *startup_;

    /** Top-level session to capture all keyboard events. */
    KeyboardSession     keyboardSession_;

    /** Screen lock window. */
    Window              lockerWindow_;

    /** Pointer to the one context object used by the window manager. */
    static Context      *globalContext_;

    /** Nanoseconds to wait before an idle timeout. */
    IdleTimer           *idleTimer_;

    /** Whether the system is idle (i.e., display is turned off. */
    bool                isIdle_;

    /** Optional native taskbar. */
    TaskBar             *taskbar_;

    bool init();
    bool initKeyboardSession();
    bool handleEvent();
    bool handleTimeout();
    bool handleCreated();
    bool handleWindowCreated();
    bool handleWindowPosted();
    bool handlePropertyChange();
    bool handleWindowProperty(int);
    bool handlePointer();
    bool handleKeyboard();
    bool handleWindowClosed();
    bool handleUserEvent();
    bool handleWindowStatusChanged(Window &, Frame *);
    bool handleManagerEvent();
    void createTaskBar(const Settings &settings);
    void idleTimeout();
    void destroy();
};

}

#endif
