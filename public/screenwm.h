/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCREENWM_SCREENWM_H
#define SCREENWM_SCREENWM_H

#include <stdint.h>

enum
{
    SCREENWM_EVENT_WINDOW_LIST,
    SCREENWM_EVENT_LOCK,
    SCREENWM_EVENT_MEDIA,
    SCREENWM_EVENT_VOLUME,
    _SCREENWM_EVENT_NUM
};

enum
{
    SCREENWM_WINDOW_LIST_ADDED,
    SCREENWM_WINDOW_LIST_REMOVED,
    SCREENWM_WINDOW_LIST_ACTIVE_CHANGED
};

enum
{
    SCREENWM_MEDIA_PLAY,
    SCREENWM_MEDIA_PAUSE
};

enum
{
    SCREENWM_VOLUME_UP,
    SCREENWM_VOLUME_DOWN,
    SCREENWM_VOLUME_MUTE
};

struct screenwm_notify_msg
{
    uint16_t        type;
    uint16_t        zero;
    int32_t         event_name;
    struct sigevent event;
};

#endif
